declare module "*css";
declare module "*scss";
declare module "*svg";
declare module "*json";
declare module "*png";

interface NodeModule {
  hot?: any;
}

declare type Omit<T, K> = Pick<T, Exclude<keyof T, K>>

declare type Constructor<T = {}> = new (...args: any[]) => T
