export class MockCommit {
  shaField: string;
  messageField: string;
  parentsField: MockCommit[];

  constructor(sha: string, message: string, parents: MockCommit[]) {
    this.shaField = sha;
    this.messageField = message;
    this.parentsField = parents;
  }

  async getParents() {
    return this.parentsField;
  }

  sha() {
    return this.shaField;
  }

  date() {
    return new Date();
  }

  message() {
    return this.messageField;
  }
}
