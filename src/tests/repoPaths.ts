import path from "path";

export const generatePathForTest = name => path.join(__dirname, "..", "..", "repos", name);
export const simple = path.join(__dirname, "..", "..", "repos", "simple");
