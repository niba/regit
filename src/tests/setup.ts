import fs from "fs-extra";
import nodegit from "nodegit";
import path from "path";

async function createRepository(repoPath): Promise<nodegit.Repository> {
  return await nodegit.Repository.init(repoPath, 0);
}

async function addFileToIndex(repository: nodegit.Repository, fileName: string): Promise<nodegit.Oid> {
  const repoIndex = await repository.refreshIndex();
  await repoIndex.addByPath(fileName);
  await repoIndex.write();
  return await repoIndex.writeTree();
}

export async function clearDirectory(repoPath) {
  try {
    await fs.remove(repoPath);
    await fs.ensureDir(repoPath);
  } catch (err) {
    console.log(`Cannot remove directory ${repoPath}`);
    throw err;
  }
}

export async function prepareSimpleRepo(repoPath): Promise<nodegit.Repository> {
  await clearDirectory(repoPath);

  if (!fs.existsSync(repoPath)) {
    console.log(`Directory ${repoPath} doesn't exist. Creating one...`);
    fs.ensureDirSync(repoPath);
  }

  const repository = await createRepository(repoPath);
  await setupSimpleRepository(repository);
  return repository;
}

async function setupSimpleRepository(repository: nodegit.Repository) {
  const branchMasterName = "master";
  const branchDevName = "dev";
  const branchFeatureName = "feature";

  const fileName = "file.txt";
  const fileContent = "regit test";
  const fileOtherContent = "we changed it";
  const secondFileName = "file2.txt";
  const secondFileContent = "blablabla";

  const firstDeveloperSignature = nodegit.Signature.create("Developer 1", "developer1@email.com", 123456789, 60);

  const secondDeveloperSignature = nodegit.Signature.create("Developer 2", "developer2@email.com", 123456789, 60);

  await fs.writeFile(path.join(repository.workdir(), fileName), fileContent);
  const oid = await addFileToIndex(repository, fileName);
  const commitOid = await repository.createCommit(
    "HEAD",
    firstDeveloperSignature,
    firstDeveloperSignature,
    "initial commit",
    oid,
    [],
  );

  const initCommit = await repository.getCommit(commitOid);
  const branchDev = await repository.createBranch(branchDevName, initCommit, true, firstDeveloperSignature, "a");
  const branchFeature = await repository.createBranch(
    branchFeatureName,
    initCommit,
    true,
    firstDeveloperSignature,
    "a",
  );

  await fs.writeFile(path.join(repository.workdir(), secondFileName), secondFileContent);
  const secondOid = await addFileToIndex(repository, secondFileName);
  const secondCommitOid = await repository.createCommit(
    "HEAD",
    firstDeveloperSignature,
    firstDeveloperSignature,
    "second commit",
    secondOid,
    [initCommit],
  );

  await repository.checkoutBranch(branchDev, {
    checkoutStrategy: nodegit.Checkout.STRATEGY.FORCE,
  });

  await fs.writeFile(path.join(repository.workdir(), fileName), fileOtherContent);
  const thirdOid = await addFileToIndex(repository, fileName);
  await repository.createCommit(
    "HEAD",
    firstDeveloperSignature,
    firstDeveloperSignature,
    "other branch commit",
    thirdOid,
    [initCommit],
  );
}
