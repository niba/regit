import installExtension, { REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS } from "electron-devtools-installer";

const extensions = [REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS];

export const installExtensions = () => {
  extensions.forEach(async extension => {
    try {
      // const name = await installExtension(extension.id);
      // console.log(`Added Extension:  ${name}`);
    } catch (err) {
      console.log("An error occurred: ", err);
    }
  });
};
