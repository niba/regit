import { Position, Toaster, IconClasses, Intent } from "@blueprintjs/core";

const toastService = Toaster.create({
  position: Position.BOTTOM,
});

export const showSuccess = (message: string | JSX.Element) =>
  toastService.show({
    iconName: IconClasses.TICK,
    message,
    intent: Intent.SUCCESS,
  });

export const showWarning = (message: string | JSX.Element) =>
  toastService.show({
    iconName: IconClasses.WARNING_SIGN,
    message,
    intent: Intent.WARNING,
  });

export const showError = (message: string | JSX.Element) =>
  toastService.show({
    iconName: IconClasses.ERROR,
    message,
    intent: Intent.DANGER,
  });

export const showNotification = (message: string | JSX.Element) =>
  toastService.show({
    iconName: IconClasses.INFO_SIGN,
    message,
    intent: Intent.PRIMARY,
  });
