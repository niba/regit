import { createStore, applyMiddleware, GenericStoreEnhancer, compose, combineReducers } from "redux";
import { createEpicMiddleware } from "redux-observable";
import { reducers, ApplicationState } from "./modules/app.reducers";
import logger from "redux-logger";
import { rootEpic } from "./modules/app.epics";
import { routerReducer, routerMiddleware } from "react-router-redux";

const windowIfDefined = typeof window === "undefined" ? undefined : (window as any);
const devToolsExtension = windowIfDefined && (windowIfDefined.devToolsExtension as () => GenericStoreEnhancer);

const epicMiddleware = createEpicMiddleware(rootEpic);

export const configureStore = history => (preloadedState?: ApplicationState) => {
  const allReducers = buildRootReducer(reducers);

  const store = createStore(
    allReducers,
    preloadedState,
    compose(
      applyMiddleware(logger, routerMiddleware(history), epicMiddleware),
      devToolsExtension ? devToolsExtension() : f => f,
    ),
  );

  return store;
};

function buildRootReducer(allReducers): any {
  return combineReducers(Object.assign({}, allReducers, { routing: routerReducer }));
}
