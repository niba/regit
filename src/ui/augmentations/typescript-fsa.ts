import { Meta, Action, AnyAction } from "typescript-fsa";

declare module "typescript-fsa" {
  export interface ActionCreator<P> {
    type: string;
    payloadType: P;
    match: (action: AnyAction) => action is Action<P>;
    (payload: P, meta?: Meta): Action<P>;
  }
}
