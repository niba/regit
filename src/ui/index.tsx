import * as React from "react";
import * as ReactDOM from "react-dom";
import { AppContainer } from "react-hot-loader";
import "ui/augmentations/typescript-fsa";
import "ui/augmentations/react-redux";
// import "normalize.css";
import "@blueprintjs/core/dist/blueprint.css";
import Root from "./Root";

const rootEl = document.getElementById("root");
const render = Component =>
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    rootEl,
  );

render(Root);
if (module.hot) {
  module.hot.accept("./Root", () => {
    const NextRoot = require("./Root").default;
    render(NextRoot);
  });
}
