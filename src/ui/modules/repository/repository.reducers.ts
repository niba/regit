// todo
import { actions } from "./repository.actions";
import { reducerWithInitialState } from "typescript-fsa-reducers";
import * as _ from "lodash";

export interface RepositoryState {
  loadedRepositoryIndex: number;
  openRepositories: string[];
  recentRepositories: string[];
  isRepoLoading: boolean;
  isAuthenticating: boolean;
  cloneWindow: {
    isLoading: boolean;
    isOpen: boolean;
    cloneProgress: number;
  };
}

const defaultState: RepositoryState = {
  openRepositories: [],
  recentRepositories: [],
  loadedRepositoryIndex: -1,
  isRepoLoading: false,
  cloneWindow: {
    isLoading: false,
    isOpen: false,
    cloneProgress: 0,
  },
  isAuthenticating: false,
};

const closeRepository = (state: RepositoryState, payload: typeof actions.closeRepository.payloadType) => {
  const isLast = payload === state.openRepositories.length - 1;
  state.openRepositories.splice(payload, 1);
  const loadedRepositoryIndex = !isLast ? payload : payload > 0 ? payload - 1 : -1;
  return {
    ...state,
    openRepositories: state.openRepositories.slice(),
    loadedRepositoryIndex,
  };
};

const openRepository = (state: RepositoryState, payload: typeof actions.openRepository.done.payloadType) => {
  const directory = payload.result.directory;
  const index = state.openRepositories.indexOf(directory);
  if (index === -1) {
    return {
      ...state,
      openRepositories: state.openRepositories.concat([directory]),
      isRepoLoading: true,
    };
  }
  return state;
};

const loadRepository = (state: RepositoryState, payload: typeof actions.loadRepository.payloadType) => ({
  ...state,
  loadedRepositoryIndex:
    payload > -1 && payload < state.openRepositories.length ? payload : state.loadedRepositoryIndex,
});

const setCloneWindowStatus = (state: RepositoryState, windowProps: Partial<RepositoryState["cloneWindow"]>) => {
  return {
    ...state,
    cloneWindow: {
      ...state.cloneWindow,
      ...windowProps,
    },
  };
};

const updateRecentRepositories = (state: RepositoryState, newRepositories: string[]) => {
  return {
    ...state,
    recentRepositories: _.chain(newRepositories)
      .uniq()
      .take(5)
      .value(),
  };
};

const authenticate = (state: RepositoryState, isAuthenticating: boolean) => {
  return {
    ...state,
    isAuthenticating,
  };
};

export const repositoryReducer = reducerWithInitialState(defaultState)
  .case(actions.openRepository.done, openRepository)
  .case(actions.loadRepository, loadRepository)
  .case(actions.closeRepository, closeRepository)
  .case(actions.authenticate.started, s => authenticate(s, true))
  .cases([actions.authenticate.done, actions.authenticate.failed], s => authenticate(s, false))
  .case(actions.cloneWindowStateChange, (s, isOpen) => setCloneWindowStatus(s, { isOpen }))
  .case(actions.loadRecentRepositories.done, (s, action) => updateRecentRepositories(s, action.result))
  .case(actions.cloneRepository.done, (s, p) => setCloneWindowStatus(s, { isOpen: false, isLoading: false }))
  .case(actions.initRepository.done, (s, p) => {
    const newState = setCloneWindowStatus(s, { isOpen: false, isLoading: false });
    newState.isRepoLoading = false;
    newState.cloneWindow.cloneProgress = 0;
    return updateRecentRepositories(newState, [p.params, ...s.recentRepositories]);
  })
  .case(actions.initRepository.failed, (s, p) => ({ ...s, isRepoLoading: false }))
  .cases([actions.cloneRepository.started], (s, p) => setCloneWindowStatus(s, { isLoading: true }))
  .cases([actions.cloneRepository.failed], (s, p) => setCloneWindowStatus(s, { isLoading: false }))
  .case(actions.cloneProgress, (state, progress) => ({
    ...state,
    cloneWindow: { ...state.cloneWindow, cloneProgress: progress },
  }));
