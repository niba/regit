import * as React from "react";
import { Classes, Button, IconClasses, Intent } from "@blueprintjs/core";
import glamorous from "glamorous";
import { callableActions } from "../repository.actions";

const InputRow = glamorous.div({
  display: "flex",
  alignItems: "center",
  width: "100%",
  margin: "5px",
});

const Label = glamorous.label({
  marginRight: "5px",
  width: "100px",
});
const OkButton = glamorous(Button)({});

type Props = { authAction: typeof callableActions.authenticateDone };

export class AuthWindow extends React.Component<Props> {
  passwordInput: HTMLInputElement;

  render() {
    return [
      <div className={Classes.DIALOG_BODY}>
        <InputRow>
          <Label>Password:</Label>
          <input
            ref={input => (this.passwordInput = input as HTMLInputElement)}
            autoFocus={true}
            className={Classes.INPUT}
            type="password"
          />
        </InputRow>
      </div>,
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <OkButton
            iconName={IconClasses.LOG_IN}
            intent={Intent.PRIMARY}
            text="Proceed"
            type="submit"
            onClick={() =>
              this.props.authAction({
                result: {
                  password: this.passwordInput.value,
                },
                params: {},
              })
            }
          />
        </div>
      </div>,
    ];
  }
}
