import { CloneRepositoryData } from "../repository.models";
import * as React from "react";
import glamorous from "glamorous";
import { Button, IconClasses, Classes, Intent, ProgressBar } from "@blueprintjs/core";
import electron from "electron";
import { ApplicationState } from "ui/modules/app.reducers";
import { connect } from "react-redux";

const InputRow = glamorous.div({
  display: "flex",
  alignItems: "center",
  width: "100%",
  margin: "5px",
});

const Label = glamorous.label({
  marginRight: "5px",
  width: "100px",
});

const remote = electron.remote;
const mainProcess = remote.require("./dialog");

class CloneRepository extends React.Component<
  { cloneRepository: (data: CloneRepositoryData) => void; isLoading: boolean } & ConnectProps,
  any
> {
  remoteUrlInput: HTMLInputElement;
  localPathInput: HTMLInputElement;

  async loadFileFromDialog() {
    const path = await mainProcess.openDirectory();
    this.localPathInput.value = path.directory;
  }

  componentDidMount() {
    this.remoteUrlInput.focus();
  }

  render() {
    return [
      <div className={Classes.DIALOG_BODY}>
        <InputRow>
          <Label>URL:</Label>
          <input
            ref={input => (this.remoteUrlInput = input as HTMLInputElement)}
            autoFocus={true}
            className={Classes.INPUT}
            type="text"
          />
        </InputRow>
        <InputRow>
          <Label>Local path:</Label>
          <input
            ref={input => (this.localPathInput = input as HTMLInputElement)}
            autoFocus={true}
            className={Classes.INPUT}
            type="text"
          />
          <div style={{ marginLeft: "5px" }}>
            {" "}
            <Button onClick={() => this.loadFileFromDialog()}>Browse</Button>
          </div>
        </InputRow>
        {this.props.isLoading && <ProgressBar value={this.props.cloneProgress} />}
      </div>,
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button
            loading={this.props.isLoading}
            iconName={IconClasses.DOWNLOAD}
            intent={Intent.PRIMARY}
            onClick={() =>
              this.props.cloneRepository({
                remoteUrl: this.remoteUrlInput.value,
                localPath: this.localPathInput.value,
              })
            }
          >
            Clone
          </Button>
        </div>
      </div>,
    ];
  }
}

const connectCreator = connect((state: ApplicationState) => state.repository.cloneWindow);
type ConnectProps = typeof connectCreator.allProps;
export const CloneRepositoryContainer = connectCreator(CloneRepository) as any;
