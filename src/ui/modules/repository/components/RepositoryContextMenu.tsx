import { ContextMenuTarget, Menu, MenuItem } from "@blueprintjs/core";
import * as React from "react";
import electron from "electron";
const remote = electron.remote;

type Props = { repositoryPath?: string };

@ContextMenuTarget
export class RepositoryContextMenu extends React.Component<Props & {}, {}> {
  render() {
    return <div>{this.props.children}</div>;
  }

  renderContextMenu() {
    if (!this.props.repositoryPath) return <div />;

    return (
      <Menu>
        <MenuItem
          text={`Open containing directory`}
          onClick={() => remote.shell.openItem(this.props.repositoryPath as string)}
        />
      </Menu>
    );
  }
}
