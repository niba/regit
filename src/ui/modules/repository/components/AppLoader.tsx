import React from "react";
import glamorous from "glamorous";
import { Classes, Dialog } from "@blueprintjs/core";
import { css } from "glamor";
import { Loader } from "./Loader";
import classNames from "classnames";

interface IAppLoaderProps {
  isLoading: boolean;
  text: string;
}

const LoadingDialogContent = glamorous.div({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  color: "white",
  margin: "20px",
});
const LoadingDialogText = glamorous.div({
  color: "white",
  fontSize: "24px",
  fontWeight: "bold",
  marginTop: "15px",
  marginLeft: "10px",
});
const LoadingDialogOverlay = css({
  background: "transparent",
  boxShadow: "initial",
}).toString();

export class AppLoader extends React.PureComponent<IAppLoaderProps, any> {
  render() {
    return (
      <Dialog
        autoFocus
        className={classNames(Classes.OVERLAY_SCROLL_CONTAINER, LoadingDialogOverlay)}
        canEscapeKeyClose={false}
        canOutsideClickClose={false}
        isOpen={this.props.isLoading}
        style={{ width: "25vw" }}
      >
        <LoadingDialogContent>
          <Loader />
          <LoadingDialogText> {this.props.text} </LoadingDialogText>
        </LoadingDialogContent>
      </Dialog>
    );
  }
}
