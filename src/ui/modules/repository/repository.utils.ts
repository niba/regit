import fs from "fs-extra";
import { repositoriesPath } from "ui/utils/paths";

export const getRecentRepositories = async () => {
  const repoJson = (await fs.readFile(await repositoriesPath())).toString();
  return JSON.parse(repoJson) as string[];
};

export const saveRecentRepositories = async (repositories: string[]) => {
  await fs.writeFile(await repositoriesPath(), JSON.stringify(repositories));
};
