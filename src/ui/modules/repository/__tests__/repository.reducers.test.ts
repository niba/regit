import { actions } from "../repository.actions";
import { repositoryReducer as reducer } from "../repository.reducers";

describe("Repostiory reducer", () => {
  it("should open repository", () => {
    const fakePayload = { params: {}, result: { directory: "C:/test/directory" } };
    expect(reducer(undefined as any, actions.openRepository.done(fakePayload))).toMatchSnapshot();
  });

  it("should open repository", () => {
    const fakePayload = { params: {}, result: { directory: "C:/test/directory" } };
    const fakePayload2 = { params: {}, result: { directory: "C:/test/haha" } };
    let state = reducer(undefined as any, actions.openRepository.done(fakePayload));
    state = reducer(state, actions.openRepository.done(fakePayload2));
    expect(reducer(state, actions.loadRepository(1))).toMatchSnapshot();
  });

  it("should close repository", () => {
    const fakePayload = { params: {}, result: { directory: "C:/test/directory" } };
    const fakePayload2 = { params: {}, result: { directory: "C:/test/haha" } };
    let state = reducer(undefined as any, actions.openRepository.done(fakePayload));
    state = reducer(state, actions.openRepository.done(fakePayload2));
    expect(reducer(state, actions.closeRepository(0))).toMatchSnapshot();
  });

  it("should close repository - 2", () => {
    const fakePayload = { params: {}, result: { directory: "C:/test/directory" } };
    const fakePayload2 = { params: {}, result: { directory: "C:/test/haha" } };
    let state = reducer(undefined as any, actions.openRepository.done(fakePayload));
    state = reducer(state, actions.openRepository.done(fakePayload2));
    expect(reducer(state, actions.closeRepository(1))).toMatchSnapshot();
  });
});
