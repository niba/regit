import { actions } from "./repository.actions";
import { branchActions } from "ui/modules/branch";
import * as branchModels from "ui/modules/branch/branch.models";
import { ActionsObservable } from "redux-observable";
import { Action, Store } from "redux";
import { makeAsyncEpic } from "ui/modules/common/makeAsyncEpic";
import { openRepository } from "ui/gitCommands/repository";
import electron from "electron";
import { Observable } from "rxjs/Observable";
import { measureTime } from "../../utils/measureTime";
import { loadCommitsMap } from "ui/gitCommands/commit";
import { ApplicationState } from "ui/modules/app.reducers";
import { diffActions } from "ui/modules/diff";
import { actionBarActions } from "ui/modules/actionBar";
import { loadCommitsOrder, cloneRepository, getReferences } from "ui/gitCommands";
import { waitForAll, waitWithResult } from "ui/modules/common/epicUtils";
import { getRecentRepositories, saveRecentRepositories } from "ui/modules/repository/repository.utils";
import { showSuccess, showError } from "ui/services/toastService";
import { authEpic } from "ui/modules/common/common.observables";
import { loadedRepositorySelector } from "ui/modules/common/common.selectors";
import { Observer, Subject } from "rxjs";
import { Action as FsaAction } from "typescript-fsa";

const remote = electron.remote;
const mainProcess = remote.require("./dialog");

export const openDirectoryEpic = makeAsyncEpic(actions.openRepository, () => mainProcess.openDirectory());

export const refreshCommitsEpic = makeAsyncEpic(
  actions.refreshCommits,
  async (path: string, state: ApplicationState) => {
    const repo = await openRepository(path);
    const repoCache = state.cache[path];
    const commits = { ...repoCache.commits };
    const branches = repoCache.branches.concat(repoCache.tags as any);

    const { commitsMap, areFreshCommits } = await measureTime(
      async () => await loadCommitsMap(repo, branches, commits),
      "REPO_EPIC - Generate commit map",
    );

    return { commitsMap, areFreshCommits };
  },
);

export const cloneRepositoryEpic = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) => {
  const progressObs = new Subject<FsaAction<number>>();

  const authObs = authEpic(actions.cloneRepository, async (payload, password) => {
    await cloneRepository(payload.remoteUrl, payload.localPath, password, progress =>
      progressObs.next(actions.cloneProgress(progress)),
    );
  })(action$, store);

  authObs.finally(() => progressObs.complete());

  return Observable.merge(progressObs, authObs);
};

export const loadRecentRepositoriesEpic = makeAsyncEpic(actions.loadRecentRepositories, getRecentRepositories);
export const saveRecentRepositoriesEpic = makeAsyncEpic(actions.saveRecentRepositories, async (p, s) =>
  saveRecentRepositories(s.repository.recentRepositories),
);

export const onCloneEpic = (action$: ActionsObservable<Action>) =>
  action$.filter(actions.cloneRepository.done.match).map(x => {
    return openRepo(x.payload.params.localPath);
  });

export const onRecentReposLoadedEpic = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) =>
  action$
    .filter(actions.loadRecentRepositories.done.match)
    .map(_ => store.getState().repository.recentRepositories)
    .filter(recentRepos => recentRepos.length !== 0)
    .map(recentRepos => openRepo(recentRepos[0]));

const openRepo = (repoPath: string) => actions.openRepository.done({ params: {}, result: { directory: repoPath } });

const initRepositoryObservable = (action$: ActionsObservable<Action>, directory: string): Observable<any> =>
  Observable.create(async (observer: Observer<FsaAction<any>>) => {
    observer.next(branchActions.loadTags.started(directory));
    observer.next(branchActions.loadBranches.started(directory));

    let res = await waitWithResult(action$, branchActions.loadBranches, branchActions.loadTags).toPromise();
    if (res.type === "Error") {
      observer.error(res.error);
      return;
    }

    observer.next(actions.refreshCommits.started(directory));

    res = await waitWithResult(action$, actions.refreshCommits).toPromise();

    if (res.type === "Error") {
      observer.error(res.error);
      return;
    }

    observer.next(actions.initRepository.done({ result: {}, params: directory }));

    observer.complete();
  });

export const onRepoDone = (action$: Observable<Action>) =>
  action$
    .filter(actions.openRepository.done.match)
    .map(action => actions.initRepository.started(action.payload.result.directory));

export const initRepositoryEpic = (action$: ActionsObservable<Action>) =>
  action$
    .filter(actions.initRepository.started.match)
    .switchMap(action =>
      initRepositoryObservable(action$, action.payload).catch(error =>
        Observable.of(actions.initRepository.failed({ params: "", error })),
      ),
    );

export const loadOnInitEpic = (action$: Observable<Action>, store: Store<ApplicationState>) =>
  action$
    .filter(actions.initRepository.done.match)
    .map(action => store.getState().repository.openRepositories.findIndex(x => x === action.payload.params))
    .map(index => actions.loadRepository(index));

export const loadRepositoryDataEpic = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) =>
  action$
    .ofType(actions.initRepository.done.type, actions.reloadRepo.done.type)
    .map(_ => loadedRepositorySelector(store.getState()))
    .filter(repository => repository !== undefined)
    .map(repo => repo as string)
    .flatMap(repo => [
      actionBarActions.getUndoReflog.started({}),
      actionBarActions.pullStatus.started({}),
      branchActions.getCurrentBranch.started({}),
      actions.saveRecentRepositories.started({}),
      actionBarActions.pullStatus.started({}),
    ]);

export const reloadRepoEpic = makeAsyncEpic(actions.reloadRepo, async (payload: any, state: ApplicationState) => {
  const repoPath = loadedRepositorySelector(state);
  if (!repoPath) {
    throw new Error("There is no active repo");
  }
  const repo = await openRepository(repoPath);
  const references = branchModels.transformReferences(await getReferences(repo));

  const repoCache = state.cache[repoPath];
  const commits = { ...repoCache.commits };

  const { commitsMap, areFreshCommits } = await measureTime(
    async () => await loadCommitsMap(repo, references, commits),
    "REPO_EPIC - Generate commit map",
  );

  const commitsOrder = await measureTime(
    async () => await loadCommitsOrder(repo, references),
    "GRAPH - LOAD COMMITS ORDER",
  );

  return { commits: { map: commitsMap, areFresh: areFreshCommits, order: commitsOrder }, references, repoPath };
});

export const refreshRepo = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) =>
  action$
    .ofType(
      diffActions.makeCommit.done.type,
      branchActions.mergeBranches.done.type,
      actionBarActions.pushBranch.done.type,
      actionBarActions.pullCurrentBranch.done.type,
      actionBarActions.undo.done.type,
      branchActions.createNewBranch.done.type,
      branchActions.checkoutToBranch.done.type,
      branchActions.deleteBranch.done.type,
    )
    .map(_ => loadedRepositorySelector(store.getState()))
    .filter(repo => !!repo)
    .map(_ => actions.reloadRepo.started({}));
