import * as React from "react";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { Menu, MenuDivider, MenuItem, IconClasses, Classes, Icon } from "@blueprintjs/core";
import path from "path";
import { RepositoryContextMenu } from "./components/RepositoryContextMenu";
import { callableActions } from "./repository.actions";
import { loadedRepositorySelector } from "ui/modules/common/common.selectors";
import { NavItem } from "ui/modules/common/components/NavBarItem";

class TopMenu extends React.Component<ConnectProps & { onSelectionChanged; isActive }, any> {
  componentDidMount() {
    this.props.loadRecentRepositories({});
  }

  renderRecentRepository = (repoPath: string) => {
    return (
      <RepositoryContextMenu repositoryPath={repoPath}>
        <MenuItem
          onClick={() => this.props.openRepositoryDone({ params: {}, result: { directory: repoPath } })}
          iconName={IconClasses.GIT_REPO}
          className={Classes.LARGE}
          text={this.getRepoName(repoPath)}
        />
      </RepositoryContextMenu>
    );
  };

  renderOpenRepository = (repoPath: string, index: number) => {
    const isMainRepo = index === this.props.loadedRepositoryIndex;
    const label = isMainRepo ? <Icon iconName={IconClasses.TICK} /> : undefined;
    return (
      <RepositoryContextMenu repositoryPath={repoPath}>
        <MenuItem
          onClick={() => this.props.loadRepository(index)}
          iconName={IconClasses.GIT_REPO}
          className={Classes.LARGE}
          text={this.getRepoName(repoPath)}
          label={label}
        />
      </RepositoryContextMenu>
    );
  };

  getRepoName(repoPath: string) {
    return path.basename(repoPath);
  }

  render() {
    const { onSelectionChanged, isActive } = this.props;
    const currentRepo = this.props.loadedRepository ? this.getRepoName(this.props.loadedRepository) : "Not loaded";

    return (
      <NavItem
        title="Current Repository"
        subtitle={currentRepo}
        icon={<Icon iconName={IconClasses.GIT_REPO} />}
        onSelectionChanged={onSelectionChanged}
        isActive={isActive}
      >
        <Menu>
          <MenuDivider title="Create new" />
          <MenuItem
            iconName={IconClasses.FOLDER_OPEN}
            onClick={() => this.props.openRepository({})}
            text="From directory"
          />
          <MenuItem
            iconName={IconClasses.DOWNLOAD}
            onClick={() => this.props.cloneWindowStateChange(true)}
            text="Clone"
          />

          <MenuDivider title="Open repositories" />
          {this.props.openRepositories.map(this.renderOpenRepository)}

          <MenuDivider title="Recent" />
          {this.props.recentRepositories.map(this.renderRecentRepository)}
        </Menu>
      </NavItem>
    );
  }
}

const connectCreator = connect(
  (state: ApplicationState) => ({
    ...state.repository,
    loadedRepository: loadedRepositorySelector(state),
  }),
  callableActions,
);

type ConnectProps = typeof connectCreator.allProps;
export const RepoTopMenuContainer = connectCreator(TopMenu);
