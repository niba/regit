import { createSelector } from "reselect";
import * as _ from "lodash";
import { loadedRepositorySelector, cacheRepositorySelector } from "ui/modules/common/common.selectors";

export const getRepositoryTagsSelector = createSelector(
  loadedRepositorySelector,
  cacheRepositorySelector,
  (loadedRepository, cache) => (loadedRepository ? cache[loadedRepository].tags : []),
);

export const getRepositoryBranchesSelector = createSelector(
  loadedRepositorySelector,
  cacheRepositorySelector,
  (loadedRepository, cache) => (loadedRepository ? cache[loadedRepository].branches : []),
);

export const getRepositoryRemoteBranchesSelector = createSelector(getRepositoryBranchesSelector, branches => {
  return _.orderBy(branches.map((branch, index) => ({ ...branch, index })).filter(branch => branch.isRemote), x =>
    x.friendlyName.toLowerCase(),
  );
});

export const getAllReferences = createSelector(
  getRepositoryTagsSelector,
  getRepositoryBranchesSelector,
  (tags, branches) => tags.concat(branches as any),
);

export const getVisibleReferencesSelector = createSelector(
  getRepositoryTagsSelector,
  getRepositoryBranchesSelector,
  (tags, branches) => tags.filter(tag => tag.isVisible).concat(branches.filter(branch => branch.isVisible)),
);

export const getRepositoryCommitsSelector = createSelector(
  loadedRepositorySelector,
  cacheRepositorySelector,
  (loadedRepository, cache) => (loadedRepository ? cache[loadedRepository].commits : {}),
);

export const isInitializedSelector = createSelector(
  loadedRepositorySelector,
  cacheRepositorySelector,
  (loadedRepository, cache) => (loadedRepository && cache[loadedRepository] ? true : false),
);
