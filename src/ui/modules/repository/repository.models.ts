export type CloneRepositoryData = { remoteUrl: string; localPath: string };
