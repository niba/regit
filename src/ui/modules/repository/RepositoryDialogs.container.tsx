import { connect } from "react-redux";
import * as React from "react";
import { ApplicationState } from "ui/modules/app.reducers";
import { Dialog, Classes } from "@blueprintjs/core";
import { CloneRepositoryContainer } from "./components/CloneRepository";
import { AppLoader } from "./components/AppLoader";
import { AuthWindow } from "ui/modules/repository/components/AuthWindow";
import { callableActions } from "./repository.actions";

class Dialogs extends React.Component<ConnectProps, any> {
  renderCloneDialog = () => (
    <Dialog
      className={Classes.OVERLAY_SCROLL_CONTAINER}
      title="Clone repository"
      autoFocus
      canEscapeKeyClose={true}
      canOutsideClickClose={false}
      isOpen={this.props.cloneWindow.isOpen}
      onClose={() => this.props.cloneWindowStateChange(false)}
    >
      <CloneRepositoryContainer
        cloneRepository={this.props.cloneRepository}
        isLoading={this.props.cloneWindow.isLoading}
      />
    </Dialog>
  );

  renderLoadingDialog = () => <AppLoader isLoading={this.props.isRepoLoading} text="Loading..." />;

  renderAuthDialog = () => (
    <Dialog
      className={Classes.OVERLAY_SCROLL_CONTAINER}
      autoFocus
      title="Enter your password"
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
      isOpen={this.props.isAuthenticating}
      onClose={() => this.props.authenticateFailed({ params: {}, error: "Cancelled" })}
    >
      <AuthWindow authAction={this.props.authenticateDone} />
    </Dialog>
  );

  render() {
    return (
      <div>
        {this.renderAuthDialog()}
        {this.renderLoadingDialog()}
        {this.renderCloneDialog()}
      </div>
    );
  }
}

const connectCreator = connect((state: ApplicationState) => state.repository, callableActions);

type ConnectProps = typeof connectCreator.allProps;
export const RepoDialogsContainer = connectCreator(Dialogs);
