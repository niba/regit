// todo
import actionCreatorFactory from "typescript-fsa";
import path from "path";
import { Commit, Reference } from "ui/modules/common/common.models";
import { PlainObject } from "ui/modules/common/common.models";
import { CloneRepositoryData } from "./repository.models";
import { isType } from "typescript-fsa";
import { Action } from "redux";
import {
  getStartedActions,
  enhanceActionCreatorFactory,
  AsyncActionCreatorWithHandler,
  getActionsArray,
} from "ui/modules/common/actionsEnhancements";

const actionCreator = enhanceActionCreatorFactory(actionCreatorFactory("REPOSITORY_MODULE"));

const actionCreators = {
  loadRepository: actionCreator<number>("LOAD"),
  cloneProgress: actionCreator<number>("CLONE_PROGRESS"),
  closeRepository: actionCreator<number>("CLOSE"),
  cloneWindowStateChange: actionCreator<boolean>("CHANGE_WINDOW_OPEN"),
};

const asyncActionCreators = {
  refreshCommits: actionCreator.async<string, { commitsMap: PlainObject<Commit>; areFreshCommits: boolean }>(
    "REFRESH_COMMITS",
  ),
  openRepository: actionCreator.async<{}, { directory: string }>("OPEN"),
  authenticate: actionCreator.async<any, { password: string }>("AUTH_USER"),
  loadCommits: actionCreator.async<string, Commit[]>("LOAD_COMMITS"),
  loadRecentRepositories: actionCreator.async<any, string[]>("LOAD_RECENT_REPOSITORIES"),
  saveRecentRepositories: actionCreator.async<any, any>("SAVE_RECENT_REPOSITORIES"),
  reloadRepo: actionCreator.async<
    {},
    {
      references: Reference[];
      commits: { map: PlainObject<Commit>; areFresh: boolean; order: string[] };
      repoPath: string;
    }
  >("RELOAD_REPO"),
};

const asyncActionCreatorsWithHandlers = {
  initRepository: actionCreator.asyncWithHandlers<string, {}>("INIT", {
    successMessage: action => `Repository ${path.basename(action.payload.params)} has been initialized.`,
  }),
  cloneRepository: actionCreator.async<CloneRepositoryData, any>("CLONE", {
    successMessage: action =>
      `Repository ${path.basename(action.payload.params.localPath)} has been cloned successfully.`,
  }),
};

export const actions = {
  ...actionCreators,
  ...asyncActionCreators,
  ...asyncActionCreatorsWithHandlers,
};

export const callableActions = {
  ...actionCreators,
  ...getStartedActions(asyncActionCreators),
  ...getStartedActions(asyncActionCreatorsWithHandlers),
  authenticateFailed: asyncActionCreators.authenticate.failed,
  authenticateDone: asyncActionCreators.authenticate.done,
  openRepositoryDone: asyncActionCreators.openRepository.done,
};

export const notifiableActions: AsyncActionCreatorWithHandler<any, any, any>[] = getActionsArray(
  asyncActionCreatorsWithHandlers,
);
