import { combineEpics, ActionsObservable } from "redux-observable";
import { notifiableActions } from "./app.actions";
import { Action } from "redux";
import { Action as FsaAction } from "typescript-fsa";

import * as diffEpics from "./diff/diff.epics";
import * as repositoryEpics from "./repository/repository.epics";
import * as actionBarEpics from "./actionBar/actionBar.epics";
import * as graphEpics from "./graph/graph.epics";
import * as commonEpics from "./common/common.epics";
import * as branchEpics from "./branch/branch.epics";
import { showSuccess, showError, showWarning } from "ui/services/toastService";
import { ActionCreator } from "typescript-fsa";
import { AsyncActionCreatorWithHandler } from "ui/modules/common/actionsEnhancements";
import { Success } from "typescript-fsa";
import { Failure } from "typescript-fsa";
import { RegitError, ErrorType } from "ui/modules/common/error";
import { notifiableActionsEpic } from "ui/modules/common/epicUtils";
import * as _ from "lodash";

const allEpics = _.chain([diffEpics, repositoryEpics, actionBarEpics, graphEpics, commonEpics, branchEpics])
  .flatMap(Object.values)
  .value();

export const rootEpic = combineEpics(notifiableActionsEpic(notifiableActions), ...allEpics);
