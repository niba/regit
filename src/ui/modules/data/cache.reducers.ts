import * as commonModels from "../common/common.models";
import { reducerWithInitialState } from "typescript-fsa-reducers/dist";
import { repositoryActions } from "../repository";
import { branchActions } from "ui/modules/branch";
import { update } from "../../utils/redux";

interface RepositoryCacheState {
  commits: commonModels.PlainObject<commonModels.Commit>;
  branches: ReadonlyArray<commonModels.Reference>;
  tags: ReadonlyArray<commonModels.Reference>;
}
export type RepoCacheState = commonModels.PlainObject<RepositoryCacheState>;

const getInitCache = () => ({
  branches: [],
  tags: [],
  commits: {},
});

const defaultRepoState: RepoCacheState = {};

const initRepository = (
  state: RepoCacheState,
  payload: typeof repositoryActions.initRepository.started.payloadType,
): RepoCacheState => {
  return {
    ...state,
    [payload]: {
      ...state[payload],
      [payload]: getInitCache(),
    },
  };
};

const initRepositoryDone = (
  state: RepoCacheState,
  payload: typeof repositoryActions.refreshCommits.done.payloadType,
): RepoCacheState => {
  if (!payload.result.areFreshCommits) {
    return state;
  }

  return {
    ...state,
    [payload.params]: {
      ...state[payload.params],
      commits: {
        ...payload.result.commitsMap,
      },
    },
  };
};

const reloadRepositoryDone = (
  state: RepoCacheState,
  payload: typeof repositoryActions.reloadRepo.done.payloadType,
): RepoCacheState => {
  const tags: commonModels.Reference[] = [];
  const branches: commonModels.Reference[] = [];

  const currentRepoCache = state[payload.result.repoPath];
  payload.result.references.forEach(reference => (reference.isTag ? tags.push(reference) : branches.push(reference)));

  if (
    !payload.result.commits.areFresh &&
    tags.length === currentRepoCache.tags.length &&
    branches.length === currentRepoCache.branches.length
  ) {
    return state;
  }

  const newCommits = payload.result.commits.areFresh
    ? {
        ...payload.result.commits.map,
      }
    : state[payload.result.repoPath].commits;

  return {
    ...state,
    [payload.result.repoPath]: {
      ...state[payload.result.repoPath],
      commits: newCommits,
      branches: branches.slice(),
      tags: tags.slice(),
    },
  };
};

const resetRepoCache = (state: RepoCacheState, payload: typeof repositoryActions.openRepository.done.payloadType) => {
  return {
    ...state,
    [payload.result.directory]: getInitCache(),
  };
};

const loadBranches = (
  state: RepoCacheState,
  payload: typeof branchActions.loadBranches.done.payloadType,
): RepoCacheState => {
  return {
    ...state,
    [payload.params]: {
      ...state[payload.params],
      branches: payload.result.slice(),
    },
  };
};

const loadTags = (state: RepoCacheState, payload: typeof branchActions.loadTags.done.payloadType): RepoCacheState => {
  return {
    ...state,
    [payload.params]: {
      ...state[payload.params],
      tags: payload.result.slice(),
    },
  };
};

const setBranchesVisibility = (
  state: RepoCacheState,
  payload: typeof branchActions.setBranchesVisibility.payloadType,
): RepoCacheState => {
  const indexes = payload.indexes;
  if (indexes.length === 0) {
    return state;
  }

  const newCopy = state[payload.repository].branches.slice();
  indexes.forEach(
    index =>
      (newCopy[index] = {
        ...newCopy[index],
        isVisible: payload.visibility,
      }),
  );

  return {
    ...state,
    [payload.repository]: {
      ...state[payload.repository],
      branches: newCopy,
    },
  };
};

const setTagVisibility = (
  state: RepoCacheState,
  payload: typeof branchActions.setTagVisibility.payloadType,
): RepoCacheState => {
  const currentRepo = payload.repository;
  const tags = update(state[currentRepo].tags, payload.index, { isVisible: payload.visibility });

  return {
    ...state,
    [currentRepo]: {
      ...state[currentRepo],
      tags,
    },
  };
};

const setBranchVisibility = (
  state: RepoCacheState,
  payload: typeof branchActions.setBranchVisibility.payloadType,
): RepoCacheState => {
  const branches = update(state[payload.repository].branches, payload.index, { isVisible: payload.visibility });

  return {
    ...state,
    [payload.repository]: {
      ...state[payload.repository],
      branches,
    },
  };
};

export const dataReducer = reducerWithInitialState(defaultRepoState)
  .case(branchActions.loadBranches.done, loadBranches)
  .case(branchActions.loadTags.done, loadTags)
  .case(branchActions.setTagVisibility, setTagVisibility)
  .case(branchActions.setBranchVisibility, setBranchVisibility)
  .case(branchActions.setBranchesVisibility, setBranchesVisibility)
  .case(repositoryActions.refreshCommits.done, initRepositoryDone)
  .case(repositoryActions.reloadRepo.done, reloadRepositoryDone);
