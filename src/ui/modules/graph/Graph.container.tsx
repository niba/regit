import React from "react";
import { GraphView } from "./components/Graph";
import { Spinner, NonIdealState } from "@blueprintjs/core";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { callableActions } from "./graph.actions";
import glamorous from "glamorous";
import { getGraphCommitsSelector } from "ui/modules/graph/graph.selectors";
import { loadedRepositorySelector } from "ui/modules/common/common.selectors";

const GraphPanel = glamorous.div({
  display: "flex",
  position: "relative",
  width: "100%",
});

class Graph extends React.Component<ConnectProps & { location: string }, any> {
  state = {
    commits: [],
    numberOfCommits: 100,
    loading: false,
  };

  componentDidCatch(error, info) {
    console.error(error);
    console.info(info);
  }

  componentWillUnmount() {
    console.log("umounting");
  }

  componentDidMount() {
    console.log("loading commits");
    if (this.props.loadedRepository) {
      this.props.initGraph(this.props.loadedRepository);
    }
  }

  componentDidUpdate(prevProps: ConnectProps) {
    if (this.props.loadedRepository !== undefined && prevProps.loadedRepository !== this.props.loadedRepository) {
      this.props.initGraph(this.props.loadedRepository);
    }
  }

  render() {
    console.log("refresh graph");
    const { isLoading, graphCommits, maximumSlot, scrollToIndex, selectedSha, selectCommit } = this.props;
    return (
      <GraphPanel>
        {isLoading ? (
          <NonIdealState title="Loading" visual={<Spinner />} />
        ) : (
          <GraphView
            isLoading={isLoading}
            maximumSlot={maximumSlot}
            commits={graphCommits}
            selectCommit={selectCommit}
            selectedSha={selectedSha}
            scrollToIndex={scrollToIndex}
          />
        )}
      </GraphPanel>
    );
  }
}

const connectCreator = connect(
  (state: ApplicationState) => ({
    isLoading: state.graph.isLoading,
    scrollToIndex: state.graph.scrollToIndex,
    selectedSha: state.graph.selectedCommitSha,
    loadedRepository: loadedRepositorySelector(state),
    ...getGraphCommitsSelector(state),
  }),
  callableActions,
);
type ConnectProps = typeof connectCreator.allProps;
export const GraphContainer = connectCreator(Graph);
