export const calculateDot = (branchIndex: number) => branchIndex * 50 + 25;
export const calculateBranchPosition = (branchIndex: number) => calculateDot(branchIndex) + 25;
export const calculateLineWidth = (endBranch: number, startBranch: number) => (endBranch - startBranch) * 50;
