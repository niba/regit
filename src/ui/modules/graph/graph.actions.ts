import actionCreatorFactory from "typescript-fsa";
import {
  getStartedActions,
  enhanceActionCreatorFactory,
  AsyncActionCreatorWithHandler,
  getActionsArray,
} from "ui/modules/common/actionsEnhancements";

const actionCreator = enhanceActionCreatorFactory(actionCreatorFactory("GRAPH_MODULE"));

const actionCreators = {
  selectCommit: actionCreator<string>("SELECT_COMMIT"),
  unselectCommit: actionCreator("UNSELECT_COMMIT"),
  toggleSearchWindow: actionCreator<boolean>("TOGGLE_SEARCH_WINDOW"),
  scrollTo: actionCreator<string>("SCROLL_TO"),
};

const asyncActionCreators = {
  initGraph: actionCreator.async<string, string[]>("INITIALIZE"),
};

const asyncActionCreatorsWithHandlers = {};

export const actions = {
  ...actionCreators,
  ...asyncActionCreators,
  ...asyncActionCreatorsWithHandlers,
};

export const callableActions = {
  ...actionCreators,
  ...getStartedActions(asyncActionCreators),
  ...getStartedActions(asyncActionCreatorsWithHandlers),
};

export const notifiableActions: AsyncActionCreatorWithHandler<any, any, any>[] = getActionsArray(
  asyncActionCreatorsWithHandlers,
);
