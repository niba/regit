import { actions } from "./graph.actions";
import { reducerWithInitialState } from "typescript-fsa-reducers";
import { PlainObject } from "ui/modules/common/common.models";
import { Commit } from "ui/modules/common/common.models";
import { actions as repositoryActions } from "ui/modules/repository/repository.actions";

export interface GraphState {
  isLoading: boolean;
  commitsOrder: string[];
  commits: PlainObject<Commit>;
  selectedCommitSha?: string;
  scrollToIndex: number;
  searchWindowVisibility: boolean;
}

const defaultState: GraphState = {
  isLoading: false,
  commitsOrder: [],
  commits: {},
  scrollToIndex: -1,
  searchWindowVisibility: false,
};

const initGraph = (state: GraphState, payload: typeof actions.initGraph.done.payloadType): GraphState => ({
  ...state,
  isLoading: false,
  commitsOrder: payload.result.slice(),
});

const scrollTo = (state: GraphState, payload: typeof actions.scrollTo.payloadType): GraphState => {
  const index = state.commitsOrder.findIndex(sha => sha === payload);

  return {
    ...state,
    scrollToIndex: index,
  };
};

export const graphReducer = reducerWithInitialState(defaultState)
  .case(actions.initGraph.started, state => ({ ...state, isLoading: true }))
  .case(actions.scrollTo, scrollTo)
  .case(repositoryActions.loadRepository, (state, payload) => ({ ...state, commitsOrder: [] }))
  .case(actions.initGraph.done, initGraph)
  .case(actions.toggleSearchWindow, (state, payload) => ({
    ...state,
    searchWindowVisibility: payload,
  }))
  .case(actions.selectCommit, (state, payload) => ({
    ...state,
    selectedCommitSha: payload === state.selectedCommitSha ? undefined : payload,
  }))
  .case(repositoryActions.reloadRepo.done, (state, payload) => ({
    ...state,
    commitsOrder: payload.result.commits.areFresh ? payload.result.commits.order : state.commitsOrder,
  }))
  .case(actions.unselectCommit, (state, payload) => ({
    ...state,
    selectedCommitSha: undefined,
  }));
