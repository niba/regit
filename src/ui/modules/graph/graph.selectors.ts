import { createSelector } from "reselect";
import { ApplicationState } from "ui/modules/app.reducers";
import { getRepositoryCommitsSelector, getAllReferences } from "ui/modules/repository/repository.selectors";
import { getGraphCommits } from "ui/gitCommands/graph";

const commitsOrderSelector = (state: ApplicationState) => state.graph.commitsOrder;

export const getCommitsInOrder = createSelector(
  getRepositoryCommitsSelector,
  commitsOrderSelector,
  (commitsMap, commitsOrder) => commitsOrder.map(sha => commitsMap[sha]),
);

export const getGraphCommitsSelector = createSelector(
  getRepositoryCommitsSelector,
  commitsOrderSelector,
  getAllReferences,
  (commitsMap, commitsOrder, references) =>
    Object.keys(commitsMap).length === 0
      ? { graphCommits: [], maximumSlot: 0 }
      : getGraphCommits(commitsMap, commitsOrder, references),
);

export const getSelectedCommit = createSelector(
  getRepositoryCommitsSelector,
  (state: ApplicationState) => state.graph.selectedCommitSha,
  (commitsMap, selectedSha) => (selectedSha ? commitsMap[selectedSha] : undefined),
);
