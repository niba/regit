import React from "react";
import { Classes, Button } from "@blueprintjs/core";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { callableActions } from "./graph.actions";
import glamorous from "glamorous";
import { getCommitsInOrder, getSelectedCommit } from "ui/modules/graph/graph.selectors";
import { SearchOmnibox } from "./components/SearchOmnibox";
import classNames from "classnames";

const ButtonContainer = glamorous.div(
  {
    marginRight: "125px",
    transition: "margin 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms",
  },
  ({ selectedCommit }: { selectedCommit: any }) => {
    if (!selectedCommit) {
      return {
        marginRight: "0px",
      };
    }
    return {};
  },
);
class GraphSearch extends React.Component<ConnectProps, any> {
  render() {
    const { commits, searchWindowVisibility, toggleSearchWindow, selectedCommit } = this.props;
    return (
      <ButtonContainer
        selectedCommit={selectedCommit}
        className={classNames(Classes.NAVBAR_GROUP, Classes.ALIGN_RIGHT)}
      >
        <Button
          style={{ height: "50px" }}
          className={Classes.MINIMAL}
          iconName="search"
          text="Search"
          onClick={() => toggleSearchWindow(true)}
        />
        <SearchOmnibox
          scrollTo={this.props.scrollTo}
          isOpen={searchWindowVisibility}
          commits={commits}
          toggleSearchWindow={this.props.toggleSearchWindow}
        />
      </ButtonContainer>
    );
  }
}

const connectCreator = connect(
  (state: ApplicationState) => ({
    ...state.graph,
    selectedCommit: getSelectedCommit(state),
    commits: getCommitsInOrder(state),
  }),
  callableActions,
);
type ConnectProps = typeof connectCreator.allProps;
export const GraphSearchContainer = connectCreator(GraphSearch);
