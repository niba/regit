import React from "react";
import { GraphCommit } from "ui/modules/graph/graph.models";
import { callableActions } from "../graph.actions";
import { MakeLines } from "ui/modules/graph/components/GraphHelpers";
import { getColor } from "../../../utils/colors";

interface IGraphCommitVectorsProps {
  commit: GraphCommit;
  selectCommit: typeof callableActions.selectCommit;
  index: number;
  isSelected: boolean;
}

export class GraphCommitVectors extends React.Component<IGraphCommitVectorsProps, any> {
  shouldComponentUpdate(nextProps: IGraphCommitVectorsProps) {
    return this.props.commit.sha !== nextProps.commit.sha || this.props.isSelected !== nextProps.isSelected;
  }

  selectCommit = () => this.props.selectCommit(this.props.commit.sha);

  getCommitDot() {
    const { commit } = this.props;

    //  return <CommitDot branch={commit.dot.index} position={commit.dot.left} />;

    return (
      <div
        style={{
          borderRadius: "20px",
          display: "inline-block",
          width: "40px",
          top: "5px",
          height: "40px",
          position: "absolute",
          zIndex: 5,
          left: `${commit.dot.left + 5}px`,
          background: commit.isMergeCommit ? "white" : getColor(commit.dot.index),
          borderColor: getColor(commit.dot.index),
          borderWidth: "2px",
          borderStyle: "solid",
          boxSizing: "border-box",
        }}
      />
    );
  }

  render() {
    const { commit } = this.props;

    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          position: "relative",
          height: "50px",
        }}
        onClick={this.selectCommit}
      >
        <div
          style={{
            height: "100%",
            width: "100%",
            display: "flex",
            alignItems: "center",
            position: "relative",
          }}
        >
          {this.getCommitDot()}
          {MakeLines(commit.lines, commit.mergeLines, commit.checkoutLines, commit.isBranchTip, commit.branchIndex)}
        </div>
      </div>
    );
  }
}
