export const COMMIT_WIDTH = 50;
export const COMMIT_HEIGHT = 50;
export const ROW_HEIGHT = 50;
export const BRANCH_LIST_WIDTH = 150;
export const COMMIT_DETAILS_WIDTH = 250;
