import * as React from "react";
import timeago from "timeago.js";

interface IGraphCommitDetailsProps {
  message: string;
  date: Date;
  sha: string;
  author: string;
  headerMessage: string;
  isSelected: boolean;
  selectCommit: any;
}

const convertDate = date => {
  return timeago().format(date);
};

export class GraphCommitDetails extends React.Component<IGraphCommitDetailsProps, any> {
  selectCommit = () => this.props.selectCommit(this.props.sha);

  render() {
    const { headerMessage, date, author } = this.props;
    return (
      <div
        style={{
          display: "flex",
          zIndex: 5,
          flexDirection: "column",
          height: "40px",
          marginTop: "5px",
          marginBottom: "5px",
          marginRight: "5px",
        }}
        onClick={this.selectCommit}
      >
        <div
          style={{
            flex: 0.6,
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
            overflow: "hidden",
          }}
        >
          {headerMessage}
        </div>
        <div
          style={{
            flex: 0.4,
            color: "gray",
            fontSize: "12px",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
            overflow: "hidden",
          }}
        >
          <span> {convertDate(date)} </span>
          <span> by {author} </span>
        </div>
      </div>
    );
  }
}
