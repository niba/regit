import React from "react";
import glamorous from "glamorous";
import { List, AutoSizer, Grid, ScrollSync } from "react-virtualized";
import { GraphCommit } from "../graph.models";
import { GraphCommitVectors } from "./GraphCommitVectors";
import { GraphBranchTag } from "./GraphBranchTag";
import { GraphCommitDetails } from "./GraphCommitDetails";
import { callableActions } from "../graph.actions";
import { Spinner } from "@blueprintjs/core";
import { css } from "glamor";
import { ROW_HEIGHT, BRANCH_LIST_WIDTH } from "ui/modules/graph/components/GraphConsts";

interface GraphViewProps {
  commits: GraphCommit[];
  selectCommit: typeof callableActions.selectCommit;
  selectedSha?: string;
  scrollToIndex: number;
  isLoading: boolean;
  maximumSlot: number;
}

const NO_SCROLL = css({
  "::-webkit-scrollbar": {
    display: "none",
  },
});

const LeftList = glamorous.div({
  width: `${BRANCH_LIST_WIDTH}px`,
});

const RightSide = glamorous.div({
  flex: 1,
  display: "flex",
  cursor: "pointer",
});

const MiddleList = glamorous.div({
  flex: 0.6,
});

const RightList = glamorous.div({
  flex: 0.4,
  cursor: "pointer",
});

export class GraphView extends React.Component<GraphViewProps> {
  list: any;
  detailsList: any;

  state = {
    scrollTop: 0,
  };

  defaultProps = {
    commits: [],
    selectedIndex: -1,
  };

  renderBranchTag = ({ index, isScrolling, key, style }) => {
    const { commits } = this.props;
    const commit = commits[index];

    if (commit.referenceNames.length === 0) {
      return <div key={key}> </div>;
    }
    return (
      <div key={key} style={{ ...style, padding: "5px" }}>
        <GraphBranchTag branchIndex={commit.branchIndex} referencesNames={commit.referenceNames} />
      </div>
    );
  };

  renderCommitDetails = ({ index, isScrolling, key, style }) => {
    const { commits, selectedSha, selectCommit } = this.props;
    const commit = commits[index];
    return (
      <div key={key} style={style}>
        {commit.sha === selectedSha ? (
          <div
            style={{ position: "absolute", width: "100%", height: "50px", backgroundColor: "#e6f2fd", zIndex: -1 }}
          />
        ) : (
          undefined
        )}
        <GraphCommitDetails
          headerMessage={commit.headerMessage}
          message={commit.message}
          date={commit.date}
          sha={commit.sha}
          author={commit.author}
          isSelected={commit.sha === selectedSha}
          selectCommit={selectCommit}
        />
      </div>
    );
  };

  renderCommitGraphic = ({ index, isScrolling, key, style, maxWidth }) => {
    const { commits, selectCommit, selectedSha } = this.props;
    const commit = commits[index];
    return (
      <div key={key} style={style}>
        {commit.sha === selectedSha ? (
          <div style={{ position: "absolute", width: maxWidth, height: "50px", backgroundColor: "#e6f2fd" }} />
        ) : (
          undefined
        )}

        <GraphCommitVectors
          commit={commit}
          selectCommit={selectCommit}
          index={index}
          isSelected={commit.sha === selectedSha}
        />
      </div>
    );
  };

  onScroll = scrollInfo => {
    console.log(scrollInfo);
    this.setState({
      scrollTop: scrollInfo.scrollTop,
    });
  };

  noContentRenderer = () => {
    if (this.props.isLoading) {
      return <Spinner />;
    } else {
      return <div> NO COMMITS </div>;
    }
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedSha !== nextProps.selectedSha) {
      this.detailsList.forceUpdateGrid();
    }
  }

  getMaximumGraphWidth = () => (this.props.maximumSlot + 1) * 50 + 25;

  getColumnWidth = (index, width) => {
    const BRANCH_PANEL = 150;
    const DETAILS_PANEL = 300;
    switch (index) {
      case 0:
        return BRANCH_PANEL;
      case 1:
        return width - BRANCH_PANEL - DETAILS_PANEL;
      case 2:
        return DETAILS_PANEL;
      default:
        return 100;
    }
  };

  render() {
    const { commits, scrollToIndex } = this.props;
    const columntWidth = this.getMaximumGraphWidth();
    return (
      <ScrollSync>
        {({ clientHeight, clientWidth, onScroll, scrollHeight, scrollLeft, scrollTop, scrollWidth }) => {
          return [
            <LeftList key="leftList">
              <AutoSizer disableWidth>
                {({ height }) => (
                  <List
                    className={NO_SCROLL.toString()}
                    width={BRANCH_LIST_WIDTH}
                    height={height}
                    rowHeight={ROW_HEIGHT}
                    rowCount={commits.length}
                    rowRenderer={this.renderBranchTag}
                    scrollTop={scrollTop}
                    onScroll={onScroll as any}
                  />
                )}
              </AutoSizer>
            </LeftList>,
            <RightSide key="rightSide">
              <MiddleList key="middleList">
                <AutoSizer>
                  {({ width, height }) => (
                    <Grid
                      className={"no-scroll"}
                      autoContainerWidth={false}
                      columnWidth={columntWidth > width ? columntWidth : width}
                      columnCount={1}
                      width={width}
                      height={height}
                      rowHeight={ROW_HEIGHT}
                      rowCount={commits.length}
                      cellRenderer={({ columnIndex, key, rowIndex, style }) => {
                        return this.renderCommitGraphic({
                          index: rowIndex,
                          isScrolling: false,
                          key,
                          style,
                          maxWidth: columntWidth > width ? columntWidth : width,
                        });
                      }}
                      scrollTop={scrollTop}
                      onScroll={onScroll}
                      scrollToRow={scrollToIndex}
                    />
                  )}
                </AutoSizer>
              </MiddleList>
              <RightList key="rightList">
                <AutoSizer>
                  {({ height, width }) => (
                    <List
                      ref={detailsList => (this.detailsList = detailsList)}
                      width={width}
                      height={height}
                      rowHeight={ROW_HEIGHT}
                      rowCount={commits.length}
                      rowRenderer={this.renderCommitDetails}
                      scrollTop={scrollTop}
                      onScroll={onScroll as any}
                    />
                  )}
                </AutoSizer>
              </RightList>
            </RightSide>,
          ];
        }}
      </ScrollSync>
    );
  }
}
