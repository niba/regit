import React from "react";
import glamorous from "glamorous";
import { getColor } from "../../../utils/colors";
import { IActionBranchLine, IBranchLine } from "../graph.models";
import { COMMIT_HEIGHT } from "../components/GraphConsts";

export const CommitDot = glamorous.div(
  {
    borderRadius: "25px",
    display: "inline-block",
    width: "30px",
    height: "30px",
    position: "absolute",
    zIndex: 5,
  },
  ({ branch, position }: { position: number; branch: number }) => ({
    left: `${position}px`,
    background: getColor(branch),
  }),
);

export const CommitDotSvg = ({ position, branch }) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%">
    <circle cx={position} cy="25" r="25" fill={getColor(branch)} />
  </svg>
);

const getCurveLine = (line: IActionBranchLine, isMerge: boolean) => {
  if (line.horizontal.index <= line.vertical.index) {
    return `<path
        d="M${line.horizontal.left} ${COMMIT_HEIGHT / 2}
          C ${line.vertical.left} ${COMMIT_HEIGHT / 2 - 5}, ${line.vertical.left} ${COMMIT_HEIGHT / 2 - 5},
          ${line.vertical.left} ${isMerge ? COMMIT_HEIGHT : 0}"
        fill="transparent"
        stroke="${getColor(line.vertical.index)}"
      />`;
  }

  return `<path
      d="M${line.horizontal.left} ${isMerge ? COMMIT_HEIGHT : 0}
        C ${line.horizontal.left} ${COMMIT_HEIGHT / 2 - 5}, ${line.horizontal.left} ${COMMIT_HEIGHT / 2 - 5},
        ${line.vertical.left} ${COMMIT_HEIGHT / 2}"
      fill="transparent"
      stroke="${getColor(line.vertical.index)}"
    />`;
};

export const MakeLines = (
  lines: IBranchLine[],
  mergeLines: IActionBranchLine[],
  checkoutLines: IActionBranchLine[],
  branchTip: boolean,
  branchIndex: number,
) => {
  const linesSVG = lines
    .map(
      element =>
        `<line
        y1="${element.vertical.index === branchIndex && branchTip ? 25 : 0}"
        y2="50"
        x1="${element.vertical.left}"
        x2="${element.vertical.left}"
        style="stroke: ${getColor(element.vertical.index)}"
      />`,
    )
    .concat(mergeLines.map(mergeLine => getCurveLine(mergeLine, true)))
    .concat(checkoutLines.map(checkoutLine => getCurveLine(checkoutLine, false)));

  const uri = "data:image/svg+xml;charset=UTF-8";
  const svgURI = `${uri},
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100%"
      height="100%"
    >
      <g>${linesSVG.join("")}</g>
    </svg>`;
  return <img style={{ width: "100%", height: `${COMMIT_HEIGHT}px` }} src={svgURI} />;
};
