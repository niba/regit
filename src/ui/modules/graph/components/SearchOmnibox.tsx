import * as React from "react";
import { Overlay, InputGroup, Classes, MenuItem, Menu } from "@blueprintjs/core";
import glamorous from "glamorous";
import Fuse from "fuse.js";
import { callableActions } from "../graph.actions";
import { debounce } from "lodash";
import { Commit } from "ui/modules/common/common.models";

interface SearchOmniboxProps {
  isOpen: boolean;
  toggleSearchWindow: typeof callableActions.toggleSearchWindow;
  commits: Commit[];
  scrollTo: typeof callableActions.scrollTo;
}

interface SearchOmniboxState {
  result: Commit[];
}

const CONSTS = {
  SEARCH_WIDTH: 700,
};

const OmniboxOverlay = glamorous(Overlay)({
  backgroundColor: "rgba(255, 255, 255, 0.2)",
});

const OmniboxBody = glamorous.div({
  top: `${15}vh`,
  left: `calc(${50}% - (${CONSTS.SEARCH_WIDTH}px / 2))`,
});

const OmniboxMenu = glamorous(Menu)({
  width: CONSTS.SEARCH_WIDTH,
});

const OmniboxInputGroup = glamorous(InputGroup)({
  width: CONSTS.SEARCH_WIDTH,
});

export class SearchOmnibox extends React.Component<SearchOmniboxProps, SearchOmniboxState> {
  fuse: Fuse;
  lunr: any;

  constructor() {
    super();

    this.state = {
      result: [],
    };

    this.debouncedSearch = debounce(this.debouncedSearch.bind(this), 250);
  }

  componentDidMount() {
    const commits = this.props.commits;
    this.reindexCommits(commits);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.commits !== this.props.commits) {
      this.reindexCommits(this.props.commits);
    }

    if (prevProps.isOpen && !this.props.isOpen) {
      this.setState({
        result: [],
      });
    }
  }

  reindexCommits(commits) {
    this.fuse = new Fuse(commits, {
      keys: ["sha", "headerMessage"],
      threshold: 0.3,
    });
  }

  debouncedSearch(value) {
    const result = this.fuse.search<Commit>(value).slice(0, 10);
    this.setState({
      result,
    });
  }

  goToCommit = (commit: Commit) => {
    this.close();
    this.props.scrollTo(commit.sha);
  };

  search = (event: React.FormEvent<HTMLInputElement>) => {
    this.debouncedSearch(event.currentTarget.value);
  };

  maybeRenderMenu() {
    if (this.state.result.length === 0) {
      return undefined;
    }
    return (
      <OmniboxMenu>
        {this.state.result.map(element => (
          <MenuItem text={element.headerMessage} onClick={() => this.goToCommit(element)} />
        ))}
      </OmniboxMenu>
    );
  }

  close = () => {
    this.props.toggleSearchWindow(false);
  };

  render() {
    const { isOpen } = this.props;

    return (
      <OmniboxOverlay hasBackdrop={true} isOpen={isOpen} onClose={this.close}>
        <OmniboxBody>
          <OmniboxInputGroup
            autoFocus={true}
            className={Classes.LARGE}
            leftIconName="search"
            placeholder="Search commits..."
            onChange={this.search}
          />
          {this.maybeRenderMenu()}
        </OmniboxBody>
      </OmniboxOverlay>
    );
  }
}
