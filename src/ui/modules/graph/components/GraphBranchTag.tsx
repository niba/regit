import React, { PureComponent } from "react";
import glamorous from "glamorous";
import { Tag, Collapse } from "@blueprintjs/core";
import { getColor } from "../../../utils/colors";
import { css } from "glamor";

interface IGraphBranchTagProps {
  branchIndex: number;
  branchName?: string;
  referencesNames: string[];
}

// todo: typescript upgrade destroyed typings
const BranchTag = glamorous(Tag)(
  {
    wordBreak: "break-all",
    textOverflow: "ellipsis",
    overflow: "hidden",
    maxWidth: "100%",
    display: "-webkit-box",
    "-webkit-line-clamp": "2",
    "-webkit-box-orient": "vertical",
    marginBottom: "1px",
    maxHeight: "50px",
  } as any,
  ({ branchIndex }: { branchIndex: number }) => ({
    backgroundColor: getColor(branchIndex),
  }),
) as any;

const FullWidthClassName = css({
  width: "100%",
}).toString();

const NestedContainer = glamorous.div({
  display: "block",
  position: "absolute",
  alignItems: "flex-start",
  flexWrap: "wrap",
  justifyContent: "center",
  flexDirection: "column",
  width: "auto",
  backgroundColor: "white",
});

const Container = glamorous.div(
  {
    display: "flex",
    position: "relative",
    alignItems: "flex-start",
    justifyContent: "center",
    flexDirection: "column",
    maxHeight: "50px",
    height: "100%",
  },
  ({ isMore, isOpen }: { isMore: boolean; isOpen: boolean }) => ({
    marginTop: isMore ? "0px" : "0px",
    cursor: isMore ? "pointer" : "inherit",
    zIndex: isOpen ? 10 : 0,
  }),
);

export class GraphBranchTag extends PureComponent<IGraphBranchTagProps, any> {
  state = { isOpen: false };

  toggle = () => {
    if (this.props.referencesNames.length > 1) {
      this.setState({ isOpen: !this.state.isOpen });
    }
  };
  render() {
    const { branchIndex, referencesNames } = this.props;

    const otherReferences =
      referencesNames.length > 1 ? (
        <Collapse transitionDuration={0} className={FullWidthClassName} isOpen={this.state.isOpen}>
          <NestedContainer>
            {referencesNames
              .slice(1, referencesNames.length)
              .map(reference => <BranchTag branchIndex={branchIndex}> {reference} </BranchTag>)}
          </NestedContainer>
        </Collapse>
      ) : (
        undefined
      );

    return (
      <Container isOpen={this.state.isOpen} isMore={referencesNames.length > 1} onClick={this.toggle}>
        <BranchTag branchIndex={branchIndex}>
          {referencesNames.length > 1 ? (
            <span style={{ fontWeight: "bold" }}>+{referencesNames.length - 1}</span>
          ) : (
            undefined
          )}{" "}
          {referencesNames[0]}
        </BranchTag>
        {otherReferences}
      </Container>
    );
  }
}
