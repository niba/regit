import { Commit } from "ui/modules/common/common.models";

interface IVerticalLine {
  left: number;
  index: number;
}
interface IHorizontalLine extends IVerticalLine {
  width: number;
}

export interface IBranchLine {
  vertical: IVerticalLine;
}

export interface IActionBranchLine extends IBranchLine {
  horizontal: IHorizontalLine;
}

export type CommitVisualData = {
  isBranchTip: boolean;
  branchName: string;
  referenceNames: string[];
  branchIndex: number;
  isMergeCommit: boolean;
  isCheckoutCommit: boolean;
  checkoutBranchIndexes: number[];
  mergeBranchesIndexes: number[];
  checkoutLines: IActionBranchLine[];
  mergeLines: IActionBranchLine[];
  lines: IBranchLine[];
  dot: {
    left: number;
    index: number;
  };
};

export type GraphCommit = CommitVisualData & Commit;

export const createGraphCommit = (commit: Commit, visualData?: Partial<CommitVisualData>): GraphCommit =>
  Object.assign(
    {},
    {
      branchIndex: -1,
      branchName: "",
      isBranchTip: false,
      isCheckoutCommit: false,
      isMergeCommit: false,
      checkoutBranchIndexes: [],
      mergeBranchesIndexes: [],
      checkoutLines: [],
      referenceNames: [],
      mergeLines: [],
      lines: [],
      dot: { index: -1, left: -1 },
    },
    commit,
    visualData,
  );
