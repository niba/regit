import { actions } from "./graph.actions";
import { branchActions } from "ui/modules/branch";
import { makeAsyncEpic } from "../common/makeAsyncEpic";
import { openRepository } from "ui/gitCommands/repository";
import { measureTime } from "../../utils/measureTime";
import { loadCommitsOrder } from "ui/gitCommands";
import { ApplicationState } from "ui/modules/app.reducers";
import { repositoryActions } from "ui/modules/repository";
import { getVisibleReferencesSelector } from "ui/modules/repository/repository.selectors";
import { Action, Store } from "redux";
import { ActionsObservable } from "redux-observable";
import { push } from "react-router-redux";
import { loadedRepositorySelector } from "ui/modules/common/common.selectors";

export const shouldReinitGraph2 = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) =>
  action$
    .ofType(branchActions.setBranchVisibility.type, branchActions.setTagVisibility.type)
    .map(action => actions.initGraph.started(loadedRepositorySelector(store.getState()) as any));

export const shouldReinitGraph = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) =>
  action$
    .filter(repositoryActions.initRepository.done.match)
    .filter(action => action.payload.params === loadedRepositorySelector(store.getState()))
    .map(action => actions.initGraph.started(action.payload.params));

export const selectOnScrollEpic = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) =>
  action$.filter(actions.scrollTo.match).map(action => actions.selectCommit(action.payload));

export const initGraphEpic = makeAsyncEpic(actions.initGraph, async (path: string, state: ApplicationState) => {
  const repo = await openRepository(path);
  const references = getVisibleReferencesSelector(state);
  const commitsOrder = await measureTime(
    async () => await loadCommitsOrder(repo, references),
    "GRAPH - LOAD COMMITS ORDER",
  );
  return commitsOrder;
});

export const goToGraphOnUnselect = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) =>
  action$
    .ofType(actions.unselectCommit.type)
    .map(_ => (store.getState() as any).routing.location.pathname)
    .filter(pathname => pathname === "diff")
    .map(_ => push("/"));
