import { ContextMenuTarget, Menu, MenuItem } from "@blueprintjs/core";
import * as React from "react";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { callableActions } from "../branch.actions";
import { actionBarCallableActions } from "ui/modules/actionBar";

type Props = { branchName: string };

@ContextMenuTarget
class BranchContextMenu extends React.Component<Props & ConnectProps, {}> {
  render() {
    return <div>{this.props.children}</div>;
  }

  renderContextMenu() {
    const { mergeBranches, branchName, checkout, deleteBranch, push } = this.props;

    return (
      <Menu>
        <MenuItem
          text={`Merge ${branchName} into current branch`}
          onClick={() => mergeBranches({ from: branchName })}
        />

        <MenuItem text={`Checkout to ${branchName}`} onClick={() => checkout({ name: branchName })} />
        <MenuItem text={`Delete ${branchName}`} onClick={() => deleteBranch({ name: branchName })} />
        <MenuItem text={`Push ${branchName}`} onClick={() => push({ name: branchName })} />
      </Menu>
    );
  }
}

const empty = {};
const actions = {
  mergeBranches: callableActions.mergeBranches,
  checkout: callableActions.checkoutToBranch,
  deleteBranch: callableActions.deleteBranch,
  push: actionBarCallableActions.pushBranch,
};

const connectCreator = connect((state: ApplicationState) => empty, actions);
type ConnectProps = typeof connectCreator.allProps;
export const BranchContextMenuContainer = connectCreator(BranchContextMenu);
