import * as React from "react";
import glamorous from "glamorous";
import { BranchContextMenuContainer } from "./BranchContextMenuContainer";
import { Reference } from "ui/modules/common/common.models";
import { Classes, IconClasses, Button, IconName } from "@blueprintjs/core";
import { graphCallableActions } from "ui/modules/graph";
import classNames from "classnames";

type IReferenceListItemProps = Reference & {
  setVisibility: (data: { visibility: boolean; index: number }) => void;
  scrollTo: typeof graphCallableActions.scrollTo;
  index: number;
  isCheckout?: boolean;
  withIcons?: boolean;
  noButton?: boolean;
  iconName?: IconName;
};

const ElementContextMenu = glamorous(BranchContextMenuContainer)({
  display: "flex",
  width: "100%",
  cursor: "pointer",
  alignItems: "center",
  justifyContent: "flex-start",
});

const ElementContainer = glamorous.div({
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
  fontSize: "13px",
  alignItems: "center",
});

// todo: typescript upgrade destroyed typings
const BranchNameButton = glamorous(Button)(
  {
    fontSize: "13px",
    flex: 1,
    textAlign: "left",
  },
  ({ isCheckout }: { isCheckout: boolean }) =>
    ({
      fontWeight: isCheckout ? "bold" : "inherit",
    } as any),
) as any;

const BranchNameText = glamorous.div(
  {
    flex: 1,
    textAlign: "left",
  },
  ({ isCheckout }: { isCheckout: boolean }) =>
    ({
      fontWeight: isCheckout ? "bold" : "inherit",
    } as any),
);

export const ReferenceListItem: React.StatelessComponent<IReferenceListItemProps> = ({
  setVisibility,
  scrollTo,
  isVisible,
  friendlyName,
  target,
  name,
  index,
  isCheckout = false,
  withIcons = true,
  noButton = false,
  iconName = IconClasses.GIT_BRANCH,
}) => {
  return (
    <ElementContextMenu key={name} branchName={friendlyName}>
      <ElementContainer>
        {noButton ? (
          <BranchNameText isCheckout={isCheckout}>{friendlyName}</BranchNameText>
        ) : (
          <BranchNameButton
            text={friendlyName}
            iconName={withIcons ? iconName : undefined}
            className={classNames(Classes.MINIMAL, Classes.SMALL, Classes.ALIGN_LEFT)}
            isCheckout={isCheckout}
            onClick={() => scrollTo(target)}
          />
        )}

        <Button
          className={classNames(Classes.MINIMAL, Classes.SMALL)}
          iconName={isVisible ? IconClasses.EYE_ON : IconClasses.EYE_OFF}
          onClick={e => {
            setVisibility({ index, visibility: !isVisible });
            e.stopPropagation();
          }}
        />
      </ElementContainer>
    </ElementContextMenu>
  );
};
