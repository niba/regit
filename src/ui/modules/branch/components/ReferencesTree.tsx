import * as React from "react";
import { Tree, ITreeNode, IconClasses } from "@blueprintjs/core";
import { graphCallableActions } from "ui/modules/graph";
import { Reference } from "ui/modules/common/common.models";
import { BranchContextMenuContainer } from "./BranchContextMenuContainer";
import { ReferenceListItem } from "./ReferenceListItem";
import { VirtualizedTree } from "ui/modules/common/components/Tree/VirtualizedTree";

interface ICollapseListProps {
  elements: ReadonlyArray<Reference>;
  setVisibility: (data: { visibility: boolean; index: number }) => void;
  scrollTo: typeof graphCallableActions.scrollTo;
}

interface ICollapseListState {
  isOpen: boolean;
  nodes: any;
  targetMap: any;
  expandedNodes: any;
}

const branchSeparator = "/";
export class CollapseTree extends React.Component<ICollapseListProps, ICollapseListState> {
  constructor(props) {
    super(props);

    this.state = {
      nodes: this.generateNodeData(props),
      targetMap: this.generateMap(props),
      expandedNodes: {},
      isOpen: true,
    };
  }

  toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });

  handleNodeCollapse = (nodeData: ITreeNode) => {
    nodeData.isExpanded = false;
    this.setState({
      ...this.state,
      expandedNodes: {
        ...this.state.expandedNodes,
        [(nodeData as any).path]: false,
      },
    });
  };

  handleNodeExpand = (nodeData: ITreeNode) => {
    nodeData.isExpanded = true;
    this.setState({
      ...this.state,
      expandedNodes: {
        ...this.state.expandedNodes,
        [(nodeData as any).path]: true,
      },
    });
  };

  forEachNode = (nodes: ITreeNode[], callback: (node: ITreeNode) => void) => {
    if (nodes === null || nodes === undefined) {
      return;
    }

    for (const node of nodes) {
      callback(node);
      this.forEachNode((node as any).childNodes, callback);
    }
  };

  handleNodeClick = (nodeData: ITreeNode, nodePath: number[], e: React.MouseEvent<HTMLElement>) => {
    if (!(nodeData as any).path) {
      return;
    }
    const originallySelected = nodeData.isSelected;
    this.forEachNode(this.state.nodes, n => (n.isSelected = false));
    nodeData.isSelected = originallySelected === null ? true : !originallySelected;
    this.scrollTo((nodeData as any).path);
    this.setState(this.state);
  };

  scrollTo = (name: string) => {
    this.props.scrollTo(this.state.targetMap[name]);
  };

  componentDidUpdate(prevProps) {
    if (prevProps.elements !== this.props.elements) {
      this.setState({
        nodes: this.generateNodeData(this.props),
        targetMap: this.generateMap(this.props),
      });
    }
  }

  generateMap = props => {
    const map = {};
    const { elements } = props;
    elements.forEach(element => (map[element.friendlyName] = element.target));
    return map;
  };

  getBranch = name => {
    return this.props.elements.find(element => element.friendlyName === name);
  };

  onSelectionChange = (node: ITreeNode) => {
    if (!(node as any).path) {
      return;
    }

    this.scrollTo((node as any).path);
    this.setState(this.state);
  };

  onExpandChange = (node: ITreeNode) => {
    this.setState({
      ...this.state,
      expandedNodes: {
        ...this.state.expandedNodes,
        [(node as any).path]: node.isExpanded,
      },
    });
  };

  generateNodeData = props => {
    const data = {};
    const { elements } = props;
    const expandedNodes = this.state ? this.state.expandedNodes : {};
    elements.forEach(element => {
      const chunks = element.friendlyName.split(branchSeparator);

      chunks.reduce((currObj, chunk, index) => {
        if (!currObj[chunk]) {
          currObj[chunk] = {};
        }
        return currObj[chunk];
      }, data);
    });

    const generateNode = (localData, key, keyChunk = "") => {
      const keys = Object.keys(localData);
      const normalizedKeyChunk = keyChunk === "" ? key : keyChunk + branchSeparator + key;

      const node = this.getBranch(normalizedKeyChunk);

      if (keys.length === 0 && node) {
        return {
          iconName: IconClasses.GIT_BRANCH,
          label: (
            <BranchContextMenuContainer branchName={normalizedKeyChunk}>
              <ReferenceListItem
                {...node}
                setVisibility={e => this.props.setVisibility(e)}
                scrollTo={this.props.scrollTo}
                index={(node as any).index}
                friendlyName={key}
                withIcons={false}
                noButton={true}
              />
            </BranchContextMenuContainer>
          ),
          path: normalizedKeyChunk,
          //   secondaryLabel,
          id: key,
        };
      } else {
        return {
          hasCaret: true,
          iconName: IconClasses.FOLDER_CLOSE,
          path: normalizedKeyChunk,
          isExpanded: expandedNodes[normalizedKeyChunk] ? true : false,
          label: key,
          childNodes: keys.map(nestedKey => generateNode(localData[nestedKey], nestedKey, normalizedKeyChunk)),
          id: key,
        };
      }
    };

    const nodeData = Object.keys(data).map(key => generateNode(data[key], key));
    console.log(nodeData);
    return nodeData;
  };

  render() {
    return (
      <Tree
        contents={this.state.nodes}
        onNodeCollapse={this.handleNodeCollapse}
        onNodeClick={this.handleNodeClick}
        onNodeExpand={this.handleNodeExpand}
      />
    );

    // return (
    //   <div style={{ width: "100%", height: "500px"}}>
    //     <VirtualizedTree
    //       nodes={this.state.nodes}
    //       onExpandChange={this.onExpandChange}
    //       onSelectionChange={this.onSelectionChange}
    //     />
    //   </div>

    // );
  }
}
