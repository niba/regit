import * as React from "react";
import { Collapse, Button, Classes, Icon, IconClasses, MenuDivider, IconName } from "@blueprintjs/core";
import glamorous from "glamorous";
import classNames from "classnames";
interface ICollapseListProps {
  title: string;
  iconName: IconName;
  length: number;
  className?: string;
}

interface ICollapseListState {
  isOpen: boolean;
}

const CollapseButton = glamorous(Button)({
  textAlign: "left",
  fontWeight: "bold",
  color: "#6a747b",
});

const Divider = glamorous(MenuDivider)({
  margin: "0px",
});

const CollapseContent = glamorous.div({
  padding: "10px",
  overflowX: "auto",
});

export class CollapseList extends React.Component<ICollapseListProps, ICollapseListState> {
  state = {
    isOpen: false,
  };

  toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    const { isOpen } = this.state;
    const { children, title, iconName, length } = this.props;

    return (
      <div>
        <CollapseButton
          className={classNames(Classes.FILL, Classes.MINIMAL, Classes.ALIGN_LEFT)}
          onClick={this.toggleOpen}
          iconName={isOpen ? IconClasses.CARET_DOWN : IconClasses.CARET_UP}
        >
          {" "}
          <Icon iconName={iconName} style={{ marginRight: "2px" }} />
          <span style={{ color: "#6a747b" }}>
            {" "}
            {title} <span style={{ fontSize: "13px", fontWeight: "normal" }}> ({length}) </span>{" "}
          </span>
        </CollapseButton>
        <Collapse isOpen={isOpen}>
          {length ? <CollapseContent className={this.props.className}>{children}</CollapseContent> : undefined}
        </Collapse>
        <Divider />
      </div>
    );
  }
}
