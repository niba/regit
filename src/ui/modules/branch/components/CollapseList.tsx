import * as React from "react";
import { Collapse, Button, Classes, Icon, IconClasses, MenuDivider } from "@blueprintjs/core";
import glamorous from "glamorous";
import { graphCallableActions } from "ui/modules/graph";
import { Reference } from "ui/modules/common/common.models";
import { BranchContextMenuContainer } from "./BranchContextMenuContainer";
import classNames from "classnames";

interface ICollapseListProps {
  elements: ReadonlyArray<Reference>;
  title: string;
  setVisibility: (data: { visibility: boolean; index: number }) => void;
  scrollTo: typeof graphCallableActions.scrollTo;
}

interface ICollapseListState {
  isOpen: boolean;
}

const CollapseButton = glamorous(Button)({
  textAlign: "left",
  fontWeight: "bold",
  color: "#6a747b",
});

const ElementContainer = glamorous(BranchContextMenuContainer)({
  display: "flex",
  width: "100%",
  cursor: "pointer",
});

const ElementIcon = glamorous.div({
  flex: 0.3,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

const ElementName = glamorous.div({
  flex: 0.7,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

const Divider = glamorous(MenuDivider)({
  margin: "0px",
});

const CollapseContent = glamorous.div({
  margin: "10px",
});

export class CollapseList extends React.Component<ICollapseListProps, ICollapseListState> {
  state = {
    isOpen: true,
  };

  toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });

  createElement = (element, index) => (
    <ElementContainer key={index} branchName={element.friendlyName}>
      <ElementIcon onClick={() => this.props.setVisibility({ index, visibility: !element.isVisible })}>
        {element.isVisible ? <Icon iconName="eye-on" /> : <Icon iconName="eye-off" />}
      </ElementIcon>
      <ElementName onClick={() => this.props.scrollTo(element.target)}>{element.friendlyName}</ElementName>
    </ElementContainer>
  );

  render() {
    const { isOpen } = this.state;
    const { elements, title } = this.props;

    return (
      <div>
        <CollapseButton
          className={classNames(Classes.FILL, Classes.MINIMAL, Classes.ALIGN_LEFT)}
          onClick={this.toggleOpen}
          iconName={isOpen ? IconClasses.CARET_DOWN : IconClasses.CARET_UP}
        >
          <Icon iconName={IconClasses.GIT_BRANCH} style={{ marginRight: "2px" }} />
          <span style={{ color: "#6a747b" }}>
            {title} <span style={{ fontSize: "13px", fontWeight: "normal" }}> ({elements.length}) </span>
          </span>
        </CollapseButton>
        <Collapse isOpen={isOpen}>
          <CollapseContent> {elements.map((element, index) => this.createElement(element, index))} </CollapseContent>
        </Collapse>
        <Divider />
      </div>
    );
  }
}
