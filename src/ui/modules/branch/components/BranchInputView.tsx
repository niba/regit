import * as React from "react";
import { Classes, Button, IconClasses } from "@blueprintjs/core";
import glamorous from "glamorous";
import classNames from "classnames";

const BranchAcceptButton = glamorous(Button)({
  width: "80px",
});
const Layout = glamorous.div({
  display: "flex",
  width: "100%",
  justifyContent: "space-between",
  padding: "10px",
});

export class BranchInputView extends React.Component<{ onAccept: (branchName: string) => void }, any> {
  input: HTMLInputElement;

  render() {
    return (
      <Layout>
        <input
          ref={input => (this.input = input as HTMLInputElement)}
          autoFocus={true}
          className={Classes.INPUT}
          type="text"
          style={{ width: "100%", marginLeft: "5px" }}
        />
        <BranchAcceptButton
          iconName={IconClasses.ADD}
          className={classNames(Classes.MINIMAL, Classes.SMALL, Classes.POPOVER_DISMISS)}
          text="New"
          onClick={() => this.props.onAccept(this.input.value)}
        />
      </Layout>
    );
  }
}
