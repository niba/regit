import { createSelector } from "reselect";
import { ApplicationState } from "ui/modules/app.reducers";
import _ from "lodash";
import { getRepositoryBranchesSelector } from "ui/modules/repository/repository.selectors";
const getCurrentBranchSelector = (state: ApplicationState) => state.branch.currentBranch;

export const getRepositoryLocalBranchesSelector = createSelector(
  getCurrentBranchSelector,
  getRepositoryBranchesSelector,
  (currentBranch, branches) => {
    return _.orderBy(
      branches
        .map((branch, index) => ({ ...branch, index, isCheckout: currentBranch === branch.friendlyName }))
        .filter(branch => !branch.isRemote),
      x => x.friendlyName.toLowerCase(),
    );
  },
);
