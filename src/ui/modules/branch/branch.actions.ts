import actionCreatorFactory from "typescript-fsa";
import {
  getStartedActions,
  enhanceActionCreatorFactory,
  AsyncActionCreatorWithHandler,
  getActionsArray,
} from "ui/modules/common/actionsEnhancements";
import { Reference } from "ui/modules/common/common.models";

const actionCreator = enhanceActionCreatorFactory(actionCreatorFactory("BRANCH_MODULE"));

const actionCreators = {
  setBranchVisibility: actionCreator<{ index: number; visibility: boolean; repository: string }>(
    "SET_BRANCH_VISIBILITY",
  ),
  setTagVisibility: actionCreator<{ index: number; visibility: boolean; repository: string }>("SET_TAG_VISIBILITY"),
  setBranchesVisibility: actionCreator<{ indexes: number[]; visibility: boolean; repository: string }>(
    "SET_BRANCHES_VISIBILITY",
  ),
};

const asyncActionCreators = {
  loadBranches: actionCreator.async<string, Reference[]>("LOAD_BRANCHES"),
  getCurrentBranch: actionCreator.async<any, { name: string }>("GET_CURRENT_BRANCH"),
  loadTags: actionCreator.async<string, Reference[]>("LOAD_TAGS"),
};

const asyncActionCreatorsWithHandlers = {
  checkoutToBranch: actionCreator.asyncWithHandlers<{ name: string }, void>("CHECKOUT_TO_BRANCH", {
    successMessage: action => `Branch has been changed to ${action.payload.params.name}.`,
  }),
  mergeBranches: actionCreator.asyncWithHandlers<{ from: string }, any>("MERGE_BRANCHES", {
    successMessage: action => `Merging from ${action.payload.params.from} branch succeeded.`,
  }),
  createNewBranch: actionCreator.asyncWithHandlers<{ name: string }, void>("CREATE_NEW_BRANCH", {
    successMessage: action => `Branch ${action.payload.params.name} has been created successfully.`,
  }),
  deleteBranch: actionCreator.asyncWithHandlers<{ name: string }, void>("DELETE_BRANCH"),
};

export const actions = {
  ...actionCreators,
  ...asyncActionCreators,
  ...asyncActionCreatorsWithHandlers,
};

export const callableActions = {
  ...actionCreators,
  ...getStartedActions(asyncActionCreators),
  ...getStartedActions(asyncActionCreatorsWithHandlers),
};

export const notifiableActions: AsyncActionCreatorWithHandler<any, any, any>[] = getActionsArray(
  asyncActionCreatorsWithHandlers,
);
