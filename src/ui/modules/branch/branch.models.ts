import nodegit from "nodegit";
import { Reference } from "ui/modules/common/common.models";

export const transformReferences = (references: nodegit.Reference[]): Reference[] =>
  references.map(reference => ({
    name: reference.name(),
    friendlyName: reference.shorthand(),
    target: reference.targetPeel() ? reference.targetPeel().tostrS() : reference.target().tostrS(),
    isVisible: true,
    isRemote: reference.isRemote() > 0,
    isHead: reference.isHead() === 1,
    isTag: reference.isTag() > 0,
  }));
