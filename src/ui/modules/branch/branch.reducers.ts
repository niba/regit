import { reducerWithInitialState } from "typescript-fsa-reducers/dist";
import { actions } from "./branch.actions";

export interface BranchState {
  currentBranch?: string;
}

const defaultState: BranchState = {};

const updateCurrentBranch = (state: BranchState, name: string) => {
  return {
    ...state,
    currentBranch: name,
  };
};

export const branchReducer = reducerWithInitialState(defaultState).case(actions.getCurrentBranch.done, (s, p) =>
  updateCurrentBranch(s, p.result.name),
);
