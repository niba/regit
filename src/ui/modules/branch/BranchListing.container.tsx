import React from "react";
import { Panel } from "ui/modules/common/components/Panel";
import { IconClasses } from "@blueprintjs/core";
import { ApplicationState } from "ui/modules/app.reducers";
import { connect } from "react-redux";
import glamorous from "glamorous";
import * as repositorySelectors from "ui/modules/repository/repository.selectors";
import { CollapseTree } from "./components/ReferencesTree";
import { CollapseList } from "./components/CollapseSection";
import { ReferenceListItem } from "./components/ReferenceListItem";
import { graphCallableActions } from "ui/modules/graph";
import { callableActions } from "./branch.actions";
import { loadedRepositorySelector } from "ui/modules/common/common.selectors";
import { getRepositoryLocalBranchesSelector } from "ui/modules/branch/branch.selectors";
import {
  getRepositoryRemoteBranchesSelector,
  getRepositoryTagsSelector,
} from "ui/modules/repository/repository.selectors";

const RepositoryPanel = glamorous(Panel)({
  flex: 0.15,
  overflow: "auto",
});

const PaddedCollapseSection = glamorous(CollapseList)({
  paddingLeft: "20px",
});

class BranchListing extends React.Component<ConnectProps> {
  render() {
    const { localBranches, remoteBranches, tags, setTagVisibility, setBranchVisibility, scrollTo } = this.props;

    return (
      <RepositoryPanel>
        <PaddedCollapseSection title="BRANCHES" iconName={IconClasses.GIT_BRANCH} length={localBranches.length}>
          {localBranches.map(localBranch => (
            <ReferenceListItem
              {...localBranch}
              setVisibility={res => setBranchVisibility({ repository: this.props.repository, ...res })}
              scrollTo={scrollTo}
            />
          ))}
        </PaddedCollapseSection>

        <PaddedCollapseSection title="TAGS" iconName={IconClasses.TAG} length={tags.length}>
          {tags.map((tag, index) => (
            <ReferenceListItem
              {...tag}
              index={index}
              setVisibility={res => setTagVisibility({ repository: this.props.repository, ...res })}
              scrollTo={scrollTo}
              iconName={IconClasses.TAG}
            />
          ))}
        </PaddedCollapseSection>

        <CollapseList title="REMOTE" iconName={IconClasses.CLOUD} length={remoteBranches.length}>
          <CollapseTree
            elements={remoteBranches}
            setVisibility={res => setBranchVisibility({ repository: this.props.repository, ...res })}
            scrollTo={scrollTo}
          />
        </CollapseList>
      </RepositoryPanel>
    );
  }
}

const connectCreator = connect(
  (state: ApplicationState) => ({
    tags: getRepositoryTagsSelector(state),
    localBranches: getRepositoryLocalBranchesSelector(state),
    remoteBranches: getRepositoryRemoteBranchesSelector(state),
    repository: loadedRepositorySelector(state) as string,
  }),
  {
    ...callableActions,
    ...graphCallableActions,
  },
);
type ConnectProps = typeof connectCreator.allProps;
export const BranchListingContainer = connectCreator(BranchListing);
