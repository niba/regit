import { makeAsyncEpic } from "ui/modules/common/makeAsyncEpic";
import { actions } from "./branch.actions";
import { getCurrentRepository } from "ui/modules/common/common.selectors";
import {
  checkoutBranch,
  deleteBranch,
  mergeTwoBranches,
  getTags,
  getBranches,
  createBranch,
  openRepository,
} from "ui/gitCommands";
import { transformReferences } from "./branch.models";
import { ActionsObservable } from "redux-observable";
import { Action } from "redux";
import { showSuccess, showError } from "ui/services/toastService";

export const checkoutToBranchEpic = makeAsyncEpic(actions.checkoutToBranch, async (payload, state) => {
  const repository = await getCurrentRepository(state);
  await checkoutBranch(repository, payload.name);
});

export const deleteBranchEpic = makeAsyncEpic(actions.deleteBranch, async (payload, state) => {
  const repository = await getCurrentRepository(state);
  await deleteBranch(repository, payload.name);
});

export const getCurrentBranchEpic = makeAsyncEpic(actions.getCurrentBranch, async (payload, state) => {
  const repository = await getCurrentRepository(state);
  const currentBranch = await repository.getCurrentBranch();
  return { name: currentBranch.shorthand() };
});

export const mergeBranchesEpic = makeAsyncEpic(actions.mergeBranches, async (payload, state) => {
  const repository = await getCurrentRepository(state);

  const srcBranch = await repository.getBranch(payload.from);
  const dstBranch = await repository.getCurrentBranch();

  return mergeTwoBranches(repository, srcBranch, dstBranch);
});

export const loadTagsEpic = makeAsyncEpic(actions.loadTags, async (path, state) => {
  return transformReferences(await getTags(await openRepository(path)));
});

export const loadBranchesEpic = makeAsyncEpic(actions.loadBranches, async (path, state) => {
  return transformReferences(await getBranches(await openRepository(path)));
});

export const createNewBranchEpic = makeAsyncEpic(actions.createNewBranch, async (payload, state) => {
  const repository = await getCurrentRepository(state);
  return createBranch(repository, payload.name);
});
