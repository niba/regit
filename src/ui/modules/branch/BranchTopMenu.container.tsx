import * as React from "react";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { Menu, MenuDivider, Icon, IconClasses, Classes, MenuItem } from "@blueprintjs/core";
import { BranchInputView } from "./components/BranchInputView";
import { Reference } from "ui/modules/common/common.models";
import { callableActions } from "./branch.actions";
import { NavItem } from "ui/modules/common/components/NavBarItem";
import { getRepositoryLocalBranchesSelector } from "./branch.selectors";

class TopMenu extends React.Component<ConnectProps & { onSelectionChanged; isActive }, any> {
  renderBranch = (branch: Reference) => {
    const label = (branch as any).isCheckout ? <Icon iconName={IconClasses.TICK} /> : undefined;

    return (
      <MenuItem
        onClick={() => !branch.isRemote && this.props.checkoutToBranch({ name: branch.friendlyName })}
        iconName={IconClasses.GIT_BRANCH}
        className={Classes.LARGE}
        text={branch.friendlyName}
        label={label}
      />
    );
  };

  render() {
    const { onSelectionChanged, isActive } = this.props;

    return (
      <NavItem
        title="Current Branch"
        subtitle={this.props.currentBranch || "Not loaded"}
        icon={<Icon iconName={IconClasses.GIT_BRANCH} />}
        onSelectionChanged={onSelectionChanged}
        isActive={isActive}
      >
        <Menu>
          <BranchInputView onAccept={val => this.props.createNewBranch({ name: val })} />
          <MenuDivider title="Local Branches" />
          {this.props.localBranches.map(this.renderBranch)}
        </Menu>
      </NavItem>
    );
  }
}

const connectCreator = connect(
  (state: ApplicationState) => ({
    localBranches: getRepositoryLocalBranchesSelector(state),
    currentBranch: state.branch.currentBranch,
  }),
  callableActions,
);

type ConnectProps = typeof connectCreator.allProps;
export const BranchTopMenuContainer = connectCreator(TopMenu);
