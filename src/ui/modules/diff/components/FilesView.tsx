import * as React from "react";
import { DiffTreeFileSelectionView } from "./DiffTreeFileSelectionView";
import { DiffListFileSelectionView } from "./DiffListFileSelectionView";
import { Classes, IconClasses, Button, Card, Intent, NonIdealState } from "@blueprintjs/core";
import glamorous from "glamorous";
import { FileStatus } from "../diff.models";
import classNames from "classnames";

type ViewMode = "treeview" | "fileview";

const { Div } = glamorous;

const FilesPanel = glamorous.div({
  display: "flex",
  width: "100%",
  height: "100%",
  overflow: "auto",
  flex: "1",
});

const HeadContainer = glamorous.div({
  display: "flex",
  width: "100%",
  marginTop: "15px",
  marginBottom: "15px",
  alignItems: "center",
  justifyContent: "space-between",
});

const HeaderTitle = glamorous.h4({
  color: "#6a747b",
  margin: 0,
});

const Container = glamorous.div({
  marginLeft: "10px",
  marginRight: "10px",
  flex: 1,
  display: "flex",
  flexDirection: "column",
});

const FilesCard = glamorous(Card)({
  flex: 1,
  padding: 10,
  overflow: "hidden",
});

interface FilesViewProps {
  files: FileStatus[];
  selectedFile?: FileStatus;
  handleFileClick: (file: FileStatus) => void;
  title?: string;
  hasActionButton?: boolean;
  actionButtonText?: string;
  actionButtonIntent?: Intent;
  actionButtonOnClick?: any;
  labelComponent?: any;
}

interface FilesViewState {
  mode: ViewMode;
}

export class FilesView extends React.PureComponent<FilesViewProps, FilesViewState> {
  state: FilesViewState = {
    mode: "treeview",
  };

  private selectionMappings: {
    [P in ViewMode]: (
      files: FileStatus[],
      handleFileClick: (file: FileStatus) => void,
      selectedFile?: FileStatus,
      labelComponent?: any,
    ) => JSX.Element
  } = {
    treeview: (files, handleFileClick, selectedFile, labelComponent?) => (
      <DiffTreeFileSelectionView
        selectedFile={selectedFile}
        files={files}
        onFileClick={handleFileClick}
        labelComponent={labelComponent}
      />
    ),
    fileview: (files, handleFileClick, selectedFile, labelComponent?) => (
      <DiffListFileSelectionView
        selectedFile={selectedFile}
        files={files}
        onFileClick={handleFileClick}
        labelComponent={labelComponent}
      />
    ),
  };

  selectTree = () => {
    this.setState({ mode: "treeview" });
  };

  selectList = () => {
    this.setState({ mode: "fileview" });
  };

  render() {
    const {
      files,
      selectedFile,
      handleFileClick,
      title,
      hasActionButton,
      actionButtonIntent,
      actionButtonOnClick,
      actionButtonText,
      labelComponent,
    } = this.props;
    const { mode } = this.state;
    const isTreeMode = mode === "treeview";

    return (
      <Container>
        <HeadContainer key="treeButtons">
          <Div flex={1} display="flex" justifyContent="flex-start">
            {hasActionButton ? (
              <Button
                disabled={files.length === 0}
                intent={actionButtonIntent}
                text={actionButtonText}
                onClick={actionButtonOnClick}
              />
            ) : (
              undefined
            )}
          </Div>
          <Div flex={1} display="flex" justifyContent="center">
            <HeaderTitle> {title} </HeaderTitle>
          </Div>
          <Div flex={1} display="flex" justifyContent="flex-end">
            <div className={classNames(Classes.BUTTON_GROUP, Classes.SMALL, Classes.MINIMAL)}>
              <Button text="Tree" iconName="pt-icon-layout-hierarchy" onClick={this.selectTree} active={isTreeMode} />
              <Button text="List" iconName="pt-icon-list" onClick={this.selectList} active={!isTreeMode} />
            </div>
          </Div>
        </HeadContainer>
        <FilesCard>
          <FilesPanel key="filesPanel" className={classNames()}>
            {this.props.files.length === 0 ? (
              <NonIdealState title="No files" visual={IconClasses.DOCUMENT} />
            ) : (
              <div style={{ height: "100%", width: "100%" }}>
                {this.selectionMappings[this.state.mode](files, handleFileClick, selectedFile, labelComponent)}
              </div>
            )}
          </FilesPanel>
        </FilesCard>
      </Container>
    );
  }
}
