import glamorous from "glamorous";
import * as React from "react";
import { Colors } from "@blueprintjs/core";
import { FileStatus } from "../diff.models";

const addedColor = Colors.GREEN3;
const removedColor = Colors.RED3;
const modifiedColor = Colors.ORANGE3;

export const FileLabel: React.StatelessComponent<{ file: FileStatus; title: string; className?: string }> = ({
  file,
  title,
  className,
}) => {
  return (
    <span className={className}>
      <FileHeaderText file={file}>{title}</FileHeaderText>
      <AddedSpan>&#43;{file.lineStats.added}</AddedSpan>
      <RemovedSpan>&#8722;{file.lineStats.removed}</RemovedSpan>
    </span>
  );
};

export const FileHeaderText = glamorous.span(
  {
    marginRight: "10px",
  },
  ({ file }: { file: FileStatus }) => ({
    color: file.status === "added" ? addedColor : file.status === "deleted" ? removedColor : modifiedColor,
  }),
);

export const AddedSpan = glamorous.span({
  marginRight: "2px",
  fontWeight: "bold",
  color: addedColor,
});
export const RemovedSpan = glamorous.span({
  color: removedColor,
  fontWeight: "bold",
});

const Row = glamorous.div({
  display: "flex",
  flex: "1",
});

export const AddedRemovedNumbers = ({ file }: { file: FileStatus }) => {
  return (
    <Row>
      <AddedSpan>+{file.lineStats.added}</AddedSpan> <RemovedSpan>-{file.lineStats.removed}</RemovedSpan>
    </Row>
  );
};
