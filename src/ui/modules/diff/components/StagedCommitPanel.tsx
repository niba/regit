import * as React from "react";
import { Classes, Button, Intent } from "@blueprintjs/core";
import glamorous from "glamorous";
import classNames from "classnames";
import { callableActions } from "../diff.actions";

const Panel = glamorous.div({
  marginTop: "20px",
  marginLeft: "10px",
  marginRight: "10px",
  "& *": {
    boxSizing: "border-box",
    marginBottom: "10px",
  },
});

interface StagedCommitPanelProps {
  onCommit: typeof callableActions.makeCommit;
  summary: string;
  description: string;
  isComitting: boolean;
  setCommitSummary: typeof callableActions.setCommitSummary;
  setCommitDescription: typeof callableActions.setCommitDescription;
}

export class StagedCommitPanel extends React.Component<StagedCommitPanelProps, any> {
  setDescription = event => {
    this.props.setCommitDescription(event.currentTarget.value);
  };

  setSummary = event => {
    this.props.setCommitSummary(event.currentTarget.value);
  };

  render() {
    return (
      <Panel>
        <input
          value={this.props.summary}
          className={classNames(Classes.INPUT, Classes.FILL)}
          type="text"
          disabled={this.props.isComitting}
          required
          placeholder="Summary"
          dir="auto"
          onChange={this.setSummary}
        />
        <textarea
          value={this.props.description}
          style={{ resize: "none", height: "100px" }}
          className={classNames(Classes.INPUT, Classes.FILL)}
          maxLength={1000}
          disabled={this.props.isComitting}
          placeholder="Description"
          dir="auto"
          onChange={this.setDescription}
        />
        <Button
          intent={Intent.PRIMARY}
          text="Commit"
          disabled={this.props.isComitting}
          className={classNames(Classes.FILL)}
          onClick={() => this.props.onCommit({ title: this.props.summary, description: this.props.description })}
        />
      </Panel>
    );
  }
}
