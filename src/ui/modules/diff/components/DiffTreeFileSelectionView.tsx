import * as React from "react";
import { Classes, ITreeNode, Tree } from "@blueprintjs/core";
import { FileLabel } from "./SharedComponents";
import { css } from "glamor";
import { FileStatus } from "../diff.models";
import { VirtualizedTree } from "../../common/components/Tree/VirtualizedTree";

type Partial<T> = { [P in keyof T]?: T[P] };

interface TreeFileNode extends ITreeNode {
  parent?: TreeFileNode;
  file?: FileStatus;
  isFile?: boolean;
}

interface DiffTreeFileSelectionViewProps {
  files: FileStatus[];
  labelComponent?: any;
  selectedFile?: FileStatus;
  onFileClick?(file: FileStatus): void;
}

const pathSeparator = "/";
const treeStyleClassName = css({ width: "100%" });

export class DiffTreeFileSelectionView extends React.Component<
  DiffTreeFileSelectionViewProps,
  { nodes?: TreeFileNode[] }
> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.createTreeData(this.props);
  }

  componentWillReceiveProps(newProps: DiffTreeFileSelectionViewProps) {
    if (this.props.files !== newProps.files) {
      this.createTreeData(newProps);
    }
  }

  createTreeData(newProps: DiffTreeFileSelectionViewProps) {
    if (!newProps) return;

    const nodes = newProps.files && this.createNodes(newProps.files);
    this.setState({ nodes });
  }

  handleFileClick = (node: TreeFileNode) => {
    const { onFileClick } = this.props;

    if (!node.file || !onFileClick) return;

    onFileClick(node.file);
  };

  setupFolderNode(node: Partial<TreeFileNode>, title: string) {
    node.label = title;
    node.iconName = "folder-close";
    node.childNodes = [];
    node.isFile = false;
    node.id = title;
  }

  setupFileNode = (node: Partial<TreeFileNode>, file: FileStatus, title: string) => {
    node.file = file;
    node.label = <FileLabel file={file} title={title} />;
    node.iconName = "document";
    node.secondaryLabel = this.props.labelComponent ? this.props.labelComponent(file) : undefined;
    node.isFile = true;
    node.id = file.path;
  };

  addPathChunks(nodes: TreeFileNode[], pathChunks: string[], file: FileStatus, parentNode?: TreeFileNode) {
    if (!pathChunks.length) return;

    const remainingPathChunks = pathChunks.slice(1);
    let node = nodes.find(y => y.label === pathChunks[0]) as Partial<TreeFileNode>;

    if (!node) {
      node = { parent: parentNode, isExpanded: true };
      nodes.push(node as TreeFileNode);

      if (remainingPathChunks.length) this.setupFolderNode(node, pathChunks[0]);
      else {
        this.setupFileNode(node, file, pathChunks[0]);
      }
    }

    if (!node.isFile) this.addPathChunks(node.childNodes!, remainingPathChunks, file, node as TreeFileNode);
  }

  createNodes(files: FileStatus[]) {
    const nodes: TreeFileNode[] = [];

    files.forEach(file => {
      this.addPathChunks(nodes, file.path.split(pathSeparator), file);
    });

    return nodes;
  }

  forEachNode(nodes: TreeFileNode[], callback: (node: TreeFileNode) => void) {
    if (!nodes) {
      return;
    }

    for (const node of nodes) {
      callback(node);

      if (!node.isFile) this.forEachNode(node.childNodes!, callback);
    }
  }

  onExpandChange = (node: TreeFileNode) => {
    this.setState(this.state);
  };

  onSelectionChange = (node: TreeFileNode) => {
    this.handleFileClick(node);
    this.setState(this.state);
  };

  render() {
    if (!this.state.nodes) return <div>Loading...</div>;

    return (
      <VirtualizedTree
        nodes={this.state.nodes}
        onExpandChange={this.onExpandChange}
        onSelectionChange={this.onSelectionChange}
        className={`${Classes.ELEVATION_0} ${treeStyleClassName}`}
      />
    );
  }
}
