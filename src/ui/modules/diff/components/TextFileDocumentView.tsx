import * as React from "react";
import { getCommitFile } from "../../../gitCommands/index";
import glamorous from "glamorous";
import { Commit } from "ui/modules/common/common.models";
import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/styles";
import { Spinner, NonIdealState } from "@blueprintjs/core";
import { getLanguage } from "../diff.utils";
import { createElement } from "react-syntax-highlighter";
import { getPathFile } from "ui/gitCommands/file";

type Props = { path: string; commit: Commit | undefined; repoPath: string };

const Line = glamorous.div({
  whiteSpace: "pre",
});

const StyledSyntaxHighlighter = glamorous(SyntaxHighlighter)({
  padding: 0,
  width: "100%",
  overflowX: "auto",
  overflowY: "hidden",
}) as any;

export class TextFileDocumentView extends React.Component<
  Props,
  { text: string; isLoading: boolean; language: string }
> {
  constructor(props) {
    super(props);
    this.state = { text: "", isLoading: false, language: getLanguage("") };
  }

  async componentDidMount() {
    if (this.props.path && this.props.repoPath) {
      this.loadFile(this.props);
    }
  }

  async componentWillReceiveProps(nextProps: Props) {
    if (this.props.path !== nextProps.path) {
      this.loadFile(nextProps);
    }
  }

  async loadFile(props: Props) {
    this.setState({
      isLoading: true,
    });

    const text = props.commit
      ? await getCommitFile(props.repoPath, props.commit, props.path)
      : (await getPathFile(props.repoPath, props.path)).toString();
    const language = getLanguage(props.path);

    this.setState({ text, isLoading: false, language });
  }
  renderer = ({ rows, stylesheet, useInlineStyles }) => {
    const generateCodeElement = (node, i) =>
      createElement({
        node,
        stylesheet,
        useInlineStyles,
        key: `code-segement${i}`,
      });

    return rows.map((node, i) => <Line>{generateCodeElement(node, i)}</Line>);
  };

  render() {
    if (this.state.isLoading) {
      return (
        <div style={{ height: "100%", width: "100%" }}>
          <NonIdealState title="Loading file" visual={<Spinner />} />
        </div>
      );
    }

    const { text, language } = this.state;

    return (
      <StyledSyntaxHighlighter showLineNumbers language={language} style={docco} renderer={this.renderer}>
        {text}
      </StyledSyntaxHighlighter>
    );
  }
}
