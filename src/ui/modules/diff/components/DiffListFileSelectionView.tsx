import * as React from "react";
import glamorous from "glamorous";
import { FileLabel } from "./SharedComponents";
import { Colors } from "@blueprintjs/core";
import { List, AutoSizer } from "react-virtualized";
import { FileStatus } from "../diff.models";
import { css } from "glamor";

interface DiffListFileSelectionViewProps {
  files: FileStatus[];
  labelComponent?: any;
  selectedFile?: FileStatus;
  onFileClick(diff: FileStatus): void;
}

const Container = glamorous.div({
  flex: 1,
});

const FileRow = glamorous.div(
  {
    "&:hover": {
      backgroundColor: Colors.LIGHT_GRAY3,
    },
    cursor: "pointer",
  },
  ({ isSelected }: { isSelected: boolean }) => ({
    backgroundColor: isSelected ? "#e6f2fd" : "transparent",
  }),
);

const FileRowLabel = glamorous(FileLabel)({
  lineHeight: "30px",
  marginLeft: "5px",
});

const noFocusClass = css({
  "&:focus": {
    outline: "transparent",
    outlineOffset: "0px",
  },
});

const FileContainer = glamorous.div({
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
  width: "100%",
});

export class DiffListFileSelectionView extends React.Component<DiffListFileSelectionViewProps, any> {
  list: List | null;
  state = {
    selectedIndex: -1,
  };

  // todo: refactor later
  selectIndex = (index: number) => {
    this.setState({
      selectedIndex: index,
    });

    this.props.onFileClick(this.props.files[index]);

    if (this.list) {
      this.list.forceUpdateGrid();
    }
  };

  renderFile = ({ index, isScrolling, key, style }) => {
    const { files } = this.props;
    const file = files[index];

    return (
      <FileRow
        key={key}
        style={style}
        isSelected={index === this.state.selectedIndex}
        onClick={() => this.selectIndex(index)}
      >
        <FileContainer>
          <FileRowLabel file={file} title={file.path} />
          {this.props.labelComponent ? this.props.labelComponent(file) : undefined}
        </FileContainer>
      </FileRow>
    );
  };

  render() {
    const { files } = this.props;

    return (
      <AutoSizer>
        {({ height, width }) => (
          <List
            ref={list => (this.list = list)}
            className={noFocusClass.toString()}
            width={width}
            height={height}
            rowHeight={30}
            rowCount={files.length}
            rowRenderer={this.renderFile}
          />
        )}
      </AutoSizer>
    );
  }
}
