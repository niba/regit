import * as React from "react";
import glamorous from "glamorous";
import { createElement } from "react-syntax-highlighter";
import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/styles";
import { getLanguage } from "../diff.utils";
import { FileStatus, DiffLineStatus, DiffLine, DiffChunk } from "../diff.models";

const addedColor = "#e6ffed";
const removedColor = "#ffeef0";

const lineMappings: { [P in DiffLineStatus]: { color: string; prefix: string } } = {
  added: { color: addedColor, prefix: "+" },
  removed: { color: removedColor, prefix: "-" },
  context: { color: "", prefix: "" },
};

const TextView = glamorous.div({
  display: "flex",
  flexDirection: "column",
  width: "100%",
});

const LineNumberSingleColumn = glamorous.div({
  color: "gray",
  display: "flex",
  flexDirection: "row",
});
const LineNumberSingle = glamorous.span({
  width: "50px",
  display: "flex",
  justifyContent: "center",
});

const Line = glamorous.div(
  {
    display: "flex",
    whiteSpace: "pre",
  },
  ({ line }: { line: DiffLine }) => ({
    backgroundColor: lineMappings[line.status].color,
  }),
);

const StyledSyntaxHighlighter = glamorous(SyntaxHighlighter)({
  width: "100%",
  overflowX: "auto",
  overflowY: "hidden",
}) as any;

export class DiffTextDocumentView extends React.Component<{ fileDiff: DiffChunk[]; file: FileStatus }, any> {
  renderLineNumberColumns(line: DiffLine) {
    return (
      <LineNumberSingleColumn>
        <LineNumberSingle>{line.from === -1 ? "" : line.from}</LineNumberSingle>
        <LineNumberSingle>{line.to === -1 ? "" : line.to}</LineNumberSingle>
      </LineNumberSingleColumn>
    );
  }

  renderer = (lines: DiffLine[]) => ({ rows, stylesheet, useInlineStyles }) => {
    const generateCodeElement = (node, i) =>
      createElement({
        node,
        stylesheet,
        useInlineStyles,
        key: `code-segement${i}`,
      });

    return rows.map((node, i) => {
      const line = lines[i];
      if (!line) {
        return undefined;
      }

      return (
        <Line line={line}>
          {this.renderLineNumberColumns(line)}
          {generateCodeElement(node, i)}
        </Line>
      );
    });
  };

  renderChunk(chunk: DiffChunk) {
    const allLines = chunk.lines.map(line => lineMappings[line.status].prefix + line.content).join("");
    return (
      <StyledSyntaxHighlighter
        language={getLanguage(this.props.file.path)}
        style={docco}
        renderer={data => this.renderer(chunk.lines)(data)}
      >
        {allLines}
      </StyledSyntaxHighlighter>
    );
  }

  render() {
    const { fileDiff } = this.props;
    return <TextView>{fileDiff.map(ch => this.renderChunk(ch))}</TextView>;
  }
}
