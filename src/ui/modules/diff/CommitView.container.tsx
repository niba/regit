import * as React from "react";
import { Icon, MenuDivider, Spinner, IconName, NonIdealState } from "@blueprintjs/core";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { callableActions } from "./diff.actions";
import glamorous from "glamorous";
import { Panel } from "ui/modules/common/components/Panel";
import { getSelectedCommit } from "ui/modules/graph/graph.selectors";
import { FileStatus } from "./diff.models";
import { FilesView } from "./components/FilesView";

const DiffPanel = glamorous(Panel)({
  display: "flex",
  flexDirection: "column",
  fontSize: "12px",
  overflowY: "auto",
  width: "100%",
});
const CommitInfoPanel = glamorous.div({
  display: "flex",
  width: "100%",
  padding: "10px",
});

const Card = glamorous.div({
  display: "flex",
  flexDirection: "column",
  width: "100%",
  marginTop: "15px",
});

const HeaderRow = glamorous.div({
  textAlign: "left",
  fontWeight: "bold",
  width: "100%",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
  fontSize: "18px",
  lineHeight: "22px",
  marginBottom: "15px",
  overflow: "hidden",
});

const DetailsRow = glamorous.div({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  "& > div": {
    marginRight: "10px",
  },
  fontSize: 16,
});

const CommitDescriptionRow = glamorous.div({
  marginTop: "15px",
  maxHeight: 100,
  overflowY: "auto",
  display: "flex",
  flexWrap: "wrap",
  width: "100%",
});

const TextWithIcon: React.StatelessComponent<{ icon: IconName; text: string }> = ({ icon, text }) => {
  return (
    <div>
      <Icon iconName={icon} />
      <span style={{ color: "#6a747b", fontSize: "15px" }}> {text} </span>
    </div>
  );
};

class CommitView extends React.Component<ConnectProps> {
  handleFileClick = (file: FileStatus) => {
    this.props.loadCommitFileDiff(file);
  };

  renderCommitInfo = () => {
    const { commit } = this.props;
    if (!commit) return <div />;

    return (
      <Card>
        <HeaderRow>{commit.headerMessage}</HeaderRow>
        <DetailsRow>
          <TextWithIcon icon="pt-icon-user" text={commit.author} />
          <TextWithIcon icon="pt-icon-time" text={commit.date.toLocaleString()} />
          <TextWithIcon icon="pt-icon-git-commit" text={commit.sha.substring(0, 6)} />
        </DetailsRow>
        <CommitDescriptionRow>
          {commit.headerMessage}
          <br />
          <br />
          {commit.message}
        </CommitDescriptionRow>
      </Card>
    );
  };

  render() {
    if (this.props.loadingCommitFiles) {
      return (
        <DiffPanel>
          <NonIdealState title="Loading" visual={<Spinner />} />
        </DiffPanel>
      );
    }
    return (
      <DiffPanel>
        <CommitInfoPanel>{this.renderCommitInfo()}</CommitInfoPanel>
        <MenuDivider />
        <FilesView files={this.props.commitFiles} handleFileClick={this.handleFileClick} />
      </DiffPanel>
    );
  }
}

const connectCreator = connect(
  (state: ApplicationState) => ({ ...state.diff, commit: getSelectedCommit(state) }),
  callableActions,
);
type ConnectProps = typeof connectCreator.allProps;
export const CommitViewContainer = connectCreator(CommitView);
