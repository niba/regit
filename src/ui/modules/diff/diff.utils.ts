export const languageMappings = {
  ts: "typescript",
  tsx: "typescript",
  cs: "cs",
  js: "javascript",
  jsx: "javascript",
  html: "html",
  java: "java",
  text: "tex",
  php: "php",
  xml: "xml",
  json: "json",
};

const defaultLanguage = "javascript";

const getExtension = (fileName: string) => fileName.split(".").pop();

export const getLanguage = (fileName: string) => {
  const ext = getExtension(fileName);
  if (!ext) {
    return defaultLanguage;
  }

  const lang = languageMappings[ext];

  if (!lang) {
    return defaultLanguage;
  }

  return lang;
};
