import actionCreatorFactory from "typescript-fsa";
import {
  getStartedActions,
  enhanceActionCreatorFactory,
  AsyncActionCreatorWithHandler,
  getActionsArray,
} from "ui/modules/common/actionsEnhancements";
import { FileStatus, WorkdirFileStatus, DiffChunk, MakeCommitParams } from "./diff.models";

const actionCreator = enhanceActionCreatorFactory(actionCreatorFactory("DIFF_MODULE"));

const actionCreators = {
  setCommitSummary: actionCreator<string>("SET_COMMIT_SUMMARY"),
  setCommitDescription: actionCreator<string>("SET_COMMIT_DETAILS"),
};

const asyncActionCreators = {
  loadWorkspaceFiles: actionCreator.async<{}, WorkdirFileStatus[]>("LOAD_WORKSPACE_FILES"),
  loadCommitFiles: actionCreator.async<any, FileStatus[]>("LOAD_COMMIT_FILES"),
  loadWorkspaceFileDiff: actionCreator.async<WorkdirFileStatus, DiffChunk[]>("LOAD_WORKSPACE_FILE_DIFF"),
  loadCommitFileDiff: actionCreator.async<FileStatus, DiffChunk[]>("LOAD_COMMIT_FILE_DIFF"),
};

const asyncActionCreatorsWithHandlers = {
  makeCommit: actionCreator.asyncWithHandlers<MakeCommitParams, void>("MAKE_COMMIT"),
  unstageFile: actionCreator.asyncWithHandlers<WorkdirFileStatus, void>("UNSTAGE_FILE"),
  unstageAll: actionCreator.asyncWithHandlers<WorkdirFileStatus[], void>("UNSTAGE_ALL"),
  stageAll: actionCreator.asyncWithHandlers<WorkdirFileStatus[], void>("STAGE_ALL"),
  stageFile: actionCreator.asyncWithHandlers<WorkdirFileStatus, void>("STAGE_FILE"),
};

export const actions = {
  ...actionCreators,
  ...asyncActionCreators,
  ...asyncActionCreatorsWithHandlers,
};

export const callableActions = {
  ...actionCreators,
  ...getStartedActions(asyncActionCreators),
  ...getStartedActions(asyncActionCreatorsWithHandlers),
};

export const notifiableActions: AsyncActionCreatorWithHandler<any, any, any>[] = getActionsArray(
  asyncActionCreatorsWithHandlers,
);
