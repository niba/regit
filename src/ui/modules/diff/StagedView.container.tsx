import * as React from "react";
import { Classes, Intent, Button } from "@blueprintjs/core";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { callableActions } from "./diff.actions";
import glamorous from "glamorous";
import { Panel } from "ui/modules/common/components/Panel";
import { getStagedFilesSelector, getUnstagedFilesSelector } from "ui/modules/diff/diff.selectors";
import { FilesView } from "./components/FilesView";
import { FileStatus, WorkdirFileStatus } from "./diff.models";
import { StagedCommitPanel } from "./components/StagedCommitPanel";
import * as commonSelectors from "ui/modules/common/common.selectors";

const { loadedRepositorySelector } = commonSelectors;

const StagedPanel = glamorous(Panel)({
  display: "flex",
  flexDirection: "column",
  overflowY: "auto",
  fontSize: "12px",
  width: "100%",
});

class StagedView extends React.Component<ConnectProps> {
  componentDidMount() {
    if (this.props.repo) {
      this.props.loadWorkspaceFiles({});
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.repo !== this.props.repo) {
      this.props.loadWorkspaceFiles({});
    }
  }

  onFileClick = (file: FileStatus) => {
    this.props.loadWorkspaceFileDiff(file as WorkdirFileStatus);
  };

  stageAllFiles = () => {
    this.props.stageAll(this.props.unstagedFiles);
  };

  unstageAllFiles = () => {
    this.props.unstageAll(this.props.stagedFiles);
  };

  render() {
    return (
      <StagedPanel>
        <FilesView
          title="STAGED"
          files={this.props.stagedFiles}
          selectedFile={this.props.selectedFile}
          handleFileClick={this.onFileClick}
          actionButtonIntent={Intent.DANGER}
          actionButtonText="Unstage All"
          actionButtonOnClick={this.unstageAllFiles}
          hasActionButton
          labelComponent={file => (
            <Button
              intent={Intent.DANGER}
              className={Classes.MINIMAL}
              text="Unstage"
              onClick={e => {
                this.props.unstageFile(file);
                e.stopPropagation();
              }}
            />
          )}
        />
        <FilesView
          title="UNSTAGED"
          files={this.props.unstagedFiles}
          selectedFile={this.props.selectedFile}
          handleFileClick={this.onFileClick}
          actionButtonIntent={Intent.SUCCESS}
          actionButtonText="Stage All"
          actionButtonOnClick={this.stageAllFiles}
          hasActionButton
          labelComponent={file => (
            <Button
              intent={Intent.SUCCESS}
              className={Classes.MINIMAL}
              text="Stage"
              onClick={e => {
                this.props.stageFile(file);
                e.stopPropagation();
              }}
            />
          )}
        />
        <StagedCommitPanel
          onCommit={this.props.makeCommit}
          {...this.props.commitPanel}
          setCommitDescription={this.props.setCommitDescription}
          setCommitSummary={this.props.setCommitSummary}
        />
      </StagedPanel>
    );
  }
}

const connectCreator = connect(
  (state: ApplicationState) => ({
    selectedFile: state.diff.selectedFile,
    commitPanel: state.diff.commitPanel,
    repo: loadedRepositorySelector(state),
    stagedFiles: getStagedFilesSelector(state),
    unstagedFiles: getUnstagedFilesSelector(state),
  }),
  callableActions,
);
type ConnectProps = typeof connectCreator.allProps;
export const StagedViewContainer = connectCreator(StagedView);
