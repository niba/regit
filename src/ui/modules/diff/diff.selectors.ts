import { createSelector } from "reselect";
import { ApplicationState } from "ui/modules/app.reducers";

const workdirFilesSelector = (state: ApplicationState) => state.diff.workdirFiles;

export const getStagedFilesSelector = createSelector(workdirFilesSelector, workdirFiles =>
  workdirFiles.filter(workdirFile => workdirFile.staging === "staged"),
);

export const getUnstagedFilesSelector = createSelector(workdirFilesSelector, workdirFiles =>
  workdirFiles.filter(workdirFile => workdirFile.staging === "unstaged"),
);
