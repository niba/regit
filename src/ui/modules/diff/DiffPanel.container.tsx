import * as React from "react";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { StagedViewContainer } from "./StagedView.container";
import { getSelectedCommit } from "ui/modules/graph/graph.selectors";
import { CommitViewContainer } from "./CommitView.container";

class DiffPanel extends React.Component<ConnectProps> {
  render() {
    if (!this.props.commit) {
      return <StagedViewContainer />;
    }

    return <CommitViewContainer />;
  }
}

const connectCreator = connect((state: ApplicationState) => ({ commit: getSelectedCommit(state) }));
type ConnectProps = typeof connectCreator.allProps;
export const DiffPanelContainer = connectCreator(DiffPanel);
