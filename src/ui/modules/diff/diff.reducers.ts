import { actions } from "./diff.actions";
import { reducerWithInitialState } from "typescript-fsa-reducers";
import { FileStatus, WorkdirFileStatus } from "./diff.models";
import { DiffChunk } from "./diff.models";

export interface DiffState {
  selectedDiff?: DiffChunk[];
  selectedFile?: FileStatus;
  loadingDiff: boolean;
  commitFiles: FileStatus[];
  workdirFiles: WorkdirFileStatus[];
  loadingCommitFiles: boolean;
  commitPanel: {
    summary: string;
    description: string;
    isComitting: boolean;
  };
}

const defaultState: DiffState = {
  loadingCommitFiles: false,
  workdirFiles: [],
  commitFiles: [],
  loadingDiff: false,
  commitPanel: { summary: "", description: "", isComitting: false },
};

export const diffReducer = reducerWithInitialState(defaultState)
  .case(actions.loadCommitFiles.done, (state, payload) => ({
    ...state,
    commitFiles: payload.result.slice(),
    loadingCommitFiles: false,
  }))
  .case(actions.loadCommitFiles.started, (state, payload) => ({
    ...state,
    loadingCommitFiles: true,
  }))
  .case(actions.loadCommitFileDiff.started, (state, payload) => ({
    ...state,
    loadingDiff: true,
  }))
  .case(actions.loadWorkspaceFileDiff.started, (state, payload) => ({
    ...state,
    loadingDiff: true,
  }))
  .case(actions.loadCommitFileDiff.done, (state, payload) => ({
    ...state,
    loadingDiff: false,
    selectedDiff: payload.result,
    selectedFile: payload.params,
  }))
  .case(actions.loadWorkspaceFileDiff.done, (state, payload) => ({
    ...state,
    loadingDiff: false,
    selectedDiff: payload.result,
    selectedFile: payload.params,
  }))
  .case(actions.makeCommit.started, (state, payload) => ({
    ...state,
    commitPanel: {
      ...state.commitPanel,
      isComitting: true,
    },
  }))
  .case(actions.makeCommit.done, (state, payload) => ({
    ...state,
    commitPanel: {
      ...state.commitPanel,
      summary: "",
      description: "",
      isComitting: false,
    },
  }))
  .case(actions.makeCommit.failed, (state, payload) => ({
    ...state,
    commitPanel: {
      ...state.commitPanel,
      isComitting: false,
    },
  }))
  .case(actions.setCommitSummary, (state, payload) => ({
    ...state,
    commitPanel: {
      ...state.commitPanel,
      summary: payload,
    },
  }))
  .case(actions.setCommitDescription, (state, payload) => ({
    ...state,
    commitPanel: {
      ...state.commitPanel,
      description: payload,
    },
  }))
  .case(actions.loadWorkspaceFiles.done, (state, payload) => ({
    ...state,
    workdirFiles: payload.result.slice(),
  }));
