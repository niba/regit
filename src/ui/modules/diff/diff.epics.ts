import { actions } from "./diff.actions";
import { Action } from "redux";
import { diffWithParent } from "../../gitCommands/diff";
import { makeAsyncEpic } from "../common/makeAsyncEpic";
import { ApplicationState } from "ui/modules/app.reducers";
import { ActionsObservable } from "redux-observable";
import { graphActions } from "../graph";
import { push } from "react-router-redux";
import { getSelectedCommit } from "ui/modules/graph/graph.selectors";
import { mapStatus, mergeStatusAndPatch, mapPatch, getHunks } from "./diff.models";
import { getFileStatuses, getStagedFilesDiff, getUnstagedFilesDiff } from "ui/gitCommands/status";
import { addFiles, removeFiles, resetFiles } from "ui/gitCommands/add";
import { actionBarActions } from "ui/modules/actionBar/";
import { makeCommit } from "ui/gitCommands/commit";
import { commonActionCreators } from "ui/modules/common/common.actions";
import { getStagedFilesSelector, getUnstagedFilesSelector } from "ui/modules/diff/diff.selectors";
import { showWarning, showError } from "ui/services/toastService";
import * as commonSelectors from "ui/modules/common/common.selectors";
import { RegitError, ErrorType } from "ui/modules/common/error";

const repositoryExistsPredicate = (action, state: ApplicationState) =>
  commonSelectors.loadedRepositorySelector(state) !== undefined;

const loadCommitFiles = async (state: ApplicationState) => {
  const repository = await commonSelectors.getCurrentRepository(state);
  const selectedCommit = getSelectedCommit(state);
  if (!selectedCommit) return [];

  const commit = await repository.getCommit(selectedCommit.sha);
  const fileDiffs = await diffWithParent(repository, commit);

  return fileDiffs.map(mapPatch);
};

export const loadCommitFileDiff = makeAsyncEpic(actions.loadCommitFileDiff, async (payload, state) => {
  const repository = await commonSelectors.getCurrentRepository(state);
  const selectedCommit = getSelectedCommit(state);
  if (!selectedCommit) {
    throw new Error("Selected commit empty");
  }

  const commit = await repository.getCommit(selectedCommit.sha);
  const fileDiffs = await diffWithParent(repository, commit, [payload.path]);

  return getHunks(fileDiffs[0]);
});

export const loadWorkspaceFileDiff = makeAsyncEpic(actions.loadWorkspaceFileDiff, async (payload, state) => {
  const repository = await commonSelectors.getCurrentRepository(state);

  const paths = [payload.path];
  const fileDiffs =
    payload.staging === "staged"
      ? await getStagedFilesDiff(repository, paths)
      : await getUnstagedFilesDiff(repository, paths);

  return getHunks(fileDiffs[0]);
});

export const loadCommitFilesEpic = makeAsyncEpic(
  actions.loadCommitFiles,
  (p, s) => loadCommitFiles(s),
  repositoryExistsPredicate,
);

export const showDiffEpic = (action$: ActionsObservable<Action>) =>
  action$
    .ofType(actions.loadWorkspaceFileDiff.started.type, actions.loadCommitFileDiff.started.type)
    .map(_ => push("/diff"));

export const refreshDiffsEpic = (action$: ActionsObservable<Action>) =>
  action$.filter(graphActions.selectCommit.match).map(_ => actions.loadCommitFiles.started({}));

export const loadStagedFilesEpic = makeAsyncEpic(
  actions.loadWorkspaceFiles,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    const statuses = (await getFileStatuses(repository)).map(mapStatus);
    const stagedPaths = statuses.filter(status => status.staging === "staged").map(status => status.path);
    const unstagedPaths = statuses.filter(status => status.staging === "unstaged").map(status => status.path);

    const stagedFilesDiff = await getStagedFilesDiff(repository, stagedPaths);
    const unstagedFilesDiff = await getUnstagedFilesDiff(repository, unstagedPaths);

    const workdirFiles = mergeStatusAndPatch(statuses, stagedFilesDiff.concat(unstagedFilesDiff));
    return workdirFiles;
  },
  repositoryExistsPredicate,
);

export const stageFileEpic = makeAsyncEpic(
  actions.stageFile,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    await addFiles(repository, [payload]);
  },
  repositoryExistsPredicate,
);

export const unstageFileEpic = makeAsyncEpic(
  actions.unstageFile,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    await removeFiles(repository, [payload]);
  },
  repositoryExistsPredicate,
);

export const stageAllEpic = makeAsyncEpic(
  actions.stageAll,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    await addFiles(repository, payload);
  },
  repositoryExistsPredicate,
);

export const unstageAllEpic = makeAsyncEpic(
  actions.unstageAll,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    await resetFiles(repository, payload.map(file => file.path));
  },
  repositoryExistsPredicate,
);

export const refreshStaged = (action$: ActionsObservable<Action>) =>
  action$
    .ofType(
      actions.stageAll.done.type,
      actions.stageFile.done.type,
      actions.unstageFile.done.type,
      actions.unstageAll.done.type,
      actions.makeCommit.done.type,
      commonActionCreators.refresh.done.type,
      actionBarActions.stashPush.done.type,
      actionBarActions.stashPop.done.type,
    )
    .map(_ => {
      return actions.loadWorkspaceFiles.started({});
    });

export const makeCommitEpic = makeAsyncEpic(
  actions.makeCommit,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    const stagedFiles = getStagedFilesSelector(state);
    const unstagedFiles = getUnstagedFilesSelector(state);
    if (payload.title === "") {
      const message = "Aborting commit due to empty commit message.";
      throw new RegitError(message, ErrorType.Warning);
    }
    if (stagedFiles.length === 0) {
      const message = unstagedFiles.length
        ? "No changes added to commit"
        : "Nothing to commit, working directory clean";
      throw new Error(message);
    }
    const fullMessage = payload.title + "\n" + payload.description;
    await makeCommit(repository, fullMessage);
  },
  repositoryExistsPredicate,
);
