import * as React from "react";
import { Classes, IconClasses, Button } from "@blueprintjs/core";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import glamorous from "glamorous";
import { getSelectedCommit } from "ui/modules/graph/graph.selectors";
import { graphCallableActions } from "ui/modules/graph";
import classNames from "classnames";

const WorkingDirButton = glamorous(Button)({
  width: "120px",
});

const WorkingDirButtonContainer = glamorous.div(
  {
    backgroundColor: "#fafbfc",
    position: "fixed",
    zIndex: 50,
    opacity: 1,
    height: "52px",
    width: "120px",
    transition: "transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms",
    right: 0,
    top: 0,
    transform: "translate(0px)",
  },
  ({ selectedCommit }: { selectedCommit: any }) => {
    if (!selectedCommit) {
      return {
        transform: "translate(150px)",
      };
    }

    return {};
  },
);

class WorkingDir extends React.Component<ConnectProps> {
  render() {
    return (
      <WorkingDirButtonContainer selectedCommit={this.props.selectedCommit}>
        {this.props.selectedCommit ? (
          <WorkingDirButton
            style={{
              height: "50px",
            }}
            className={classNames(Classes.MINIMAL)}
            iconName={IconClasses.LOG_OUT}
            text="Working Dir"
            onClick={() => this.props.unselectCommit()}
          />
        ) : (
          undefined
        )}
      </WorkingDirButtonContainer>
    );
  }
}

const connectCreator = connect(
  (state: ApplicationState) => ({
    selectedFile: state.diff.selectedFile,
    selectedCommit: getSelectedCommit(state),
  }),
  {
    unselectCommit: graphCallableActions.unselectCommit,
  },
);
type ConnectProps = typeof connectCreator.allProps;
export const WorkingDirContainer = connectCreator(WorkingDir);
