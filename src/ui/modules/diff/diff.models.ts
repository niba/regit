import nodegit from "nodegit";
import { PlainObject } from "ui/modules/common/common.models";

export enum FILES_VIEWS_INSTANCES {
  COMMIT_DIFF,
  STAGED_FILES,
  UNSTAGED_FILES,
}
export interface MakeCommitParams {
  title: string;
  description: string;
}
export interface DiffChunk {
  lines: DiffLine[];
  oldLines: number;
  newLines: number;
}

export type DiffLineStatus = "added" | "removed" | "context";

export interface DiffLine {
  from: number;
  to: number;
  content: string;
  status: DiffLineStatus;
}

export type FileStatusType = "added" | "deleted" | "modified" | "renamed" | "conflicted" | "unknown";

export type StagingType = "staged" | "unstaged";
export type FileLineStats = { lineStats: { added: number; removed: number } };
export type FileBaseInfo = { path: string; status: FileStatusType; oldPath?: string };
export type FileStatus = FileBaseInfo & FileLineStats;
export type WorkdirFileStatus = FileStatus & { staging: StagingType };

const createFileStatus = (fileInfo: FileBaseInfo, fileLineStats?: FileLineStats): FileStatus => {
  return Object.assign({}, fileInfo, { lineStats: { added: 0, removed: 0 } }, fileLineStats);
};

export const getHunks = async (patch: nodegit.ConvenientPatch): Promise<DiffChunk[]> => {
  const hunks = await patch.hunks();

  const getHunk = async (hunk): Promise<DiffChunk> => {
    const lines = (await hunk.lines()).map((line, i) => ({
      from: line.oldLineno(),
      to: line.newLineno(),
      content: line.content(),
      status: line.oldLineno() === -1 ? "added" : line.newLineno() === -1 ? "removed" : "context",
    }));

    return {
      lines,
      oldLines: hunk.oldLines(),
      newLines: hunk.newLines(),
    };
  };

  return Promise.all(hunks.map(getHunk));
};

export const mergeStatusAndPatch = (statuses: WorkdirFileStatus[], patches: nodegit.ConvenientPatch[]) => {
  const modifiedPatches = patches.filter(patchFilter);
  const cache: PlainObject<FileStatus> = {};
  const workdirFiles: WorkdirFileStatus[] = [];

  modifiedPatches.forEach(patch => {
    const fileStatus = mapPatch(patch);
    cache[fileStatus.path] = fileStatus;
  });

  statuses.forEach(status => {
    if (cache[status.path]) {
      workdirFiles.push({ ...cache[status.path], staging: status.staging });
    }
  });

  return workdirFiles;
};

export const mapStatus = (file): WorkdirFileStatus => {
  let status: FileStatusType = "unknown";

  if (file.isNew()) {
    status = "added";
  } else if (file.isModified()) {
    status = "modified";
  } else if (file.isDeleted()) {
    status = "deleted";
  } else if (file.isRenamed()) {
    status = "renamed";
  }

  // https://github.com/libgit2/libgit2/blob/89c332e41b12a72d89de40d63bc568c56a2c336a/tests/status/status_data.h
  const statuses: string[] = file.status();
  const staging: StagingType = statuses.findIndex(element => element.includes("WT")) >= 0 ? "unstaged" : "staged";

  const path = file.path();

  return { ...createFileStatus({ path, status }), staging };
};

const patchFilter = (patch: nodegit.ConvenientPatch) => !patch.isUnmodified();

export const mapPatch = (patch: nodegit.ConvenientPatch): FileStatus => {
  let status: FileStatusType = "unknown";
  if (patch.isAdded() || patch.isUntracked()) {
    status = "added";
  } else if (patch.isDeleted()) {
    status = "deleted";
  } else if (patch.isModified()) {
    status = "modified";
  } else if (patch.isRenamed()) {
    status = "renamed";
  } else if (patch.isConflicted()) {
    status = "conflicted";
  }

  const oldPath = status === "renamed" ? patch.oldFile().path() : undefined;
  const path = patch.newFile().path();
  const pathLineStats = patch.lineStats();
  const lineStats = {
    added: pathLineStats["total_additions"],
    removed: pathLineStats["total_deletions"],
  };

  return createFileStatus({ status, path, oldPath }, { lineStats });
};
