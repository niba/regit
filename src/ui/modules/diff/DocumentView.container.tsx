import * as React from "react";
import { FormEvent } from "react";
import { Classes, MenuDivider, Button, IconClasses, NonIdealState, Spinner } from "@blueprintjs/core";
import { DiffTextDocumentView } from "./components/DiffTextDocumentView";
import { TextFileDocumentView } from "./components/TextFileDocumentView";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import glamorous from "glamorous";
import { getSelectedCommit } from "ui/modules/graph/graph.selectors";
import { Commit } from "ui/modules/common/common.models";
import { DiffChunk, FileStatus } from "./diff.models";
import classNames from "classnames";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import * as path from "path";
import { loadedRepositorySelector } from "ui/modules/common/common.selectors";

type DisplayTypeSelection = "diff" | "text";

const View = glamorous.div({
  overflow: "auto",
  padding: "0px",
  width: "100%",
});

const Content = glamorous.div({
  display: "flex",
  flex: "1",
  width: "100%",
  height: "auto",
  overflowX: "auto",
});

const HeaderBar = glamorous.div({
  display: "flex",
  height: "50px",
  width: "100%",
  alignItems: "center",
});

const { Div } = glamorous;

const HeaderTitle = glamorous.h4({
  color: "#6a747b",
  margin: 0,
  textOverflow: "ellipsis",
  overflow: "hidden",
  whiteSpace: "nowrap",
  lineHeight: "35px",
});

class DocumentView extends React.Component<
  ConnectProps & { location: string } & { history?: any },
  { displayTypeSelection: DisplayTypeSelection }
> {
  private selectionMappings: {
    [P in DisplayTypeSelection]: (
      selectedDiff: DiffChunk[],
      selectedFile: FileStatus,
      commit: Commit | undefined,
      repoPath: string,
    ) => JSX.Element
  } = {
    diff: (selectedDiff, selectedFile) => <DiffTextDocumentView fileDiff={selectedDiff} file={selectedFile} />,
    text: (selectedDiff, selectedFile, commit, repoPath) => (
      <TextFileDocumentView commit={commit} path={selectedFile.path} repoPath={repoPath} />
    ),
  };

  constructor(props) {
    super(props);

    this.state = { displayTypeSelection: "diff" };
  }

  handleDisplayViewChange = (event: FormEvent<HTMLInputElement>) => {
    this.setState({ displayTypeSelection: event.currentTarget.value as DisplayTypeSelection });
  };

  selectDiff = () => {
    this.setState({ displayTypeSelection: "diff" });
  };

  selectText = () => {
    this.setState({ displayTypeSelection: "text" });
  };

  render() {
    if (this.props.loadingDiff) {
      return (
        <View>
          <NonIdealState title="Loading file" visual={<Spinner />} />
        </View>
      );
    }

    if (!this.props.selectedFile) {
      this.props.history.push("/");
      return (
        <View>
          <NonIdealState title="No file" visual={IconClasses.DOCUMENT} />
        </View>
      );
    }

    if (!this.props.selectedDiff || !this.props.currentRepo) {
      return (
        <View>
          <NonIdealState title="No file" visual={IconClasses.DOCUMENT} />
        </View>
      );
    }

    const isDiffMode = this.state.displayTypeSelection === "diff";
    return (
      <View>
        <HeaderBar>
          <Div display="flex" flex="0.5" alignItems="center" justifyContent="flex-start">
            <div className={classNames(Classes.BUTTON_GROUP, Classes.MINIMAL)} style={{ marginLeft: "20px" }}>
              <Button text="Diff" iconName={IconClasses.TH_LIST} onClick={this.selectDiff} active={isDiffMode} />
              <Button text="Text" iconName={IconClasses.LABEL} onClick={this.selectText} active={!isDiffMode} />
            </div>
          </Div>
          <Div display="flex" flex="1" alignItems="center" justifyContent="center" overflow="hidden">
            <HeaderTitle> {path.basename(this.props.selectedFile.path)} </HeaderTitle>
          </Div>
          <Div display="flex" flex="0.5" alignItems="center" justifyContent="flex-end">
            <div style={{ marginRight: "20px" }}>
              <Link to="/">
                <Button text="Close" iconName={IconClasses.CROSS} className={Classes.MINIMAL} />
              </Link>
            </div>
          </Div>
        </HeaderBar>
        <MenuDivider />
        <Content>
          {this.selectionMappings[this.state.displayTypeSelection](
            this.props.selectedDiff,
            this.props.selectedFile,
            this.props.commit,
            this.props.currentRepo,
          )}
        </Content>
      </View>
    );
  }
}

const connectCreator = connect((state: ApplicationState) => ({
  ...state.diff,
  commit: getSelectedCommit(state),
  currentRepo: loadedRepositorySelector(state),
}));
type ConnectProps = typeof connectCreator.allProps;
export const DocumentViewContainer = connectCreator(withRouter(DocumentView));
