import { ActionCreator } from "typescript-fsa";
import { isType } from "typescript-fsa";
import { AsyncActionCreatorWithHandler } from "ui/modules/common/actionsEnhancements";
import { actionBarNotifiableActions } from "ui/modules/actionBar";
import { branchNotifiableActions } from "ui/modules/branch";
import { diffNotifiableActions } from "ui/modules/diff";
import { graphNotifiableActions } from "ui/modules/graph";
import { repositoryNotifiableActions } from "ui/modules/repository";

export const notifiableActions: AsyncActionCreatorWithHandler<any, any, any>[] = [
  ...actionBarNotifiableActions,
  ...branchNotifiableActions,
  ...diffNotifiableActions,
  ...graphNotifiableActions,
  ...repositoryNotifiableActions,
];
