import { commonActionCreators } from "./common.actions";
import { repositoryActions } from "../repository";
import { ApplicationState } from "ui/modules/app.reducers";
import { ActionsObservable } from "redux-observable";
import { Action, Store } from "redux";
import { Observable } from "rxjs/Observable";
import { waitForAll } from "ui/modules/common/epicUtils";
import { loadedRepositorySelector } from "ui/modules/common/common.selectors";

export const refreshEpic = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) =>
  action$
    .filter(commonActionCreators.refresh.started.match)
    .map(_ => loadedRepositorySelector(store.getState()))
    .filter(repo => !!repo)
    .map(repo => repo as string)
    .switchMap(repo => {
      return Observable.merge(
        waitForAll(action$, repositoryActions.reloadRepo.done).map(() =>
          commonActionCreators.refresh.done({ params: {}, result: {} }),
        ),
        Observable.of(repositoryActions.reloadRepo.started({})),
      );
    });
