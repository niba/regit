import nodegit from "nodegit";
import { Commit } from "ui/modules/common/common.models";

export const getCommitMessage = (commit: nodegit.Commit) => {
  const messageChunks = commit.message().split("\n");

  return {
    headerMessage: messageChunks[0],
    message: messageChunks.length > 0 ? messageChunks.slice(1, messageChunks.length).join("\n") : "",
  };
};

export const mapCommit = (commit: nodegit.Commit): Commit => ({
  ...getCommitMessage(commit),
  sha: commit.sha(),
  author: commit.author().name(),
  date: commit.date(),
  parents: commit.parents().map(oid => oid.tostrS()),
});
