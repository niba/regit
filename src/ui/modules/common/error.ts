export enum ErrorType {
  Warning,
  Error,
}

export class RegitError extends Error {
  type: ErrorType;
  constructor(message, type: ErrorType = ErrorType.Error) {
    super(message);
    Object.setPrototypeOf(this, RegitError.prototype);
    this.name = this.constructor.name;
    this.type = type;
  }

  getType() {
    return this.type;
  }

  dump() {
    return { message: this.message, stack: this.stack };
  }
}
