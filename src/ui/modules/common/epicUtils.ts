import { Observable } from "rxjs/Observable";
import { ActionsObservable, combineEpics } from "redux-observable";
import { AsyncActionCreatorWithHandler } from "ui/modules/common/actionsEnhancements";
import { Action as FsaAction } from "typescript-fsa";
import { ActionCreator } from "typescript-fsa";
import { Action } from "redux";
import { Failure } from "typescript-fsa";
import { showWarning, showError, showSuccess } from "ui/services/toastService";
import { RegitError, ErrorType } from "ui/modules/common/error";
import { AsyncActionCreators } from "typescript-fsa";

export const forkEpic = (epicFactory, store, ...actions) => {
  const input$ = Observable.of(...actions);
  const actions$ = new ActionsObservable(input$);
  return epicFactory(actions$, store);
};

export const waitForAll = (action$, ...reduxActions): Observable<any[]> => {
  const obs = reduxActions.map(x => action$.ofType(x.type).take(1));
  return Observable.forkJoin(obs);
};
export const waitForAny = (action$, ...reduxActions): Observable<any> => {
  return action$.ofType(...reduxActions.map(x => x.type)).take(1);
};

interface WaitSuccess {
  type: "Success";
  actions: FsaAction<any>;
}
interface WaitError {
  type: "Error";
  error: any;
}

type ActionsType = AsyncActionCreators<any, any, any>[];

export const waitWithResult = (action$, ...actions: ActionsType): Observable<WaitSuccess | WaitError> => {
  const successObs = waitForAll(action$, ...actions.map(x => x.done)).map(action => ({
    actions,
    type: "Success",
  }));
  const failedObs = waitForAny(action$, ...actions.map(x => x.failed)).map(action => ({
    error: action.payload.error,
    type: "Error",
  }));

  return successObs.race(failedObs);
};

type Show = (message: string | JSX.Element) => void;

const showNotification = (message: string | JSX.Element | undefined, show: Show) => {
  if (message) show(message);
};

export const notifiableActionsEpic = (actionsCreators: AsyncActionCreatorWithHandler<any, any, any>[]) => {
  const createEpic = <P, S, E>(
    selector: (ac: AsyncActionCreatorWithHandler<P, S, E>) => ActionCreator<any>,
    show: (ac: AsyncActionCreatorWithHandler<P, S, E>, action: FsaAction<any>) => void,
  ) => {
    return (action$: ActionsObservable<Action>) =>
      action$
        .ofType(...actionsCreators.map(x => selector(x).type))
        .map(action => {
          const actionCreator = actionsCreators.find(x => selector(x).type === action.type);
          if (actionCreator) show(actionCreator, action as any);

          return false;
        })
        .filter(x => false);
  };

  const successEpic = createEpic(
    x => x.done,
    (ac, action) => {
      if (ac.successMessage) showNotification(ac.successMessage(action), showSuccess);
    },
  );

  const failedEpic = createEpic(
    x => x.failed,
    (ac, action: FsaAction<Failure<any, Error>>) => {
      const message = ac.failMessage ? ac.failMessage(action) : action.payload.error.toString();
      const show = isWarning(action.payload.error) ? showWarning : showError;
      showNotification(message, show);
    },
  );

  return combineEpics(successEpic, failedEpic);
};

const isWarning = (error: Error) => error instanceof RegitError && error.getType() === ErrorType.Warning;
