import { createSelector } from "reselect";
import { ApplicationState } from "ui/modules/app.reducers";
import { openRepository } from "ui/gitCommands";

export const openRepositoriesSelector = (state: ApplicationState) => state.repository.openRepositories;
export const loadedRepositoryIndexSelector = (state: ApplicationState) => state.repository.loadedRepositoryIndex;
export const cacheRepositorySelector = (state: ApplicationState) => state.cache;
export const getCurrentRepository = (state: ApplicationState) =>
  openRepository(loadedRepositorySelector(state) as string);

export const loadedRepositorySelector = createSelector(
  openRepositoriesSelector,
  loadedRepositoryIndexSelector,
  (openRepositories, loadedRepositoryIndex) =>
    loadedRepositoryIndex === -1 ? undefined : openRepositories[loadedRepositoryIndex],
);
