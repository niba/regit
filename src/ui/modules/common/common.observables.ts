import { ActionsObservable } from "redux-observable";
import { ApplicationState } from "ui/modules/app.reducers";
import { Action } from "redux";
import { Observable } from "rxjs/Observable";
import { Action as FsaAction } from "typescript-fsa";
import { repositoryActions } from "../repository";
import { Store } from "react-redux";
import { AsyncActionCreators } from "typescript-fsa";

export const authEpic = <P, S>(
  actionCreator: AsyncActionCreators<P, S, any>,
  onSuccessPromise: (payload: P, password?: string) => Promise<S>,
) => (action$: ActionsObservable<Action>, store: Store<ApplicationState>) => {
  const authFailedObs = (currAction: FsaAction<P>) =>
    action$
      .filter(repositoryActions.authenticate.failed.match)
      .take(1)
      .map(() => actionCreator.failed({ error: "Authentication failed", params: currAction.payload }));

  const waitForAuthObs = (currAction: FsaAction<P>) =>
    action$
      .filter(repositoryActions.authenticate.done.match)
      .take(1)
      .mergeMap(authAction => onSuccessPromise(currAction.payload, authAction.payload.result.password))
      .map(result => actionCreator.done({ result, params: currAction.payload }))
      .catch(error => Observable.of(actionCreator.failed({ error, params: currAction.payload })));

  const authObs = (currAction: FsaAction<P>) =>
    Observable.merge(
      Observable.of(repositoryActions.authenticate.started({})),
      waitForAuthObs(currAction).race(authFailedObs(currAction)),
    );

  return action$
    .filter(actionCreator.started.match)
    .mergeMap(async currAction => {
      try {
        const result = await onSuccessPromise(currAction.payload);
        return Observable.of(actionCreator.done({ result, params: currAction.payload }));
      } catch (error) {
        return error.message.includes("auth")
          ? authObs(currAction)
          : Observable.of(actionCreator.failed({ error, params: currAction.payload }));
      }
    })
    .mergeMap(obs => obs);
};
