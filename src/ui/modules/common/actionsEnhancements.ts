import { Success, Failure, ActionCreator, ActionCreatorFactory, Action } from "typescript-fsa";
import { RegitError, ErrorType } from "ui/modules/common/error";
import { AsyncActionCreators } from "typescript-fsa";

type SuccessMessageHandler<P, S> = (p: Action<Success<P, S>>) => string | JSX.Element | undefined;
type FailMessageHandler<P, E> = (p: Action<Failure<P, E>>) => string | JSX.Element | undefined;

type MessageHandlers<P, S> = {
  successMessage?: SuccessMessageHandler<P, S>;
  failMessage?: FailMessageHandler<P, Error>;
};

export type AsyncActionCreatorWithHandler<P, S, E> = AsyncActionCreators<P, S, E> & MessageHandlers<P, S>;

export function enhanceActionCreatorFactory(
  factory: ActionCreatorFactory,
): ActionCreatorFactory & { asyncWithHandlers: AsyncWithHandlers } {
  (factory as any).asyncWithHandlers = <P, S, E>(type: string, messageHandlers: MessageHandlers<P, S>) =>
    asyncWithHandlers(factory, type, messageHandlers);

  return factory as ActionCreatorFactory & { asyncWithHandlers: AsyncWithHandlers };
}

type AsyncWithHandlers = <P, S>(
  type: string,
  messageHandlers?: MessageHandlers<P, S>,
) => AsyncActionCreatorWithHandler<P, S, Error>;
export function asyncWithHandlers<P, S, E>(
  factory: ActionCreatorFactory,
  type: string,
  messageHandlers?: MessageHandlers<P, S>,
) {
  const actionCreatorNew = factory.async<P, S, Error>(type) as AsyncActionCreatorWithHandler<P, S, Error>;
  actionCreatorNew.successMessage = messageHandlers ? messageHandlers.successMessage : undefined;
  actionCreatorNew.failMessage = messageHandlers ? messageHandlers.failMessage : undefined;

  return actionCreatorNew;
}

type StartedActions<T extends { [k: string]: { started: any } }> = { [P in keyof T]: T[P]["started"] };

export function getStartedActions<T extends { [k: string]: { started: any } }>(actionCreators: T) {
  return Object.keys(actionCreators).reduce((result, currentKey) => {
    result[currentKey] = actionCreators[currentKey].started;
    return result;
  }, {}) as StartedActions<T>;
}

export function getActionsArray<T extends { [k: string]: AsyncActionCreatorWithHandler<any, any, any> }>(
  actionCreators: T,
) {
  return Object.keys(actionCreators).map(key => actionCreators[key]);
}
