import { AsyncActionCreators } from "typescript-fsa";
import { Success } from "typescript-fsa";
import { Failure } from "typescript-fsa";
import { ActionCreator } from "typescript-fsa";
import { Action } from "typescript-fsa";
import { ActionCreatorFactory } from "typescript-fsa";

export interface Commit {
  message: string;
  headerMessage: string;
  sha: string;
  date: Date;
  parents: string[];
  author: string;
}

export interface PlainObject<T> {
  [key: string]: T;
}

export type Reference = {
  name: string;
  friendlyName: string;
  target: string;
  isVisible: boolean;
  isRemote: boolean;
  isHead: boolean;
  isTag: boolean;
};

export type Branch = Reference;
export type Tag = Reference;
