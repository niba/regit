import actionCreatorFactory from "typescript-fsa";

const actionCreator = actionCreatorFactory("COMMON");

export const commonActionCreators = {
  refresh: actionCreator.async<{}, {}>("REFRESH"),
};

export const loadableActions = [];
