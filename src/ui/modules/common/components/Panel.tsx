import React from "react";
import glamorous from "glamorous";

export const Panel = glamorous(({ children, className }) => <div className={`${className}`}>{children}</div>)({
  backgroundColor: "#fafbfc",
  padding: "0px",
  borderRight: "2px solid #e1e4e8",
  borderLeft: "2px solid #e1e4e8",
  width: "100%",
});
