import * as React from "react";
import { AutoSizer, List, Grid, GridCellProps, CellMeasurer, CellMeasurerCache } from "react-virtualized";
import { ITreeNode, Icon, IconClasses, Classes } from "@blueprintjs/core";
import { FlatNode, flattenNodes } from "./VirtualizedTreeHelpers";
import glamorous from "glamorous";
import classNames from "classnames";

export interface ITreeData {
  path: string;
}

interface ITreeProps {
  nodes: ITreeNode[];
  onExpandChange: any;
  onSelectionChange: any;
  className?: string;
}

interface ITreeState {
  flatNodes: FlatNode[];
}

const ROW_HEIGHT = 25;

const IconButton = glamorous(Icon)({
  cursor: "pointer",
});

const Label = glamorous.span(
  {
    cursor: "pointer",
  },
  ({ isSelected }: { isSelected: boolean | undefined }) => ({
    backgroundColor: isSelected ? "blue" : "transparent",
  }),
);

const NodeElement = glamorous.div(
  {
    cursor: "pointer",
    textAlign: "left",
  },
  ({ flatNode }: { flatNode: FlatNode }) => ({
    paddingLeft: `${flatNode.depth * 25 + 5}px`,
    backgroundColor: flatNode.node.isSelected ? "#137cbd" : "transparent",
    color: flatNode.node.isSelected ? "white" : "black",
    ":hover": {
      backgroundColor: flatNode.node.isSelected ? "#137cbd" : "#ebf1f5",
    },
  }),
);

const cache = new CellMeasurerCache({
  defaultWidth: 100,
  minWidth: 75,
  fixedHeight: true,
});

export class VirtualizedTree extends React.Component<ITreeProps, ITreeState> {
  tree: Grid | null;

  constructor(props: ITreeProps) {
    super();

    this.state = {
      flatNodes: flattenNodes(props.nodes),
    };
  }

  componentWillReceiveProps(nextProps: ITreeProps) {
    this.setState({
      flatNodes: flattenNodes(nextProps.nodes),
    });

    if (this.tree) {
      this.tree.forceUpdate();
    }
  }

  toggleExpand = (node: ITreeNode) => {
    node.isExpanded = !node.isExpanded;

    this.props.onExpandChange(node);
  };

  toggleSelection = (node: ITreeNode) => {
    const isSelected = !node.isSelected;
    if (isSelected) {
      this.state.flatNodes.forEach(flatNode => (flatNode.node.isSelected = false));
    }

    node.isSelected = isSelected;

    this.props.onSelectionChange(node);
  };

  getExpandedIcon = (node: ITreeNode) => (
    <IconButton
      iconName={node.isExpanded ? IconClasses.CHEVRON_RIGHT : IconClasses.CHEVRON_DOWN}
      onClick={ev => {
        this.toggleExpand(node);
        ev.stopPropagation();
      }}
    />
  );

  getLabel = (node: ITreeNode) => (
    <Label isSelected={node.isSelected} onClick={() => this.toggleSelection(node)}>
      {" "}
      {node.label}{" "}
    </Label>
  );

  maybeRenderSecondaryLabel(secondaryLabel) {
    if (secondaryLabel) {
      return <span className={Classes.TREE_NODE_SECONDARY_LABEL}>{secondaryLabel}</span>;
    } else {
      return undefined;
    }
  }

  renderTreeNode = ({ rowIndex, style, isScrolling, key, columnIndex, parent }: GridCellProps) => {
    const flatNode = this.state.flatNodes[rowIndex];
    style.width = "100%";

    const ExpandedIcon = flatNode.hasChildNodes ? this.getExpandedIcon(flatNode.node) : undefined;

    return (
      <CellMeasurer cache={cache} columnIndex={columnIndex} key={key} parent={parent as any} rowIndex={rowIndex}>
        <NodeElement
          key={key}
          style={{ ...style, lineHeight: style.height + "px", whiteSpace: "nowrap" }}
          flatNode={flatNode}
          onClick={() => this.toggleSelection(flatNode.node)}
        >
          {ExpandedIcon} {this.maybeRenderIcon(flatNode.node.iconName, flatNode.node.isSelected)} {flatNode.node.label}
          {this.maybeRenderSecondaryLabel(flatNode.node.secondaryLabel)}
        </NodeElement>
      </CellMeasurer>
    );
  };

  maybeRenderIcon = (iconName, isSelected?) => {
    if (iconName) {
      const iconClasses = classNames(Classes.TREE_NODE_ICON, "pt-icon-standard", Classes.iconClass(iconName));
      const style = isSelected ? { color: "white" } : {};
      return <span className={iconClasses} style={style} />;
    } else {
      return undefined;
    }
  };

  renderBluePrintNode = ({ rowIndex, style, isScrolling, key }: GridCellProps) => {
    const flatNode = this.state.flatNodes[rowIndex];

    const isExpanded = flatNode.hasChildNodes ? flatNode.node.isExpanded : false;

    const caretClass = flatNode.hasChildNodes ? Classes.TREE_NODE_CARET : Classes.TREE_NODE_CARET_NONE;
    const caretStateClass = isExpanded ? Classes.TREE_NODE_CARET_OPEN : Classes.TREE_NODE_CARET_CLOSED;
    const caretClasses = classNames(caretClass, "pt-icon-standard", {
      [caretStateClass]: flatNode.hasChildNodes !== undefined,
    });

    const classes = classNames(Classes.TREE_NODE, {
      [Classes.TREE_NODE_SELECTED]: flatNode.node.isSelected,
      [Classes.TREE_NODE_EXPANDED]: isExpanded,
    });

    const contentClasses = classNames(Classes.TREE_NODE_CONTENT, `pt-tree-node-content-${flatNode.depth}`);

    return (
      <div key={key} style={style} onClick={() => this.toggleSelection(flatNode.node)} className={classes}>
        <div className={contentClasses}>
          <span
            className={caretClasses}
            onClick={ev => {
              this.toggleExpand(flatNode.node);
              ev.stopPropagation();
            }}
          />
          {this.maybeRenderIcon(flatNode.node.iconName)}
          <span className={Classes.TREE_NODE_LABEL}>{flatNode.node.label}</span>
        </div>
      </div>
    );
  };

  calculateExpandedNodesCount = () => {
    const { nodes } = this.props;
    const totalSum = nodes.reduce((sum, node) => sum + this.calculateExpandedNodeCount(node), 0);
    return totalSum;
  };

  calculateExpandedNodeCount = (node: ITreeNode) => {
    let totalCount = 1;

    if (node.isExpanded && node.childNodes) {
      totalCount += node.childNodes.map(this.calculateExpandedNodeCount).reduce((total, count) => total + count, 0);
    }

    return totalCount;
  };

  calculateRowHeight = ({ index }) => {
    return this.calculateExpandedNodeCount(this.props.nodes[index]) * ROW_HEIGHT;
  };

  setRef = (grid: Grid) => {
    this.tree = grid;
  };

  render() {
    return (
      <AutoSizer>
        {({ height, width }) => (
          <Grid
            height={height}
            width={width}
            cellRenderer={this.renderTreeNode}
            columnCount={1}
            ref={this.setRef}
            rowCount={this.calculateExpandedNodesCount()}
            rowHeight={ROW_HEIGHT}
            columnWidth={cache.columnWidth as any}
            deferredMeasurementCache={cache}
            className={this.props.className}
            containerStyle={{
              overflowX: "auto",
            }}
          />
        )}
      </AutoSizer>
    );
  }
}
