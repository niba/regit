import { ITreeNode } from "@blueprintjs/core";
import { omit } from "lodash";

type GetNodeKey = ({ node, treeIndex }: { node: ITreeNode; treeIndex: number }) => string;
type NodeCallback = (flatNode: FlatNode) => void;
export type FlatNode = {
  node: ITreeNode;
  parentNode: ITreeNode | undefined;
  hasChildNodes: boolean;
  depth: number;
  index: number;
  key: string | number;
};

export const flattenNodes = (nodes: ITreeNode[]) => {
  const flattenedNodes: FlatNode[] = [];
  let index = 0;

  const flattenNode = (parentNode: ITreeNode | undefined, node: ITreeNode, depth: number) => {
    const flatNode: FlatNode = {
      node: {
        ...(omit(node, "childNodes") as Omit<ITreeNode, "childNodes">),
      },
      hasChildNodes: node.childNodes !== undefined && node.childNodes.length > 0,
      parentNode: parentNode
        ? {
            ...(omit(parentNode, "childNodes") as Omit<ITreeNode, "childNodes">),
          }
        : parentNode,
      depth,
      index,
      key: node.id,
    };
    index += 1;

    flattenedNodes.push(flatNode);
    if (node.isExpanded && node.childNodes) {
      node.childNodes.forEach(childNode => flattenNode(node, childNode, depth + 1));
    }
  };

  nodes.forEach(node => flattenNode(undefined, node, 0));

  return flattenedNodes;
};
