import React from "react";
import { storiesOf } from "@storybook/react";
import { VirtualizedTree } from "./VirtualizedTree";
import { ITreeNode } from "@blueprintjs/core";
import "@blueprintjs/core/dist/blueprint.css";

const nodes: ITreeNode[] = [
  {
    id: "a",
    label: "long text example",
    isSelected: false,
    isExpanded: false,
    childNodes: [
      {
        id: "a - 1",
        label: "longer longer longer text example",
        isSelected: false,
        iconName: "folder-open",
        isExpanded: false,
      },
      {
        id: "a - 2",
        label: "a - 2",
        isSelected: false,
        isExpanded: false,
      },
    ],
  },
  {
    id: "b",
    label: "b",
    isSelected: false,
    isExpanded: true,
    childNodes: [
      {
        id: "b - 1",
        label: "b - 1",
        isSelected: false,
        isExpanded: false,
      },
      {
        id: "b - 2",
        label: "b- 2",
        isSelected: false,
        isExpanded: false,
      },
    ],
  },
];

class VirtualizedTreeExample extends React.Component<any, any> {
  constructor() {
    super();

    this.state = {
      nodes,
    };
  }

  onChange = node => {
    this.setState(this.state);
  };

  render() {
    return <VirtualizedTree nodes={nodes} onExpandChange={this.onChange} onSelectionChange={this.onChange} />;
  }
}

storiesOf("VirtualizedTree", module)
  .addDecorator(story => <div style={{ textAlign: "center" }}>{story()}</div>)
  .add("standard", () => (
    <div style={{ width: "500px", height: "500px" }}>
      {" "}
      <VirtualizedTreeExample />
    </div>
  ));
