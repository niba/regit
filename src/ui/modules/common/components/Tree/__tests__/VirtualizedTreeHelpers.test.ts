import { flattenNodes } from "../VirtualizedTreeHelpers";
import { ITreeNode } from "@blueprintjs/core";

const nodes: ITreeNode[] = [
  {
    id: "a",
    label: "a",
    isSelected: false,
    isExpanded: false,
    childNodes: [
      {
        id: "a - 1",
        label: "a - 1",
        isSelected: false,
        isExpanded: false,
      },
      {
        id: "a - 2",
        label: "a - 2",
        isSelected: false,
        isExpanded: false,
      },
    ],
  },
  {
    id: "b",
    label: "b",
    isSelected: false,
    isExpanded: true,
    childNodes: [
      {
        id: "b - 1",
        label: "b - 1",
        isSelected: false,
        isExpanded: false,
      },
      {
        id: "b - 2",
        label: "b- 2",
        isSelected: false,
        isExpanded: false,
      },
    ],
  },
];

describe("Tree helpers", () => {
  it("walk tree properly", () => {
    const result = flattenNodes(nodes);
    expect([
      {
        node: {
          id: "a",
          label: "a",
          isSelected: false,
          isExpanded: false,
        },
        hasChildNodes: true,
        parentNode: undefined,
        depth: 0,
        index: 0,
        key: "a",
      },
      {
        node: {
          id: "b",
          label: "b",
          isSelected: false,
          isExpanded: true,
        },
        hasChildNodes: true,
        parentNode: undefined,
        depth: 0,
        index: 1,
        key: "b",
      },
      {
        node: {
          id: "b - 1",
          label: "b - 1",
          isSelected: false,
          isExpanded: false,
        },
        parentNode: {
          id: "b",
          label: "b",
          isSelected: false,
          isExpanded: true,
        },
        hasChildNodes: false,
        depth: 1,
        index: 2,
        key: "b - 1",
      },
      {
        node: {
          id: "b - 2",
          label: "b- 2",
          isSelected: false,
          isExpanded: false,
        },
        parentNode: {
          id: "b",
          label: "b",
          isSelected: false,
          isExpanded: true,
        },
        hasChildNodes: false,
        depth: 1,
        index: 3,
        key: "b - 2",
      },
    ]).toEqual(result);
  });
});
