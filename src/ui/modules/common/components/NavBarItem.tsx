import { Classes, Popover, Position, Icon, IconClasses } from "@blueprintjs/core";
import glamorous from "glamorous";
import * as React from "react";

const NavItemWidth = "200px";

const PopoverContent = glamorous.div({
  overflow: "auto",
  maxHeight: "calc(100vh - 200px)",
  maxWidth: "300px",
});

const NavBarItemLayout = glamorous.div({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: NavItemWidth,
});

const NavBarArrow = glamorous.div({
  textAlign: "right",
  flex: "1",
});

const NavBarItemTitleColumn = glamorous.div({
  display: "flex",
  flexDirection: "column",
  marginLeft: "10px",
  lineHeight: "16px",
  overflow: "hidden",
});
const NavBarItemTitleText = glamorous.span({
  fontSize: "12px",
  fontWeight: "lighter",
});
const NavBarItemSubtitleText = glamorous.span({
  fontSize: "14px",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
  maxWidth: "100%",
});

const NavBarItemPopover = glamorous(Popover)(
  {
    boxSizing: "border-box",
    padding: "10px",
    cursor: "pointer",
    "&:hover": {
      background: "rgba(138, 155, 168, 0.15)",
    },
    ":after": {
      margin: "0px",
      width: "1px",
      display: "inline-block",
      position: "absolute",
      top: "0%",
      bottom: "0%",
      left: "100%",
      content: '""',
      background: "rgba(255, 255, 255, 0.10)",
    },
  },
  ({ isActive }: { isActive: boolean }) => ({
    background: isActive ? "rgba(138, 155, 168, 0.15)" : "transparent",
  }),
);

type NavItemProps = {
  title: string;
  subtitle: string | JSX.Element;
  icon: any;
  children;
  isActive: boolean;
  onSelectionChanged: (isActive: boolean) => void;
};

export const NavItem = (props: NavItemProps) => {
  const { title, subtitle, icon, children, onSelectionChanged, isActive } = props;

  return (
    <NavBarItemPopover
      popoverClassName={Classes.POPOVER}
      autoFocus
      canEscapeKeyClose={true}
      isOpen={isActive}
      position={Position.BOTTOM}
      content={<PopoverContent>{children}</PopoverContent>}
      onClose={() => onSelectionChanged(false)}
      isActive={isActive}
    >
      <NavBarItemLayout onClick={() => onSelectionChanged(true)}>
        {icon}
        <NavBarItemTitleColumn>
          <NavBarItemTitleText>{title}</NavBarItemTitleText>
          <NavBarItemSubtitleText>{subtitle}</NavBarItemSubtitleText>
        </NavBarItemTitleColumn>
        <NavBarArrow>
          <Icon iconName={IconClasses.CHEVRON_DOWN} />
        </NavBarArrow>
      </NavBarItemLayout>
    </NavBarItemPopover>
  );
};
