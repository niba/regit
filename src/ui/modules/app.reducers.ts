import { Reducer } from "redux";
import { diffReducer, DiffState } from "./diff/diff.reducers";
import { graphReducer, GraphState } from "./graph/graph.reducers";
import { repositoryReducer, RepositoryState } from "./repository/repository.reducers";
import { actionBarReducer, ActionBarState } from "./actionBar/actionBar.reducers";
import { branchReducer, BranchState } from "./branch/branch.reducers";
import { dataReducer, RepoCacheState } from "./data/cache.reducers";

export interface ApplicationState {
  diff: DiffState;
  graph: GraphState;
  repository: RepositoryState;
  actionBar: ActionBarState;
  branch: BranchState;
  cache: RepoCacheState;
}

type ApplicationReducer = { [P in keyof ApplicationState]: Reducer<ApplicationState[P]> };

export const reducers: ApplicationReducer = {
  diff: diffReducer,
  graph: graphReducer,
  repository: repositoryReducer,
  actionBar: actionBarReducer,
  branch: branchReducer,
  cache: dataReducer,
};
