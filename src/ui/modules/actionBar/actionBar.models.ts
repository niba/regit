export interface GitAction {
  name: string;
  invoke(): Promise<void>;
}

export interface GitUndoableAction extends GitAction {
  message?: string;
  undo(): Promise<void>;
}

export type ActionType = "checkout" | "reset" | "commit" | "undo";

export interface RefLog {
  committer: NodeGit.Signature;
  idOld: NodeGit.Oid;
  idNew: NodeGit.Oid;
  message: string;
  displayedMessage: string;
  command: ActionType;
}

export interface GitStatus {
  ahead: number;
  behind: number;
  local: string;
  upstream: string;
}
