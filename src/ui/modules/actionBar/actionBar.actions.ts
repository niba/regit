import actionCreatorFactory from "typescript-fsa";
import { RefLog, GitStatus } from "./actionBar.models";
import {
  getStartedActions,
  enhanceActionCreatorFactory,
  AsyncActionCreatorWithHandler,
  getActionsArray,
} from "ui/modules/common/actionsEnhancements";

const actionCreator = enhanceActionCreatorFactory(actionCreatorFactory("ACTIONBAR_MODULE"));

const actionCreators = {}; // tu nie ASYNC

const asyncActionCreators = {
  getUndoReflog: actionCreator.async<any, RefLog | undefined>("GET_UNDO_REFLOG"),
  pullStatus: actionCreator.async<any, GitStatus, any>("PULL_STATUS"),
};

const asyncActionCreatorsWithHandlers = {
  pullCurrentBranch: actionCreator.asyncWithHandlers<any, any>("PULL_CURRENT_BRANCH", {
    successMessage: () => "Changes have been pulled sucessfully.",
  }),
  pushBranch: actionCreator.asyncWithHandlers<{ name?: string }, void>("PUSH_BRANCH", {
    successMessage: () => "Changes have been pushed successfully.",
  }),
  stashPop: actionCreator.asyncWithHandlers<any, void>("STASH_POP", {
    successMessage: () => "Changes have been popped.",
  }),
  stashPush: actionCreator.asyncWithHandlers<any, void>("STASH_PUSH", {
    successMessage: () => "Changes have been stashed.",
  }),
  undo: actionCreator.asyncWithHandlers<{ reflog: RefLog }, void>("UNDO", {
    successMessage: () => "Undo successful.",
  }),
};

export const actions = {
  ...actionCreators,
  ...asyncActionCreators,
  ...asyncActionCreatorsWithHandlers,
};

export const callableActions = {
  ...actionCreators,
  ...getStartedActions(asyncActionCreators),
  ...getStartedActions(asyncActionCreatorsWithHandlers),
};

export const notifiableActions: AsyncActionCreatorWithHandler<any, any, any>[] = getActionsArray(
  asyncActionCreatorsWithHandlers,
);
