import { ActionType, RefLog } from "./actionBar.models";
import { checkoutBranch, resetTo, reflogReplaceLast, reflog } from "../../gitCommands";
import nodegit from "nodegit";
import * as _ from "lodash";

type HandlerType = (refLog: RefLog) => ((repo: nodegit.Repository) => Promise<any>) | undefined;

const parseReflogMessage = (msg: string) => {
  const separatorSplit = msg.indexOf(":");

  const command = msg.substring(0, separatorSplit);
  const message = msg.substring(separatorSplit + 1).trim();

  return { command: command as ActionType, message };
};

const checkoutActionRegexp = new RegExp("moving from (.*) to (.*)");

const parseCheckoutAction = (refLog: RefLog) => {
  const res = refLog.message.match(checkoutActionRegexp);
  if (res) {
    const srcBranch = res[1];
    return repo => checkoutBranch(repo, srcBranch);
  }

  return undefined;
};

const parseResetAction = (refLog: RefLog) => (repo: nodegit.Repository) => resetTo(repo, refLog.idOld);

const commandMappings: { [P in ActionType]: HandlerType } = {
  checkout: parseCheckoutAction,
  reset: parseResetAction,
  commit: parseResetAction,
  undo: () => undefined,
  // todo commit (merge), commit (initial) mot supported
};

async function reflogEntries(repository: nodegit.Repository): Promise<RefLog[]> {
  const log = await reflog(repository, "HEAD");

  const count = log.entrycount();
  const entries = _.range(0, count).map((n, i) => {
    const entry = log.entryByIndex(i);
    const { command, message } = parseReflogMessage(entry.message());
    if (!command || !message) return undefined;

    const refEntry: RefLog = {
      committer: entry.committer(),
      idOld: entry.idOld(),
      idNew: entry.idNew(),
      displayedMessage: message,
      message: entry.message(),
      command,
    };
    return refEntry;
  });

  return entries.filter(x => x) as RefLog[];
}

export async function runUndoAction(repository: nodegit.Repository, refLog: RefLog) {
  const mapping = commandMappings[refLog.command] as HandlerType;
  if (mapping) {
    const undoAction = mapping(refLog);
    if (undoAction) {
      await undoAction(repository);
      await reflogReplaceLast(repository, "HEAD", `undo: ${refLog.message}`);
    }
  }
}

export async function getUndoReflog(repository: nodegit.Repository): Promise<RefLog | undefined> {
  const entries = await reflogEntries(repository);

  const ignored = Array<number>();

  for (let i = 0; i < entries.length; i++) {
    const entry = entries[i];

    if (ignored.indexOf(i) !== -1 || Object.keys(commandMappings).indexOf(entry.command) === -1) continue;

    if (entry.command !== "undo") {
      return entry;
    }

    const undoedEntry = entries.find(
      (x, ind) =>
        ind > i && ignored.indexOf(ind) === -1 && x.command !== "undo" && x.message === entry.displayedMessage,
    );

    if (undoedEntry) {
      // todo super unefficient
      const undoedEntryIndex = entries.indexOf(undoedEntry);
      ignored.push(undoedEntryIndex);
    }
  }

  return undefined;
}
