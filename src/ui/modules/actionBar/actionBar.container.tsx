import * as React from "react";
import { Button, Tooltip, Position, IconClasses, Classes } from "@blueprintjs/core";
import { RefLog } from "./actionBar.models";
import { connect } from "react-redux";
import { ApplicationState } from "ui/modules/app.reducers";
import { callableActions } from "./actionBar.actions";
import { ActionCreator } from "typescript-fsa";
import glamorous from "glamorous";
import classNames from "classnames";

const ActionButton = glamorous(Button)({
  height: "50px",
});

class ActionBar extends React.Component<ConnectProps, any> {
  undo = async (reflog?: RefLog) => {
    if (!reflog) return;

    this.props.undo({ reflog });
  };

  getTooltipMessage() {
    const reflog = this.props.undoReflog;

    if (!reflog) return "Undo not possible";

    return `Undo action: ${reflog.message}`;
  }

  isDisabled = () => !!this.props.loadingAction;
  isLoading = (loadingAction: ActionCreator<any>) => {
    return this.props.loadingAction && this.props.loadingAction.type === loadingAction.type;
  };

  renderUndoButton = () => {
    const message = this.getTooltipMessage();
    return (
      <Tooltip content={<span>{message}</span>} inline={true} position={Position.BOTTOM}>
        <ActionButton
          text="Undo"
          iconName={IconClasses.UNDO}
          disabled={this.isDisabled()}
          loading={this.isLoading(callableActions.undo)}
          onClick={() => this.undo(this.props.undoReflog)}
        />
      </Tooltip>
    );
  };

  getPullStatusMessage = () => {
    const { status } = this.props;

    if (!status) return "No pull information";

    if (status.ahead === 0 && status.behind === 0) return "Already up-to-date";

    if (status.ahead === 0) {
      return `Local ${status.local} branch is ${status.behind} commit(s) behind ${status.upstream}`;
    }

    if (status.behind === 0) {
      return `Local ${status.local} branch is ${status.ahead} commit(s) ahead of ${status.upstream}`;
    }

    return `Local ${status.local} branch is ${status.ahead} commit(s) ahead and
    ${status.behind} commit(s) behind ${status.upstream}`;
  };

  render() {
    return (
      <div className={classNames(Classes.BUTTON_GROUP, Classes.MINIMAL)}>
        <Tooltip content={<span>{this.getPullStatusMessage()}</span>} inline={true} position={Position.BOTTOM}>
          <ActionButton
            text="Pull"
            iconName={IconClasses.GIT_PULL}
            disabled={this.isDisabled()}
            loading={this.isLoading(callableActions.pullCurrentBranch)}
            onClick={() => this.props.pullCurrentBranch({})}
          />
        </Tooltip>
        <ActionButton
          text="Push"
          iconName={IconClasses.GIT_PUSH}
          disabled={this.isDisabled()}
          loading={this.isLoading(callableActions.pushBranch)}
          onClick={() => this.props.pushBranch({})}
        />
        <ActionButton
          text="Stash"
          iconName={IconClasses.ARROW_DOWN}
          disabled={this.isDisabled()}
          loading={this.isLoading(callableActions.stashPush)}
          onClick={() => this.props.stashPush({})}
        />
        <ActionButton
          text="Pop"
          iconName={IconClasses.ARROW_UP}
          disabled={this.isDisabled()}
          loading={this.isLoading(callableActions.stashPop)}
          onClick={() => this.props.stashPop({})}
        />
        {this.renderUndoButton()}
      </div>
    );
  }
}

const connectCreator = connect((state: ApplicationState) => state.actionBar, callableActions);
type ConnectProps = typeof connectCreator.allProps;
export const ActionBarContainer = connectCreator(ActionBar);
