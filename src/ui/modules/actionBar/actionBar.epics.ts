import { getUndoReflog, runUndoAction } from "./actionBar.services";
import { GitStatus } from "./actionBar.models";
import { makeAsyncEpic } from "../common/makeAsyncEpic";
import { actions } from "./actionBar.actions";
import { ApplicationState } from "ui/modules/app.reducers";
import { pullCurrentBranch, pushBranch, stashPush, stashPop } from "./actionBar.gitActions";
import { ActionsObservable } from "redux-observable";
import { Action, Store } from "redux";
import { getStatus } from "ui/gitCommands";
import { authEpic } from "ui/modules/common/common.observables";
import * as commonSelectors from "ui/modules/common/common.selectors";

const repositoryExistsPredicate = (action, state: ApplicationState) =>
  commonSelectors.loadedRepositorySelector(state) !== undefined;

export const getUndoReflogEpic = makeAsyncEpic(
  actions.getUndoReflog,
  async (_, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    return getUndoReflog(repository);
  },
  repositoryExistsPredicate,
);

export const getPullStatus = makeAsyncEpic(
  actions.pullStatus,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    const status = await getStatus(repository);
    return status as GitStatus;
  },
  repositoryExistsPredicate,
);

export const undoActionEpic = makeAsyncEpic(
  actions.undo,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    return runUndoAction(repository, payload.reflog);
  },
  repositoryExistsPredicate,
);

export const pullCurrentBranchEpic = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) => {
  return authEpic(actions.pullCurrentBranch, async (payload, password) => {
    const repository = await commonSelectors.getCurrentRepository(store.getState());
    await pullCurrentBranch(repository, password);
  })(action$, store);
};

export const pushBranchEpic = (action$: ActionsObservable<Action>, store: Store<ApplicationState>) => {
  return authEpic(actions.pushBranch, async (payload, password) => {
    const repository = await commonSelectors.getCurrentRepository(store.getState());
    let currentBranchName = payload.name;

    if (!currentBranchName) {
      const currentBranch = await repository.getCurrentBranch();
      currentBranchName = currentBranch.shorthand();
    }

    await pushBranch(repository, currentBranchName, password);
  })(action$, store);
};

export const stashPushEpic = makeAsyncEpic(
  actions.stashPush,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    await stashPush(repository);
  },
  repositoryExistsPredicate,
);

export const stashPopEpic = makeAsyncEpic(
  actions.stashPop,
  async (payload, state) => {
    const repository = await commonSelectors.getCurrentRepository(state);
    await stashPop(repository);
  },
  repositoryExistsPredicate,
);

export const refreshUndoReflogEpic = (
  action$: ActionsObservable<Action>, // todo react on git changes
) => action$.filter(actions.undo.done.match).map(_ => actions.getUndoReflog.started({}));
