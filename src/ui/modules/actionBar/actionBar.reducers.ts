import { reducerWithInitialState } from "typescript-fsa-reducers";
import { actions, notifiableActions } from "./actionBar.actions";
import { RefLog, GitStatus } from "./actionBar.models";
import { Action } from "typescript-fsa";

export interface ActionBarState {
  undoReflog?: RefLog;
  loadingAction?: Action<any>;
  status?: GitStatus;
}

const defaultState: ActionBarState = { undoReflog: undefined, loadingAction: undefined, status: undefined };

export const actionBarReducer = reducerWithInitialState(defaultState)
  .case(actions.getUndoReflog.done, (state, payload) => ({
    ...state,
    undoReflog: payload.result,
  }))
  .case(actions.pullStatus.done, (state, payload) => ({
    ...state,
    status: payload.result,
  }))
  .casesWithAction(notifiableActions.map(notifiableAction => notifiableAction.failed), (state, action) => ({
    ...state,
    loadingAction: action,
  }))
  .cases(
    notifiableActions
      .map(notifiableAction => notifiableAction.failed as any)
      .concat(notifiableActions.map(notifiableAction => notifiableAction.done as any)),
    (state, payload) => ({
      ...state,
      loadingAction: undefined,
    }),
  );
