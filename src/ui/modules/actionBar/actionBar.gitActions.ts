import { resetTo, push, pull, apply, pop, stash } from "../../gitCommands/index";
import nodegit from "nodegit";

export async function resetWorkingChanges(repository: nodegit.Repository) {
  const lastCommit = await repository.getHeadCommit();
  await resetTo(repository, lastCommit);
}

export async function undoLastCommit(repository: nodegit.Repository) {
  const lastCommit = await repository.getHeadCommit();
  const previous = await lastCommit.getParents();
  await resetTo(repository, previous[0]);
}

export async function pullCurrentBranch(repository: nodegit.Repository, password?: string) {
  const currentBranch = await repository.getCurrentBranch();
  const currentBranchName = currentBranch.shorthand();
  await pull(repository, `origin/${currentBranchName}`, currentBranchName, password);
}

export async function pushBranch(repository: nodegit.Repository, branch: string, password?: string) {
  await push(repository, branch, branch, password);
}

export async function stashPush(repository: nodegit.Repository) {
  await stash(repository);
}
export async function stashApply(repository: nodegit.Repository) {
  await apply(repository, 0);
}
export async function stashPop(repository: nodegit.Repository) {
  await pop(repository, 0);
}
