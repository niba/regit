import React from "react";
import { Panel } from "ui/modules/common/components/Panel";
import glamorous from "glamorous";

const CommitPanel = glamorous(Panel)({
  flex: 0.05,
});

export const CommitView: React.StatelessComponent<any> = () => <CommitPanel>SELECTED COMMIT DIFF</CommitPanel>;
