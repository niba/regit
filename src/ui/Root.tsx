import { Provider } from "react-redux";
import * as React from "react";
import { configureStore } from "./store";
import { GraphContainer } from "./modules/graph/Graph.container";
import { DocumentViewContainer } from "./modules/diff/DocumentView.container";
import { App } from "./App";
import { Switch, Route } from "react-router";
import { ConnectedRouter } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import { bridgeActions } from "./bridge.actions";

const history = createHistory();
const store = configureStore(history)();
bridgeActions(store);

export default class Root extends React.Component<any, {}> {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App>
            <Switch>
              <Route exact path="/" children={props => <GraphContainer location={props.location.pathname} />} />
              <Route path="/diff" children={props => <DocumentViewContainer location={props.location.pathname} />} />
            </Switch>
          </App>
        </ConnectedRouter>
      </Provider>
    );
  }
}
