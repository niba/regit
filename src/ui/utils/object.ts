export function objectValues<T>(obj: T): any[] {
  return Object.keys(obj).map(key => obj[key]);
}
