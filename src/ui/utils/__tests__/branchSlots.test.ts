import { BranchSlots } from "../branchSlots";

describe("Branch Slots ", () => {
  it("reserve", async () => {
    const branchSlots = new BranchSlots(50);
    branchSlots.reserve(0);
    branchSlots.reserve(1);
    const freeSlot = branchSlots.getFreeIndex();

    expect(freeSlot).toBe(2);
  });

  it("reserve in the middle", async () => {
    const branchSlots = new BranchSlots(50);
    branchSlots.reserve(4);
    branchSlots.reserve(7);
    const freeSlot = branchSlots.getFreeIndex();

    expect(freeSlot).toBe(0);
    branchSlots.reserve(0);
    branchSlots.reserve(1);
    branchSlots.reserve(2);
    branchSlots.reserve(3);

    expect(branchSlots.getFreeIndex()).toBe(5);
  });

  it("unreserve", async () => {
    const branchSlots = new BranchSlots(50);
    branchSlots.reserve(0);
    branchSlots.reserve(1);
    const freeSlot = branchSlots.getFreeIndex();

    expect(freeSlot).toBe(2);
    branchSlots.unreserve(0);

    expect(branchSlots.getFreeIndex()).toBe(0);
  });

  it("get reserved branches", async () => {
    const branchSlots = new BranchSlots(50);
    branchSlots.reserve(0);
    branchSlots.reserve(1);
    branchSlots.reserve(5);
    branchSlots.reserve(6);

    expect(branchSlots.getReservedSlotsIndexes()).toEqual([0, 1, 5, 6]);
  });

  it("get reserved branches - 2", async () => {
    const branchSlots = new BranchSlots(50);
    branchSlots.reserve(0);
    branchSlots.reserve(1);
    branchSlots.reserve(5);
    branchSlots.reserve(6);
    branchSlots.unreserve(4);
    branchSlots.unreserve(6);

    expect(branchSlots.getReservedSlotsIndexes()).toEqual([0, 1, 5]);
  });
});
