import electron from "electron";
import path from "path";
import fs from "fs-extra";

export const appPath = () => {
  return path.join(electron.remote.app.getPath("documents"), "regit");
};
export const repositoriesPath = async () => {
  const app = appPath();
  await fs.ensureDir(app);
  return path.join(appPath(), "repositories.config");
};

export const sshPubKeyPath = () => {
  return path.join(electron.remote.app.getPath("home"), ".ssh", "id_rsa.pub");
};
export const sshPrivKeyPath = () => {
  return path.join(electron.remote.app.getPath("home"), ".ssh", "id_rsa");
};
