export class BranchSlots {
  private slots: number[] = [];
  private reservedSlotsIndexes: number[] = [];
  private activeBranchesSize: number;
  private maximumSlot = 0;

  constructor(size = 1000) {
    this.slots = Array.apply(undefined, Array(size)).map(Number.prototype.valueOf, 0);
    this.activeBranchesSize = 0;
    this.reservedSlotsIndexes = [];
  }

  getFreeIndex = () => this.slots.findIndex(value => value === 0);

  getMaximumSlot = () => this.maximumSlot;

  reserve = (index: number) => {
    if (this.slots[index] === 0) {
      if (index > this.maximumSlot) {
        this.maximumSlot = index;
      }
      this.slots[index] = 1;
      this.activeBranchesSize += 1;
      this.reservedSlotsIndexes.push(index);
    } else {
      this.slots[index] = 1;
    }
  };

  isReserved = index => this.slots[index] === 1;

  getFreeIndexAndReserve = () => {
    const freeSlot = this.getFreeIndex();
    this.reserve(freeSlot);
    return freeSlot;
  };

  unreserve = index => {
    if (this.slots[index] === 0) {
      console.error("UNRESERVER NOT RESERVED");
    }
    if (this.slots[index] > 1) {
      this.slots[index] -= 1;
      return;
    }
    if (this.slots[index] === 1) {
      if (this.activeBranchesSize > 0) {
        this.activeBranchesSize -= 1;
      }
      this.slots[index] = 0;
      const reservedSlotIndex = this.reservedSlotsIndexes.indexOf(index);
      if (reservedSlotIndex > -1) {
        this.reservedSlotsIndexes.splice(reservedSlotIndex, 1);
      }
    }
  };

  getReservedSlotsIndexes = () => this.reservedSlotsIndexes;
}
