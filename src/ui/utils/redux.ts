export function update<T>(elements: T[] | ReadonlyArray<T>, index: number, newValues: Partial<T>) {
  const newValuesAny = newValues as any;
  return (elements as T[]).map((el, i) => (i === index ? { ...(el as any), ...newValuesAny } : el));
}
export function updateByPredicate<T>(
  elements: T[] | ReadonlyArray<T>,
  predicate: (el: T, ind?: number) => boolean,
  newValues: Partial<T>,
) {
  const newValuesAny = newValues as any;
  return (elements as T[]).map((el, i) => (predicate(el, i) ? { ...(el as any), ...newValuesAny } : el));
}
