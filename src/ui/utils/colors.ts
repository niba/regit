const colorsList = [
  "#0aaf8e",
  "#0c00d9",
  "#b41000",
  "#d66900",
  "#c90046",
  "#6900cb",
  "#001b94",
  "#0061ac",
  "#aeb000",
  "#aa0083",
  "#039400",
  "#007edd",
  "#90f200",
  "#ca0eb5",
  "#00bbb0",
  "#b60cd9",
  "#00f4da",
  "#e00010",
  "#00cf53",
  "#de2700",
  "#d9e20a",
  "#43008b",
  "#00df81",
  "#9d00bf",
  "#e0bc00",
  "#00a466",
  "#70b500",
  "#a34600",
  "#4bc400",
  "#00bd23",
];

function getRandomColor() {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export const colors = colorsList;
export const getColor = (index: number) => colorsList[index % colorsList.length ? index % colorsList.length : index];

export const changeColor = (index: number) => (colors[index] = getRandomColor());
