export const measureTime = async (func: any, name = "empty") => {
  const t0 = performance.now();
  const result = await func();
  const t1 = performance.now();
  console.log("Call to " + name + " took " + (t1 - t0) / 1000 + " seconds.");
  return result;
};
