import { ipcRenderer } from "electron";
import { commonActionCreators } from "./modules/common/common.actions";

let storeReference;
export const bridgeActions = store => (storeReference = store);

ipcRenderer.on("refresh", (event, arg) => {
  storeReference.dispatch(commonActionCreators.refresh.started({}));
});
