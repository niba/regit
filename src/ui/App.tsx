import React from "react";
import glamorous from "glamorous";

import { GraphSearchContainer } from "./modules/graph/GraphSearch.container";
import { BranchListingContainer } from "./modules/branch/BranchListing.container";
import { RepoDialogsContainer } from "./modules/repository/RepositoryDialogs.container";
import { DiffPanelContainer } from "./modules/diff/DiffPanel.container";
import { WorkingDirContainer } from "./modules/diff/WorkingDir.container";
import { ActionBarContainer } from "./modules/actionBar/actionBar.container";
import { Redirect } from "react-router-dom";
import { css } from "glamor";
import "./App.css";
import { TopBar } from "ui/TopBar";

const AppLayout = glamorous.div({
  display: "flex",
  height: "100vh",
  width: "100%",
});

const Panels = glamorous.div({
  display: "flex",
  flexDirection: "column",
  width: "100%",
});

const TopPanel = glamorous.div({
  height: "50px",
  width: "100%",
  display: "flex",
  alignItems: "center",
});

const TopPanelSection = glamorous.div(
  {
    flex: 1,
    display: "flex",
    alignItems: "center",
    height: "50px",
  },
  ({ justifyContent }: { justifyContent: any }) => ({
    justifyContent,
  }),
);

const MainPanel = glamorous.div({
  flex: 1,
  display: "flex",
  justifyContent: "space-between",
});

const MainView = glamorous.div(
  {
    display: "flex",
    flex: 0.6,
  },
  ({ location }: { location: string }) =>
    ({
      overflow: location.includes("/diff") ? "auto" : "inherit",
    } as any),
);

const RightPanel = glamorous.div({
  width: "100%",
  display: "flex",
  flex: 0.25,
  overflow: "auto",
});

const navbarClassName = css({ width: "100%" });

const Group = glamorous.div({
  display: "flex",
});

const NavContainer = glamorous.div({
  display: "flex",
  position: "relative",
});

export const App = ({ children }) => {
  return (
    <AppLayout>
      <RepoDialogsContainer />
      {window.location.pathname.includes("index.html") && <Redirect to="/" />}
      <Panels>
        <NavContainer>
          <nav className={`pt-navbar pt-dark ${navbarClassName}`}>
            <TopPanel>
              <TopPanelSection justifyContent="flex-start">
                <TopBar />
              </TopPanelSection>
              <TopPanelSection justifyContent="center">
                <ActionBarContainer />
              </TopPanelSection>
              <TopPanelSection justifyContent="flex-end">
                <GraphSearchContainer />
              </TopPanelSection>
            </TopPanel>
          </nav>
          <WorkingDirContainer />
        </NavContainer>

        <MainPanel>
          <BranchListingContainer />
          <MainView location={window.location.pathname}>{children}</MainView>
          <RightPanel>
            <DiffPanelContainer />
          </RightPanel>
        </MainPanel>
      </Panels>
    </AppLayout>
  );
};
