import * as React from "react";
import glamorous from "glamorous";
import { BranchTopMenuContainer } from "ui/modules/branch/BranchTopMenu.container";
import { RepoTopMenuContainer } from "ui/modules/repository/RepositoryTopMenu.container";

const NavBarMainLayout = glamorous.span({
  display: "flex",
  flexDirection: "row",
  height: "100%",
  alignItems: "center",
});

type State = { current?: string };

export class TopBar extends React.Component<{}, State> {
  constructor(props) {
    super(props);

    this.state = { current: undefined };
  }

  isActive = (id: string) => {
    return this.state.current === id;
  };

  setActive = (id: string) => {
    if (this.isActive(id)) {
      this.setState({ current: undefined });
      return;
    }

    this.setState({ current: id });
  };

  render() {
    return (
      <NavBarMainLayout>
        <div className="pt-button-group pt-large pt-fill" />
        <RepoTopMenuContainer isActive={this.isActive("r")} onSelectionChanged={() => this.setActive("r")} />
        <BranchTopMenuContainer isActive={this.isActive("b")} onSelectionChanged={() => this.setActive("b")} />
      </NavBarMainLayout>
    );
  }
}
