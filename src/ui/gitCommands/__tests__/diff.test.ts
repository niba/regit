import { generatePathForTest } from "tests/repoPaths";
import { prepareSimpleRepo } from "../../../tests/setup";
import { getBranches, getTags } from "../repository";
import { diffStaging } from "ui/gitCommands";

jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

const PATH = generatePathForTest("diff-tests");

describe("Test simple repo", () => {
  let simpleRepository;

  beforeEach(async () => {
    try {
      simpleRepository = await prepareSimpleRepo(PATH);
    } catch (er) {
      console.error(er);
    }
  });

  it("should get all branches", async () => {
    const hmm = await diffStaging(simpleRepository);
    console.log(hmm);
  });
});
