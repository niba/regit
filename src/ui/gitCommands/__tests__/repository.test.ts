import { generatePathForTest } from "tests/repoPaths";
import { prepareSimpleRepo } from "../../../tests/setup";
import { getBranches, getTags } from "../repository";
import { transformReferences } from "ui/modules/branch/branch.models";

jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

const PATH = generatePathForTest("repository-tests");

const compare = (a, b) => (a.friendlyName > b.friendlyName ? 1 : b.friendlyName > a.friendlyName ? -1 : 0);

describe("Test simple repo", () => {
  let simpleRepository;

  beforeEach(async () => {
    try {
      simpleRepository = await prepareSimpleRepo(PATH);
    } catch (er) {
      console.error(er);
    }
  });

  it("should get all branches", async () => {
    const branches = await getBranches(simpleRepository);
    expect(branches.length).toBe(3);
    expect(transformReferences(branches).sort(compare)).toEqual([
      {
        friendlyName: "dev",
        name: "refs/heads/dev",
        isRemote: false,
        isHead: true,
        isTag: false,
        isVisible: true,
        target: "1447207c1eee126bf8d4a57cbbb92a8cddac9a83",
      },
      {
        friendlyName: "feature",
        name: "refs/heads/feature",
        isHead: false,
        isTag: false,
        isRemote: false,
        isVisible: true,
        target: "4cf0a1fe7957fadb7ccf6179f0d65b07b69b4e28",
      },
      {
        friendlyName: "master",
        name: "refs/heads/master",
        isHead: false,
        isTag: false,
        isRemote: false,
        isVisible: true,
        target: "98cc3dd848be9948a8167f8fe19f45760f449c41",
      },
    ]);
  });
});
