import { prepareSimpleRepo } from "../../../tests/setup";
import { MockCommit } from "tests/mockCommit";
import * as mockRepository from "../repository";

enum FAKE_BRANCHES {
  FAKE_BRANCH_1 = "FAKE_BRANCH_1",
  FAKE_BRANCH_2 = "FAKE_BRANCH_2",
  FAKE_BRANCH_3 = "FAKE_BRANCH_3",
}

(mockRepository as any).getBranches = jest.fn(() =>
  Object.keys(FAKE_BRANCHES).map(key => ({ name: () => FAKE_BRANCHES[key] })),
);

const getFakeBranchCommit = commitBranchMap =>
  jest.fn(name => {
    if (commitBranchMap[name]) {
      return commitBranchMap[name];
    }
    return new MockCommit(`empty-sha`, `empty-message`, []);
  });

type FakeCommits = number[][];

const generateMockCommits = (structure: FakeCommits) => {
  const output: MockCommit[] = [];
  structure.reverse().forEach((parents, index) => {
    const trueIndex = structure.length - 1 - index;
    const parentsObjects = parents.map(value => output[value]);
    output[trueIndex] = new MockCommit(`${trueIndex}-sha`, `${trueIndex}-message`, parentsObjects);
  });
  return output;
};

describe("Test graph", () => {
  it("empty, add tests", () => {
    expect(true).toBe(true);
  });
  // it("simple merge", async () => {
  //   // *
  //   // | \
  //   // *  *
  //   // |
  //   // *
  //   const simple = [[1, 2], [3], [], []];
  //   const fakeCommits = generateMockCommits(simple);
  //   const branchCommits = {
  //     [FAKE_BRANCHES.FAKE_BRANCH_1]: fakeCommits[0],
  //   };
  //   const fakeRepository = {
  //     getBranchCommit: getFakeBranchCommit(branchCommits),
  //   };
  //   const result = await getGraph(fakeCommits as any, fakeRepository as any);
  //   expect(result.length).toBe(4);
  //   expect(result[0].isMergeCommit).toBe(true);
  //   expect(result[0].mergeBranchesIndexes).toEqual([1]);
  //   expect(result[1].isMergeCommit).toBe(false);
  //   expect(result[2].branchIndex).toBe(1);
  //   expect(result[3].branchIndex).toBe(0);
  //   console.log("END OF SIMPLE MERGE");
  // });
  // it("simple checkout", async () => {
  //   // *
  //   // |
  //   // *  *
  //   // | |
  //   // *
  //   const simple = [[1], [3], [3], []];
  //   const fakeCommits = generateMockCommits(simple);
  //   const branchCommits = {
  //     [FAKE_BRANCHES.FAKE_BRANCH_1]: fakeCommits[0],
  //     [FAKE_BRANCHES.FAKE_BRANCH_2]: fakeCommits[2],
  //   };
  //   const fakeRepository = {
  //     getBranchCommit: getFakeBranchCommit(branchCommits),
  //   };
  //   const result = await getGraph(fakeCommits as any, fakeRepository as any);
  //   expect(result[0].isMergeCommit).toBe(false);
  //   expect(result[3].isCheckoutCommit).toBe(true);
  //   expect(result[3].isMergeCommit).toBe(false);
  //   expect(result[2].branchIndex).toBe(1);
  //   expect(result[3].branchIndex).toBe(0);
  // });
  // it("multi merge", async () => {
  //   // * 0
  //   // |
  //   // * 2 -> * 1
  //   // |        |
  //   // * 4 -> * 3
  //   //  \     |
  //   //      * 5
  //   const simple = [[2], [2, 3], [4], [4, 5], [5], []];
  //   const fakeCommits = generateMockCommits(simple);
  //   const branchCommits = {
  //     [FAKE_BRANCHES.FAKE_BRANCH_1]: fakeCommits[0],
  //     [FAKE_BRANCHES.FAKE_BRANCH_2]: fakeCommits[1],
  //   };
  //   const fakeRepository = {
  //     getBranchCommit: getFakeBranchCommit(branchCommits),
  //   };
  //   const result = await getGraph(fakeCommits as any, fakeRepository as any);
  //   expect(result[1].isMergeCommit).toBe(true);
  //   expect(result[3].isMergeCommit).toBe(true);
  //   expect(result[1].isBranchTip).toBe(true);
  //   expect(result[2].isCheckoutCommit).toBe(false);
  //   expect(result[2].isMergeCommit).toBe(false);
  //   expect(result[2].checkoutBranchIndexes.length).toBe(0);
  //   expect(result[3].branchIndex).toBe(1);
  //   expect(result[4].isCheckoutCommit).toBe(false);
  //   expect(result[5].isCheckoutCommit).toBe(true);
  // });
  // it("multi merge - 2", async () => {
  //   // * 0
  //   // |
  //   // * 2 -> * 1
  //   // |        |
  //   // * 4 -> * 3
  //   //  \     |
  //   //      * 5
  //   const simple = [[1, 2], [2, 4], [3], [], []];
  //   const fakeCommits = generateMockCommits(simple);
  //   const branchCommits = {
  //     [FAKE_BRANCHES.FAKE_BRANCH_1]: fakeCommits[0],
  //     [FAKE_BRANCHES.FAKE_BRANCH_2]: fakeCommits[1],
  //   };
  //   const fakeRepository = {
  //     getBranchCommit: getFakeBranchCommit(branchCommits),
  //   };
  //   const result = await getGraph(fakeCommits as any, fakeRepository as any);
  // });
  // it("multi merge - 3", async () => {
  //   // * 0
  //   // |    \
  //   // * 2    * 1     * 6
  //   // |        |     |
  //   // * 4    * 3     * 7
  //   //    \---| --- \ |
  //   //      * 5      * 8
  //   //               |  \
  //   //               * 9   * 10
  //   //      11 *------*| *--/
  //   //        |    /    /
  //   //        *  12
  //   // *13
  //   const simple = [[1, 2], [3], [4], [5], [13, 8], [], [7], [8], [9, 10], [12], [12, 11], [12], [], []];
  //   const fakeCommits = generateMockCommits(simple);
  //   // const branchCommits = {
  //   //   fakeBranch: fakeCommits[0],
  //   //   fakeBranch2: fakeCommits[6],
  //   //   fakeBranch3:
  //   // }
  //   const branchCommits = {
  //     [FAKE_BRANCHES.FAKE_BRANCH_1]: fakeCommits[0],
  //     [FAKE_BRANCHES.FAKE_BRANCH_2]: fakeCommits[6],
  //     [FAKE_BRANCHES.FAKE_BRANCH_3]: fakeCommits[10],
  //   };
  //   const fakeRepository = {
  //     getBranchCommit: getFakeBranchCommit(branchCommits),
  //   };
  //   const result = await getGraph(fakeCommits as any, fakeRepository as any);
  //   console.log(result);
  // });
});
