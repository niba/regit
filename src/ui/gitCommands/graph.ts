import { PlainObject, Commit, Reference } from "ui/modules/common/common.models";
import { BranchSlots } from "ui/utils/branchSlots";
import { calculateBranchPosition, calculateLineWidth, calculateDot } from "ui/modules/graph/graphMath";
import { createGraphCommit, GraphCommit } from "ui/modules/graph/graph.models";

export function getGraphCommits(
  commitsMap: PlainObject<Commit>,
  commitsOrder: string[],
  references: Reference[],
): { graphCommits: GraphCommit[]; maximumSlot: number } {
  const branchSlots = new BranchSlots();
  const branchesMap: PlainObject<string[]> = {};
  const tagsMap: PlainObject<string[]> = {};
  const tags = references.filter(reference => reference.isTag && reference.isVisible);
  const branches = references.filter(reference => !reference.isTag && reference.isVisible);
  for (const reference of branches) {
    if (!branchesMap[reference.target]) {
      branchesMap[reference.target] = [reference.friendlyName];
    } else {
      branchesMap[reference.target].push(reference.friendlyName);
    }
    // if (!referencesMap[reference.target] || (referencesMap[reference.target] && !reference.isRemote)) {
    //   referencesMap[reference.target] = reference;
    // }
  }
  for (const reference of tags) {
    if (!tagsMap[reference.target]) {
      tagsMap[reference.target] = [reference.friendlyName];
    } else {
      tagsMap[reference.target].push(reference.friendlyName);
    }
    // if (!referencesMap[reference.target] || (referencesMap[reference.target] && !reference.isRemote)) {
    //   referencesMap[reference.target] = reference;
    // }
  }
  const commitsProcessed: PlainObject<GraphCommit> = {};

  const isTipOfBranch = (commit: Commit) => branchesMap[commit.sha] !== undefined;
  const getBranchName = (commit: Commit) => (branchesMap[commit.sha] ? branchesMap[commit.sha][0] : "");
  const wasCommitProcessed = (commit: Commit) => commitsProcessed[commit.sha] !== undefined;
  const getReferencesNames = (commit: Commit) => {
    const commitBranches = branchesMap[commit.sha] ? branchesMap[commit.sha] : [];
    const commitTags = tagsMap[commit.sha] ? tagsMap[commit.sha] : [];
    return commitBranches.concat(commitTags);
  };

  const makeLine = (actionBranchIndex, branchIndex) => {
    const shouldSwap = actionBranchIndex - branchIndex < 0;
    const startBranch = shouldSwap ? actionBranchIndex : branchIndex;
    const endBranch = shouldSwap ? branchIndex : actionBranchIndex;

    const lineWidth = calculateLineWidth(endBranch, startBranch);
    const left = calculateBranchPosition(startBranch);
    return {
      vertical: {
        index: actionBranchIndex,
        left: calculateBranchPosition(endBranch),
      },
      horizontal: {
        left,
        width: lineWidth,
        index: endBranch,
      },
    };
  };
  let a = 0;
  for (const commitSha of commitsOrder) {
    const commit = commitsMap[commitSha];
    const parents = commit.parents.map(parent => commitsMap[parent]);
    const numberOfParents = parents.length;

    if (isTipOfBranch(commit) && !wasCommitProcessed(commit)) {
      // do we need second comparison?
      const branchIndex = branchSlots.getFreeIndex();
      const branchName = getBranchName(commit);

      branchSlots.reserve(branchIndex);

      commitsProcessed[commit.sha] = createGraphCommit(commit, {
        branchIndex,
        branchName,
        isBranchTip: true,
      });
    }

    if (!wasCommitProcessed(commit)) {
      console.error(`Commit ${commit.sha} was not processed`);
      const branchIndex = branchSlots.getFreeIndex();
      branchSlots.reserve(branchIndex);

      commitsProcessed[commit.sha] = createGraphCommit(commit, {
        branchIndex,
        isBranchTip: true,
      });
    }

    const processedCommit = commitsProcessed[commit.sha];
    processedCommit.referenceNames = getReferencesNames(processedCommit);
    a++;
    if (!processedCommit) {
      console.log(a);
    }
    if (processedCommit.isCheckoutCommit) {
      processedCommit.checkoutBranchIndexes.forEach(checkoutBranch => {
        branchSlots.unreserve(checkoutBranch); // must
        processedCommit.checkoutLines.push(makeLine(checkoutBranch, processedCommit.branchIndex));
      });
    }

    const reservedSlots = branchSlots.getReservedSlotsIndexes().slice();
    reservedSlots.forEach(reservedSlot => {
      processedCommit.lines.push({
        vertical: {
          index: reservedSlot,
          left: calculateBranchPosition(reservedSlot),
        },
      });
    });

    if (numberOfParents > 1) {
      processedCommit.isMergeCommit = true;

      const processedParents: Commit[] = [];
      const notProcessedParents: Commit[] = [];
      const firstParent = parents[0];
      const restParents = parents.slice(1, parents.length);

      parents.forEach(
        parent => (wasCommitProcessed(parent) ? processedParents.push(parent) : notProcessedParents.push(parent)),
      );

      const wasAllProcessed = processedParents.length === parents.length;
      const wasNoneProcessed = notProcessedParents.length === parents.length;
      const wasSomeProcessed = wasAllProcessed === false && wasNoneProcessed === false;

      if (wasAllProcessed) {
        const firstParentProcessed = commitsProcessed[firstParent.sha]; // are we sure that frst?
        firstParentProcessed.checkoutBranchIndexes.push(processedCommit.branchIndex);
        firstParentProcessed.isCheckoutCommit = true;

        restParents.forEach(parent =>
          processedCommit.mergeBranchesIndexes.push(commitsProcessed[parent.sha].branchIndex),
        );
      }

      if (wasSomeProcessed) {
        processedParents.forEach(parent => {
          processedCommit.mergeBranchesIndexes.push(commitsProcessed[parent.sha].branchIndex);
        });

        notProcessedParents.forEach(
          parent =>
            (commitsProcessed[parent.sha] = createGraphCommit(parent, {
              branchIndex: processedCommit.branchIndex,
            })),
        );
      }

      if (wasNoneProcessed) {
        commitsProcessed[firstParent.sha] = createGraphCommit(firstParent, {
          branchIndex: processedCommit.branchIndex,
        });

        restParents.forEach(parent => {
          const branchIndex = branchSlots.getFreeIndex();
          branchSlots.reserve(branchIndex);

          commitsProcessed[parent.sha] = createGraphCommit(parent, {
            branchIndex,
          });

          processedCommit.mergeBranchesIndexes.push(branchIndex);
        });
      }
    }

    // is single parent processed? Detecting checkout
    if (numberOfParents === 1) {
      const parent = parents[0];
      if (wasCommitProcessed(parent)) {
        const processedParent = commitsProcessed[parent.sha];
        processedParent.checkoutBranchIndexes.push(processedCommit.branchIndex);
        processedParent.isCheckoutCommit = true;
      } else {
        commitsProcessed[parent.sha] = createGraphCommit(parent, {
          branchIndex: processedCommit.branchIndex,
        });
      }
    }

    // orphan branch
    if (numberOfParents === 0) {
      branchSlots.unreserve(processedCommit.branchIndex);
    }

    if (processedCommit.isMergeCommit) {
      processedCommit.mergeBranchesIndexes.forEach(mergeBranchIndex => {
        processedCommit.mergeLines.push(makeLine(mergeBranchIndex, processedCommit.branchIndex));
      });
    }

    processedCommit.dot = {
      left: calculateDot(processedCommit.branchIndex),
      index: processedCommit.branchIndex,
    };
  }

  return {
    graphCommits: commitsOrder.map(commitOrder => commitsProcessed[commitOrder]),
    maximumSlot: branchSlots.getMaximumSlot(),
  };
}
