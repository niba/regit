import nodegit from "nodegit";

export async function reflog(repository: nodegit.Repository, name: string) {
  return nodegit.Reflog.read(repository, name);
}

export async function reflogAppend(repository: nodegit.Repository, name: string, message: string) {
  const ref = await reflog(repository, name);
  const currentBranch = await repository.getCurrentBranch();
  ref.append(currentBranch.target(), repository.defaultSignature(), message);
  await ref.write();
}

export async function reflogDrop(repository: nodegit.Repository, name: string) {
  const ref = await reflog(repository, name);
  ref.drop(0, 1);
  await ref.write();
}

export async function reflogReplaceLast(repository: nodegit.Repository, name: string, message: string) {
  const ref = await reflog(repository, name);
  const d = ref.drop(0, 1);
  const currentBranch = await repository.getCurrentBranch();
  ref.append(currentBranch.target(), repository.defaultSignature(), message);
  await ref.write();
}
