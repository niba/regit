import nodegit from "nodegit";
import { authenticator } from "./auth";

export async function createRepository(repoPath): Promise<nodegit.Repository> {
  return await nodegit.Repository.init(repoPath, 0);
}

export async function openRepository(repoPath: string): Promise<nodegit.Repository> {
  return nodegit.Repository.open(repoPath);
}

export async function cloneRepository(
  remoteUrl: string,
  localPath: string,
  password?: string,
  updateAction?: (progress: number) => void,
) {
  return nodegit.Clone.clone(remoteUrl, localPath, {
    fetchOpts: {
      callbacks: {
        certificateCheck: () => 1,
        credentials: (url, username) => authenticator(url, username, password),
        transferProgress: {
          throttle: 1000,
          callback: stats => {
            const progress =
              (stats.indexedDeltas() + stats.indexedObjects()) / (stats.totalObjects() + stats.totalDeltas());

            if (updateAction) {
              updateAction(progress);
            }
            return progress;
          },
        },
      },
    },
  });
}

export async function addFileToIndex(repository: nodegit.Repository, fileName: string): Promise<nodegit.Oid> {
  const repoIndex = await repository.refreshIndex();
  await repoIndex.addByPath(fileName);
  await repoIndex.write();
  return await repoIndex.writeTree();
}

interface ReferenceOptions {
  withRemote: boolean;
  omitReferences: string[];
}
const defaultOptions: ReferenceOptions = {
  withRemote: false,
  omitReferences: [],
};

export async function getReferences(
  repository: nodegit.Repository,
  { withRemote: withRemote = false, omitReferences: omitReferences = [] }: Partial<ReferenceOptions> = {},
): Promise<nodegit.Reference[]> {
  const allReferences = await repository.getReferences(nodegit.Reference.TYPE.LISTALL);
  const tmpMap = {};
  const references = allReferences.filter(
    reference => reference.isBranch() || reference.isRemote() || reference.isTag(),
  );
  const referencesWithoutDuplicates = references.filter(branch => {
    if (!tmpMap[branch.name()]) {
      tmpMap[branch.name()] = true;
      return true;
    }
    return false;
  });

  return referencesWithoutDuplicates;
}

export async function getBranches(
  repository: nodegit.Repository,
  { withRemote: withRemote = false, omitReferences: omitReferences = [] }: Partial<ReferenceOptions> = {},
): Promise<nodegit.Reference[]> {
  const references = await repository.getReferences(nodegit.Reference.TYPE.LISTALL);
  const tmpMap = {};
  const branches = references.filter(reference => reference.isBranch() || reference.isRemote());
  const branchesWithoutDuplicates = branches.filter(branch => {
    if (!tmpMap[branch.name()]) {
      tmpMap[branch.name()] = true;
      return true;
    }
    return false;
  });

  return branchesWithoutDuplicates;
}

export async function getTags(repository: nodegit.Repository, options = defaultOptions): Promise<nodegit.Reference[]> {
  const references = await repository.getReferences(nodegit.Reference.TYPE.LISTALL);
  return references.filter(reference => reference.isTag());
}
