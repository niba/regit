import nodegit from "nodegit";
import { getRemote, authenticator } from "ui/gitCommands/auth";

export async function pull(
  repository: nodegit.Repository,
  from: nodegit.Reference | string,
  to: nodegit.Reference | string,
  password?: string,
  mergePreference: nodegit.Merge.PREFERENCE = nodegit.Merge.PREFERENCE.NONE,
) {
  await repository.fetchAll({
    callbacks: {
      certificateCheck: () => 1,
      credentials: (url, username) => authenticator(url, username, password),
    },
  });

  try {
    await repository.mergeBranches(to, from, undefined, mergePreference);
  } catch (error) {
    if (error instanceof nodegit.Index) throw new Error("Branches couldn't be merged due to conflicts");
    throw error;
  }
}

export async function push(repository: nodegit.Repository, from: string, to: string, password?: string) {
  const refSpecs = `refs/heads/${from}:refs/heads/${to}`;
  const remote = await getRemote(repository);
  // if (!remoteConnected) {
  // remote = await nodegit.Remote.create(repository, "origin", "git@github.com:nodegit/push-example.git");
  //  }

  await remote.push([refSpecs], {
    callbacks: {
      certificateCheck: () => 1,
      credentials: (url, username) => authenticator(url, username, password),
    },
  });
}
