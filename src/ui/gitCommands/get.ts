import nodegit from "nodegit";

export const get = (repo: nodegit.Repository, option) => {
  if (option === undefined) {
    return repo.getBranchCommit("master");
  }
  option = option || {};
  if (option instanceof nodegit.Commit) {
    return Promise.resolve(option);
  }
  if (option instanceof nodegit.Oid) {
    return repo.getCommit(option);
  }

  return Promise.resolve({
    branch: option.branch || "master",
    commit: option.commit || option || "HEAD",
  }).then(o => {
    if (typeof o.commit !== "string") {
      return repo.getBranchCommit(o.branch);
    }
    if (o.commit.length === 40) {
      return repo.getCommit(o.commit);
    }
    return repo.getCommit(o.commit).catch(() =>
      nodegit.AnnotatedCommit.fromRevspec(repo, o.commit)
        .then(annotatedCommit => annotatedCommit.id())
        .then(id => repo.getCommit(id)),
    );
  });
};
