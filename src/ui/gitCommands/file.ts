import * as path from "path";
import fs from "fs-extra";

import { Commit } from "ui/modules/common/common.models";
import { openRepository } from "ui/gitCommands/repository";

export const getCommitFile = async (repoPath: string, commit: Commit, filePath: string) => {
  try {
    const repo = await openRepository(repoPath);
    const nodegitCommit = await repo.getCommit(commit.sha);
    const entry = await nodegitCommit.getEntry(filePath);
    const fileText = (await entry.getBlob()).toString();
    return fileText;
  } catch (error) {
    return ""; // e.g file removed
  }
};

export const getPathFile = async (repoPath: string, filePath: string) => {
  const totalPath = path.join(repoPath, filePath);
  return fs.readFile(totalPath);
};
