import nodegit from "nodegit";

export const createBranch = async (repo: nodegit.Repository, branchName: string) => {
  const headCommit = await repo.getHeadCommit();

  const branch = await repo.createBranch(
    branchName,
    headCommit,
    undefined,
    repo.defaultSignature(),
    "Created new-branch on HEAD",
  );

  return await repo.checkoutBranch(branch);
};

export const checkoutBranch = async (repo: nodegit.Repository, branchName: string) => {
  const res = await repo.checkoutBranch(branchName);
  if (res === false) {
    // if its undefined its ok : )if false it's not :)
    throw new Error(`Unable to checkout to: ${branchName}`);
  }

  return res;
};

export const deleteBranch = async (repo: nodegit.Repository, branchName: string) => {
  const res = await repo.getBranch(branchName);
  const result = res.delete();
  if (result !== 0) {
    throw new Error("Deleting branch failed");
  }
};
