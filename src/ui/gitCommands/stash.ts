import nodegit from "nodegit";
import { getSignature } from "./config";

export async function stash(
  repo: nodegit.Repository,
  message: string = "stash", // tslint:disable-line
  flags: nodegit.Stash.FLAGS = nodegit.Stash.FLAGS.DEFAULT,
) {
  const signature = await getSignature(repo);
  return nodegit.Stash.save(repo, signature, message, flags);
}

export async function apply(repo: nodegit.Repository, index: number, options?: nodegit.StashApplyOptions) {
  return nodegit.Stash.apply(repo, index, options);
}

export async function pop(repo: nodegit.Repository, index: number, options?: nodegit.StashApplyOptions) {
  return nodegit.Stash.pop(repo, index, options);
}

export async function drop(repo: nodegit.Repository, index: number) {
  return nodegit.Stash.drop(repo, index);
}
