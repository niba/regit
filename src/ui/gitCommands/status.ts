import nodegit from "nodegit";

export async function getStagedFilesDiff(
  repo: nodegit.Repository,
  filepaths?: string[],
): Promise<nodegit.ConvenientPatch[]> {
  const headCommit = await repo.getHeadCommit();
  const headTree = await headCommit.getTree();
  const indexTree = await repo.index();

  const defaultOptions = {
    flags: nodegit.Diff.OPTION.DISABLE_PATHSPEC_MATCH,
  };
  const options = filepaths ? { ...defaultOptions, pathspec: filepaths } : defaultOptions;

  return await (await nodegit.Diff.treeToIndex(repo, headTree, indexTree, options)).patches();
}

export async function getUnstagedFilesDiff(
  repo: nodegit.Repository,
  filepaths?: string[],
): Promise<nodegit.ConvenientPatch[]> {
  const indexTree = await repo.index();

  const defaultOptions = {
    flags:
      nodegit.Diff.OPTION.SHOW_UNTRACKED_CONTENT |
      nodegit.Diff.OPTION.RECURSE_UNTRACKED_DIRS |
      nodegit.Diff.OPTION.DISABLE_PATHSPEC_MATCH,
  };
  const options = filepaths ? { ...defaultOptions, pathspec: filepaths } : defaultOptions;

  return await (await nodegit.Diff.indexToWorkdir(repo, indexTree, options)).patches();
}

export async function getFileStatuses(repo: nodegit.Repository) {
  return await repo.getStatus();
}

export async function getStatus(repo: nodegit.Repository) {
  const local = await repo.getCurrentBranch();
  const upstream = await nodegit.Branch.upstream(local);

  const res = await nodegit.Graph.aheadBehind(repo, local.target(), upstream.target());
  return { ...res, local: local.shorthand(), upstream: upstream.shorthand() };
}
