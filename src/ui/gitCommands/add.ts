import nodegit from "nodegit";
import { WorkdirFileStatus } from "ui/modules/diff/diff.models";

export async function addAll(repo: nodegit.Repository): Promise<nodegit.Oid> {
  const index = await repo.refreshIndex();
  const ind = index as any; // todo typings broken?
  await ind.addAll();
  await ind.write();
  return ind.writeTree();
}

export async function addFiles(repo: nodegit.Repository, files: WorkdirFileStatus[]): Promise<nodegit.Oid> {
  const ind = await repo.refreshIndex();

  for (const file of files) {
    if (file.status === "deleted") {
      await ind.removeByPath(file.path);
    } else if (file.status === "renamed" && file.oldPath) {
      await ind.removeByPath(file.oldPath);
      await ind.addByPath(file.path);
    } else {
      await ind.addByPath(file.path);
    }
  }

  await ind.write();
  return await ind.writeTree();
}

export async function removeFiles(repo: nodegit.Repository, files: WorkdirFileStatus[]): Promise<nodegit.Oid> {
  const ind = await repo.refreshIndex();

  for (const file of files) {
    await ind.removeByPath(file.path);
  }

  await ind.write();
  return await ind.writeTree();
}

export async function resetFiles(repo: nodegit.Repository, files: string[]) {
  const headCommit = await repo.getHeadCommit();
  await nodegit.Reset.default(repo, headCommit as any, files as any);
}
