import nodegit from "nodegit";
import { sshPubKeyPath, sshPrivKeyPath } from "../utils/paths";

export const getRemote = async (repository: nodegit.Repository) => {
  const remotes = await repository.getRemotes(() => {
    console.log("get remote");
  });

  const remote = await repository.getRemote(remotes[0]);
  return remote;
};

export const authenticator = async (url: string, username: string, password?: string) => {
  const httpAuth = () =>
    password ? nodegit.Cred.userpassPlaintextNew(username, password) : nodegit.Cred.usernameNew(username);
  const sshAuth = () => nodegit.Cred.sshKeyNew(username, sshPubKeyPath(), sshPrivKeyPath(), password || "");

  const cred = url.toLowerCase().startsWith("http") ? await httpAuth() : sshAuth();

  return cred;
};
