import nodegit from "nodegit";

async function getConfigsInternal(config: nodegit.Config, options) {
  if (typeof options === "string") {
    return config.getStringBuf(options);
  }
  if (!Array.isArray(options)) {
    return;
  }

  return Promise.all(options.map(option => config.getStringBuf(option)));
}

function sequential<T>(all: (() => Promise<T>)[]) {
  return all.reduce((prev, current) => prev.then(current), Promise.resolve({} as T));
}

function setConfig(config: nodegit.Config, key: string, value: number) {
  if (typeof value === "number") {
    return Promise.resolve(config.setInt64(key, value));
  }
  return config.setString(key, value);
}

async function setUser(config: nodegit.Config, user) {
  if (user.name) await config.setString("user.name", user.name);
  if (user.email) await config.setString("user.email", user.email);
}

export async function setConfigs(repo: nodegit.Repository, o) {
  const options = o || repo;
  const config = await (!!o ? repo.config() : nodegit.Config.openDefault());

  const changes = [] as (() => Promise<number>)[];
  for (const key in options) {
    if (key === "user") continue;
    changes.push(() => setConfig(config, key, options[key]));
  }

  await sequential(changes);

  if (!options.user) return;
  return setUser(config, options.user);
}

export async function getConfigs(repo, o?) {
  const options = o || repo;
  const config = await (!!o ? repo.config() : nodegit.Config.openDefault());
  return getConfigsInternal(config, options);
}

export async function getSignature(repo) {
  const user = ["user.name", "user.email"];
  const userObj = (await (!!repo ? getConfigs(repo, user) : getConfigs(user))) as any;
  return nodegit.Signature.now(userObj[0], userObj[1]);
}
