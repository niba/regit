import nodegit from "nodegit";
import { get } from "./get";

const diffTree = (current, tree) => current.diff(tree).then(diffList => diffList.patches());

export const diffCommits = async (
  repo: nodegit.Repository,
  commitFrom: nodegit.Commit,
  commitTo: nodegit.Commit,
): Promise<nodegit.ConvenientPatch[]> => {
  const from = await get(repo, commitFrom);
  const to = await get(repo, commitTo);
  const trees = await Promise.all([to.getTree(), from.getTree()]);
  const patches: nodegit.ConvenientPatch[] = await diffTree(trees[0], trees[1]);
  return patches;
};

export const diffWithParent = async (
  repo: nodegit.Repository,
  commit: nodegit.Commit,
  filePaths?: string[],
): Promise<nodegit.ConvenientPatch[]> => {
  const options = filePaths
    ? {
        flags: nodegit.Diff.OPTION.DISABLE_PATHSPEC_MATCH,
        pathspec: filePaths,
      }
    : undefined;

  const diffs = await commit.getDiffWithOptions(options);

  const combinedPatches: nodegit.ConvenientPatch[][] = await Promise.all(diffs.map(diff => diff.patches()));
  const patches = ([] as nodegit.ConvenientPatch[]).concat(...combinedPatches);
  return patches;
};

export const diffWithWorkdir = async (
  repo: nodegit.Repository,
  commit: nodegit.Commit,
): Promise<nodegit.ConvenientPatch[]> => {
  const tree = await commit.getTree();
  const diff = await nodegit.Diff.treeToWorkdir(repo, tree, {} as nodegit.DiffOptions);
  const patches = await diff.patches();
  return patches;
};

export const diffWithIndex = async (repo: nodegit.Repository): Promise<nodegit.ConvenientPatch[]> => {
  const index = await repo.refreshIndex();

  const diffOptions = {
    flags:
      nodegit.Diff.OPTION.INCLUDE_UNTRACKED |
      nodegit.Diff.OPTION.INCLUDE_UNREADABLE |
      nodegit.Diff.OPTION.SKIP_BINARY_CHECK,
  } as nodegit.DiffOptions;

  const patches = await nodegit.Diff.indexToWorkdir(repo, index, diffOptions).then(diff => diff.patches());

  return patches;
};

export const diffStaging = async (repo: nodegit.Repository): Promise<nodegit.ConvenientPatch[]> => {
  const index = await repo.refreshIndex();

  const diffOptions = {
    flags: nodegit.Diff.OPTION.SHOW_UNTRACKED_CONTENT | nodegit.Diff.OPTION.RECURSE_UNTRACKED_DIRS,
  } as nodegit.DiffOptions;

  const patches = await nodegit.Diff.indexToWorkdir(repo, index, diffOptions).then(diff => diff.patches());

  return patches;
};
