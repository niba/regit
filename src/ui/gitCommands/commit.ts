import nodegit from "nodegit";
import { getSignature } from "./config";
import { getBranches } from "./repository";
import { Reference, Commit } from "ui/modules/common/common.models";
import { mapCommit } from "ui/modules/common/common.mappers";
import { PlainObject } from "ui/modules/common/common.models";

export async function makeCommit(repo: nodegit.Repository, message: string) {
  const status = await repo.getStatus();
  if (!status.length) {
    return undefined;
  }

  const index = await repo.index();
  const tree = await index.writeTree();
  const result = await Promise.all([tree, repo.getHeadCommit(), getSignature(repo)]);
  const data = {
    tree: result[0],
    commit: result[1],
    author: result[2],
    committer: result[2],
    message,
  };

  return repo.createCommit("HEAD", data.author, data.committer, data.message, data.tree, [data.commit.id()]);
}

export async function resetTo(repo: nodegit.Repository, targetCommit: nodegit.Commit | nodegit.Oid) {
  const commitInstance = targetCommit instanceof nodegit.Commit ? targetCommit : await repo.getCommit(targetCommit);
  return nodegit.Reset.reset(repo, commitInstance as any, nodegit.Reset.TYPE.HARD, {});
}

interface GetCommitsOptions {
  limit: number;
  branchesToFilter: nodegit.Reference[];
  tagsToFilter: nodegit.Reference[];
}

const getCommitsDefaultOptions: GetCommitsOptions = {
  limit: -1,
  branchesToFilter: [],
  tagsToFilter: [],
};

export async function getCommitsFast(repository: nodegit.Repository, limit: number) {
  const walker = repository.createRevWalk();
  walker.sorting(nodegit.Revwalk.SORT.TOPOLOGICAL | nodegit.Revwalk.SORT.TIME);
  const branches = await getBranches(repository);

  for (const branch of branches) {
    const branchLastCommit = await repository.getBranchCommit(branch.name());
    walker.push(branchLastCommit.id());
  }
  const commits = await walker.fastWalk(limit);

  for (const commit of commits) {
    await repository.getCommit(commit);
  }
  return commits;
}

export async function loadCommitsOrder(repository: nodegit.Repository, references: Reference[]): Promise<string[]> {
  const walker = repository.createRevWalk();
  walker.sorting(nodegit.Revwalk.SORT.TIME | nodegit.Revwalk.SORT.TOPOLOGICAL);

  for (const reference of references) {
    walker.push(nodegit.Oid.fromString(reference.target));
  }

  return (await walker.fastWalk(200000)).map(oid => oid.tostrS());
}

export async function loadCommitsMap(
  repository: nodegit.Repository,
  references: Reference[],
  commitsMap: PlainObject<Commit> = {},
): Promise<{ areFreshCommits: boolean; commitsMap?: PlainObject<Commit> }> {
  const walker = repository.createRevWalk();

  for (const reference of references) {
    walker.push(nodegit.Oid.fromString(reference.target));
  }

  const commitShas = (await walker.fastWalk(200000)).map(oid => oid.tostrS()); // todo: set limit in settings;
  let areFreshCommits = false;

  const mapLength = Object.keys(commitsMap).length;
  areFreshCommits = mapLength > commitShas.length; // commit removed

  for (const sha of commitShas) {
    if (!commitsMap[sha]) {
      areFreshCommits = true;
      commitsMap[sha] = mapCommit(await repository.getCommit(sha));
    }
  }

  if (areFreshCommits) {
    return { areFreshCommits, commitsMap };
  } else {
    return { areFreshCommits };
  }
}

export async function getCommits2(
  repository: nodegit.Repository,
  references: Reference[] = [],
  commitsMap: PlainObject<Commit> = {},
  options = getCommitsDefaultOptions,
): Promise<{
  areFreshCommits: boolean;
  commitsOrder: string[];
  commitsMap: PlainObject<Commit>;
}> {
  const walker = repository.createRevWalk();
  walker.sorting(nodegit.Revwalk.SORT.TOPOLOGICAL | nodegit.Revwalk.SORT.TIME); // tslint:disable-line

  for (const reference of references) {
    walker.push(nodegit.Oid.fromString(reference.target));
  }

  let oid: nodegit.Oid | undefined;
  let commitCounter = 0;
  let areFreshCommits = false;
  const commitsOrder: string[] = [];

  do {
    try {
      oid = await walker.next();
      if (options.limit > -1 && commitCounter > options.limit) {
        throw new Error();
      }
    } catch (err) {
      oid = undefined;
    }

    if (oid !== undefined) {
      commitCounter += 1;
      const oidString = oid.tostrS();
      if (!commitsMap[oidString]) {
        areFreshCommits = true;
        commitsMap[oidString] = mapCommit(await repository.getCommit(oid));
      }
      commitsOrder.push(commitsMap[oidString].sha);
    }
  } while (oid !== undefined);

  return { areFreshCommits, commitsOrder, commitsMap };
}

export async function getCommits(repository: nodegit.Repository, options = getCommitsDefaultOptions) {
  const walker = repository.createRevWalk();
  walker.sorting(nodegit.Revwalk.SORT.TOPOLOGICAL | nodegit.Revwalk.SORT.TIME); // tslint:disable-line

  const branches = await getBranches(repository, {
    omitReferences: options.branchesToFilter.map(branch => branch.name()),
  });

  for (const branch of branches) {
    const branchLastCommit = await repository.getBranchCommit(branch.name());
    walker.push(branchLastCommit.id());
  }

  let oid;
  let commitCounter = 0;
  const commits: nodegit.Commit[] = [];

  do {
    try {
      oid = await walker.next();
      if (options.limit > -1 && commitCounter > options.limit) {
        throw new Error();
      }
    } catch (err) {
      oid = undefined;
    }

    if (oid !== undefined) {
      commitCounter += 1;
      const commit = await repository.getCommit(oid);
      commits.push(commit);
    }
  } while (oid !== undefined);

  return commits;
}

export async function mergeTwoBranches(repository: nodegit.Repository, src: nodegit.Reference, dst: nodegit.Reference) {
  try {
    const id = await repository.mergeBranches(dst, src);
    if (src.target().equal(id)) {
      throw new Error("Already up-to-date");
    }
  } catch (error) {
    if (error instanceof nodegit.Index) throw new Error("Branches couldn't be merged due to conflicts");

    throw error;
  }
}

// export async function mergeTwoBranches(
//   repository: nodegit.Repository,
//   their: nodegit.Reference,
//   our: nodegit.Reference,
// ) {
//   const theirCommit = await repository.getCommit(their.target());
//   const ourCommit = await repository.getCommit(our.target());

//   const index = await nodegit.Merge.commits(repository, ourCommit, theirCommit);

//   if (index.hasConflicts()) throw new Error("There are conflicts than cannot be resolved");

//   const oid = await index.writeTreeTo(repository);
//   const mergeCommit = await repository.createCommit(
//     our.name(),
//     repository.defaultSignature(),
//     repository.defaultSignature(),
//     `Merge branch '${their.shorthand()}'`,
//     oid,
//     [ourCommit, theirCommit],
//   );

//   return mergeCommit;
// }
