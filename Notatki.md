Zeby odpalic testy wejdz do test_libs/node_modules/nodegit/dist/nodegit.js i zamien poczatek na pasujacy do tego

```
"use strict";

// This is a generated file, modify: generate/templates/templates/nodegit.js
var _ = require("lodash");
var promisify = require("promisify-node");
var rawApi;

// Attempt to load the production release first, if it fails fall back to the
// debug release.
try {
  rawApi = require("../build/Release/nodegit.node");
} catch (ex) {
  /* istanbul ignore next */
  if (ex.code !== "MODULE_NOT_FOUND") {
    throw ex;
  }

  rawApi = require("../build/Debug/nodegit.node");
}

rawApi = _.cloneDeep(rawApi);

... dalej bez zmian
```

i plik revwalki.js w tym samym katalogu
```
Object.defineProperty(Revwalk.prototype, "repo", {
  get: function get() {
    return this.repository();
  },
  configurable: true
});
```


"^.+\\.(ts|tsx)$": "<rootDir>/config/jest/tsTransform.js",

  ".(ts|tsx)": "<rootDir>/node_modules/ts-jest/preprocessor.js",



