const tsc = require('typescript');
const tsConfig = require('../../tsconfig.json');
const babelJest = require('babel-jest');

function typescriptTransform(src, path) {
    if (path.endsWith('.ts') || path.endsWith('.tsx')) {
      return {
        src: tsc.transpile(
          src,
          tsConfig.compilerOptions,
          path,
          []
        ),
        path : path.substr(0, path.lastIndexOf(".")) + ".js"
      }
    }
    return { src, path };
}

module.exports = {
  process(src, path) {
    const transformedByTypescript = typescriptTransform(src, path);
    return babelJest.process(transformedByTypescript.src, transformedByTypescript.path);
  },
};
