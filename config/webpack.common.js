const autoprefixer = require('autoprefixer');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const getClientEnvironment = require('./env');
const paths = require('./paths');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');
const CircularDependencyPlugin = require('circular-dependency-plugin')

const publicPath = '/';
const publicUrl = '';

const env = getClientEnvironment(publicUrl);

module.exports.env = env;

module.exports.loaders = {
  css: {
    test: /\.css$/,
    use: [
      require.resolve('style-loader'),
      {
        loader: require.resolve('css-loader'),
        options: {
          importLoaders: 1,
        },
      },
    ],
  },
  file: {
    exclude: [
      /\.html$/,
      /\.(js|jsx)$/,
      /\.(scss|css)$/,
      /\.json$/,
      /\.bmp$/,
      /\.gif$/,
      /\.jpe?g$/,
      /\.png$/,
    ],
    loader: 'file-loader',
    options: {
      name: 'static/media/[name].[hash:8].[ext]',
    },
  },
  url: {
    test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
    loader: 'url-loader',
    options: {
      limit: 10000,
      name: 'static/media/[name].[hash:8].[ext]',
    },
  },
}


module.exports.config = function(extendedConfig = {}) {
  return {
    entry: [
      require.resolve('./polyfills'),

      paths.appIndexJs,
    ],
    output: {
      path: paths.appBuild,
      filename: 'static/js/bundle.js',
      publicPath: extendedConfig.publicPath ? extendedConfig.publicPath : publicPath,
    },
    externals: [
      function(context, request, callback) { // todo: check it later
        if(/^nodegit/.test(request))
          return callback(null, 'commonjs' + " " + request);
        callback();
      },
    ],
    resolve: {
      modules: ['node_modules', paths.appNodeModules].concat(
        process.env.NODE_PATH.split(path.delimiter).filter(Boolean)
      ),
      extensions: [
        '.web.ts',
        '.ts',
        '.web.tsx',
        '.tsx',
        '.web.js',
        '.js',
        '.json',
        '.web.jsx',
        '.jsx',
      ],
      plugins: [
        new ModuleScopePlugin(paths.appSrc),
      ],
    },
    target: 'electron-renderer',
    module: {
      rules: []
    },
    plugins: [
      new CircularDependencyPlugin({
        // exclude detection of files based on a RegExp
        exclude: /node_modules/,
        // add errors to webpack instead of warnings
        failOnError: false
      }),
      new InterpolateHtmlPlugin(env.raw),
      new HtmlWebpackPlugin({
        inject: true,
        template: paths.appHtml,
        minify: extendedConfig.minify
      }),
      new webpack.DefinePlugin(env.stringified),
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    ]
  }
}
