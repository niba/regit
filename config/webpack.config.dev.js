const paths = require('./paths');
const commonWebpack = require("./webpack.common");
const webpack = require('webpack');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');

const commonConfig = commonWebpack.config();
const commonLoaders = commonWebpack.loaders;

commonConfig.devtool = 'eval'// 'cheap-module-source-map',
commonConfig.entry = ['react-hot-loader/patch', require.resolve('react-dev-utils/webpackHotDevClient')].concat(commonConfig.entry);

commonConfig.module.rules = [
  {
    test: /\.(ts|tsx)$/,
    loader: require.resolve('tslint-loader'),
    enforce: 'pre',
    include: paths.appSrc,
  },
  {
    test: /\.js$/,
    loader: require.resolve('source-map-loader'),
    enforce: 'pre',
    include: paths.appSrc,
  },
  { parser: { requireEnsure: false } },
  {
    oneOf: [
      {
        test: /\.(ts|tsx)$/,
        include: paths.appSrc,
        loaders: [
           require.resolve('babel-loader'),
          'react-hot-loader/webpack',
          require.resolve('ts-loader'),
        ]
      },
      commonLoaders.url,
      commonLoaders.css,
      commonLoaders.file,
    ]
  }
]

commonConfig.plugins.push(new webpack.NamedModulesPlugin());
commonConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
commonConfig.plugins.push(new CaseSensitivePathsPlugin());
commonConfig.plugins.push(new WatchMissingNodeModulesPlugin(paths.appNodeModules));

commonConfig.performance = {
  hints: false,
}
module.exports = commonConfig;
