const paths = require('./paths');
const commonWebpack = require("./webpack.common");
const webpack = require('webpack');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const ManifestPlugin = require('webpack-manifest-plugin');
//const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

if (commonWebpack.env.stringified['process.env'].NODE_ENV !== '"production"') {
  throw new Error('Production builds must have NODE_ENV=production.');
}

const commonConfig = commonWebpack.config({
  minify: {
    removeComments: true,
    collapseWhitespace: true,
    removeRedundantAttributes: true,
    useShortDoctype: true,
    removeEmptyAttributes: true,
    removeStyleLinkTypeAttributes: true,
    keepClosingSlash: true,
    minifyJS: true,
    minifyCSS: true,
    minifyURLs: true,
  },
  publicPath: './'
});
const commonLoaders = commonWebpack.loaders;
const cssFilename = 'static/css/[name].[contenthash:8].css';

commonConfig.module.rules = [
  {
    oneOf: [
      {
        test: /\.(ts|tsx)$/,
        include: paths.appSrc,
        loaders: [
          require.resolve('babel-loader'),
          {
            loader: 'ts-loader',
            options: {
              configFile : "tsconfig.react.json",
            }
          },
        ]
      },
      commonLoaders.url,
      commonLoaders.css,
      commonLoaders.file,
    ]
  }
]

commonConfig.plugins.push(new UglifyJSPlugin({
  uglifyOptions: {
    compress: {
      warnings: false,
      comparisons: false,
    },
    output: {
      comments: false,
      ascii_only: true,
    }
  }
}));
commonConfig.plugins.push(new ManifestPlugin({
  fileName: 'asset-manifest.json',
}));
// commonConfig.plugins.push( new ExtractTextPlugin({
//   filename: cssFilename,
// }));
commonConfig.plugins.push(new WatchMissingNodeModulesPlugin(paths.appNodeModules));

commonConfig.performance = {
  hints: false,
}


module.exports = commonConfig;
