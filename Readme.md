## Odpalanie

1. `npm install -g npm` zeby zaktualizowac `npm`, sprawdzcie czy macie wersje `5.4.2` albo wyzsza. Sprawdza sie za pomoca `npm --version`
2. `npm install`. W przypadku bledu z node-gyp trzeba wywolac `npm install -g --production windows-build-tools`
3. `npm run build:native` zeby zbudowac natywne biblioteki takie jak `nodegit` pod Electrona V8.
4.  Wejsc w folder `test_libs` i odpalic `npm install`. Zainstaluje nam to `nodegit` ktory bedzie uzywany do testow ktore sa uruchamiane na Node V8.

Czemu dwa razy instalujemy nodegit? Nodegit to biblioteka natywna czyli taka ktora buduje kod c++ pod konkretna maszyne V8. Za pomoca `npm run build:native` budujecie nodegita pod maszyne V8 z ktorej korzysta `Electron` pod spodem. Testy sa odpalane na nodzie a nie na electronie wiec potrzebuja inaczej zbudowanego `nodegit` i dlatego instalujemy go drugi raz. Samo wywolanie `npm install` buduje natywna biblioteke pod wersje V8 zainstalowanego globalnie Node. Ja uzywam Node 7.0.10 i proponuje uzywac tego samego. Tutaj link https://nodejs.org/download/release/v7.10.1/node-v7.10.1-x64.msi

- `npm run start` Odpala cala aplikacje.
- `npm run start:electron` Przebudowuje i restartuje electrona. (NIE MA HOT RELOADA)
- `npm run start:ui` Odpala czesc Reactowa oparta na webpacku w celu uzyskania hot reloadingu
- `npm run test` lub `npm run test:watch`

Dodatkowo skonfigurowalem taski w VSCode dzieki ktorym mozna odpalac wszystko w VCode i miec debugowanie.

- `Regit Electron` odpowiednik `npm run start` z debugowaniem
- `Launch Regit UI` odpowiednik ``npm run start:ui` i na ten moment nie dziala :D. Patrz nizej
- `Attach Regit UI` po odpaleniu `npm run start:ui` mozna sie podpiac pod dzialajacego chrome w electronie. Jedyne wymaganie jest takie ze Developer Toolsy chromowe musza byc zamkniete. Dzieki temu mozemy debugowac Front i Backend(Electron) w VSCode.
- `Tests` odpala testy z debuggerem.

## Do ogarniecia

- [Jak dziala git](https://codewords.recurse.com/issues/two/git-from-the-inside-out)
- Tutaj koles pisze o tym co chcemy osiagnac: http://tonsky.me/blog/reinventing-git-interface/
- API gita. Korzystamy z biblioteki [nodegit](https://github.com/nodegit/nodegit) ktora jest nakladka na biblioteke zrobiona w c++ [libgit2](https://libgit2.github.com/).
- [Electron](https://electron.atom.io/docs/) na ktorym stoi aplikacja.
- Jak podpiac baze do electrona.
- Jak zrobic installera.
- Jak zrobic auto update aplikacji.
- Trzeba przygotowac cala infrastrukture pod testy. Tzn napisac swoje API dzieki ktorym testy beda tworzyc jakies testowe repo? Zaczalem juz tworzyc cos takiego `tests/setup.ts`


## Konkurencja

- [GitKraken](https://www.gitkraken.com/)
- [GitTower](https://www.git-tower.com/windows/)
- [SourceTree](https://www.sourcetreeapp.com/)
- [GMaster](https://gmaster.io/)


### Ogolnie na ten moment wzorujemy sie na GitKraken












